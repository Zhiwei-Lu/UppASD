%Plots the spin-spin dynamic structure factor S(q,w)
%Reads the sqw-file

% Clear variables
clear

% Flags to govern script execution
mode = 'MODE'
aunits = 'AUNITS'

%Make sure Nw, Nq, Ts matches entries in inpsd.dat
Nw=NW % uc_nsteps Number of frequencies (=number of samples)
Nq=NQ % Number of q-values
Nqhalf = Nq/2
stepsize = SAMPSTEP
delta_t = TIMESTEP
Ts=stepsize * delta_t
if strcmp(aunits,'Y')
f=(1:Nw/2)/(Nw*Ts);
else
f=(1:Nw/2)/(Nw*Ts)*1e-12; % in THz
end

load ams.triang2D.out
spindisp_triang2D = ams_triang2D;
load qpoints.out

figure(12); clf
plot(qpoints(:,1),spindisp_triang2D(:,2),'.-','LineWidth',2)
xlabel('Wavevector Q','FontSize',16)
ylabel('w(Q) meV','FontSize',16)
box on
set(gca,'FontSize',16)
set(gca,'LineWidth',2)
if Nq==21
set(gca,'XTick',[1 8 14 24])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'K' 'M' 'G']')%'
axis tight
print -depsc2 spindisp

disp('Start reading sqw file')
load sqw.triang2D.out
sqw=sqw_triang2D;
for i=1:Nq
ns=(i-1)*Nw/2+1;
ne=i*Nw/2;
sqwm(:,:,i)=sqw(ns:ne,:);
end
disp('Finished reading sqw file')

%Normalize separately each component of S(q,w) for each q-value
for j=6:9
for i=1:Nq
sqwmmax = max(max(sqwm(1:Nw/2,j,i)));
sqwm(:,j,i)=sqwm(:,j,i)/sqwmmax;
end
end
disp('Finished normalizing sqw')
sqwmav = sqwm;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) for fixed q as a function of w
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%which q-values to plot for
for i=1:6
figure(50+i)
clf
hold on
plot(f,sqwmav(:,9,i),'k','LineWidth',2)
plot(f,sqwmav(:,6,i),'.r','LineWidth',2)
plot(f,sqwmav(:,7,i),'.-g','LineWidth',2)
plot(f,sqwmav(:,8,i),':b','LineWidth',2)
legend('S(q,\omega)','S^x(q,\omega)','S^y(q,\omega)','S^z(q,\omega)', 'Location','SouthEast','FontSize',16)
xlabel('f (THz)','FontSize',16)
ylabel('Normalized structure factor S(q,\omega)','FontSize',16)
box on
axis ([0 f(Nw/2) 0 1])
set(gca,'FontSize',16)
prnstr=['print -depsc2 sqw' num2str(i-1)];
eval(prnstr);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Common variables for S(q,w) plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

hbar=6.582e-13; %in meVs
fmax=Nw/2/4
qmin=1
qmax=Nq %full range, plots for all q-values


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in frequency f (THz)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(41)
clf
hold on

y=f(1:fmax);
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,6,qmin:qmax));
surf(X,Y,Z)
grid on
box on
axis([qmin qmax f(1) f(fmax)])
xlabel('Wave vector q')
ylabel('f (THz)','FontSize',16)
set(gca,'FontSize',16)
shading interp
print -depsc2 sqwspinxthz


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in energy (meV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(42)
clf
hold on

y=f(1:fmax)*2*pi*hbar*1e12;
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,6,qmin:qmax));
surf(X,Y,Z)
grid on
box on
axis([qmin qmax f(1) f(fmax)*2*pi*hbar*1e12])
xlabel('Wave vector q')
ylabel('E (meV)','FontSize',16)
set(gca,'FontSize',16)
if Nq==21
set(gca,'XTick',[1 8 14 24])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'K' 'M' 'G']')%'
shading interp
print -depsc2 sqwspinxmev


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in energy (meV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(43)
clf
hold on

y=f(1:fmax)*2*pi*hbar*1e12;
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,6,qmin:qmax));
surf(X,Y,Z)
grid on
box on
axis([qmin qmax f(1) f(fmax)*2*pi*hbar*1e12])
xlabel('Wave vector q')
ylabel('E (meV)','FontSize',16)
set(gca,'FontSize',16)
if Nq==21
set(gca,'XTick',[1 8 14 24])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'K' 'M' 'G']')%'
shading interp
print -depsc2 sqwspinxmev


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in energy (meV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(44)
clf
hold on

y=f(1:fmax)*2*pi*hbar*1e12;
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,7,qmin:qmax));
surf(X,Y,Z)
grid on
box on
axis([qmin qmax f(1) f(fmax)*2*pi*hbar*1e12])
xlabel('Wave vector q')
ylabel('E (meV)','FontSize',16)
set(gca,'FontSize',16)
if Nq==21
set(gca,'XTick',[1 8 14 24])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'K' 'M' 'G']')%'
shading interp
print -depsc2 sqwspinzmev


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in energy (meV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(46)
clf
hold on

y=f(1:fmax)*2*pi*hbar*1e12;
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,6,qmin:qmax));
h=surf(X,Y,Z)
z = get(h,'ZData');
set(h,'ZData',z-10)  
plot(qpoints(:,1),spindisp_triang2D(:,2:2),'-k','LineWidth',2)
grid on
box on
fmax0=1.1*max(max(spindisp_triang2D(:,2:2)))
axis([qmin qmax f(1) fmax0])
xlabel('Wave vector q')
ylabel('E (meV)','FontSize',16)
set(gca,'FontSize',16)
if Nq==21
set(gca,'XTick',[1 8 14 24])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'K' 'M' 'G']')%'
shading interp
print -depsc2 sqwspinxmev2
