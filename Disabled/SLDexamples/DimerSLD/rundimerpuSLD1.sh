#!/bin/bash

mkdir DimerpuSLD1
cd DimerpuSLD1
cp -p ../Base/* .
sed -i "s/AUNITS/N/" inpsd.dat
sed -i "s/#mml/mml/" inpsd.dat
sed -i "s/MMLFILE/mmlfile.nosym/" inpsd.dat
sed -i "s/NSTEP/20000/" inpsd.dat
sed -i "s/TIMESTEP/1e-16/" inpsd.dat
sed -i "s/DAMPING/0.00/" inpsd.dat
sed -i "s/LATTDAMP/0.00/" inpsd.dat
sed -i "s/TEMP/0.00/" inpsd.dat
sed -i "s/SAMPSTEP/10/" inpsd.dat

sed -i "s/MODE/R/" plotTwoion.m
sed -i "s/AUNITS/N/" plotTwoion.m
sed -i "s/NSTEP/20000/" plotTwoion.m
sed -i "s/TIMESTEP/1e-16/" plotTwoion.m
sed -i "s/LATTDAMP/0.00/" plotTwoion.m
sed -i "s/SAMPSTEP/10/" plotTwoion.m

../../../source/sd > out.log
cd ..