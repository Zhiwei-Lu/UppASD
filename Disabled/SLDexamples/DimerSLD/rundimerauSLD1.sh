#!/bin/bash

mkdir DimerauSLD1
cd DimerauSLD1
cp -p ../Base/* .
sed -i "s/AUNITS/Y/" inpsd.dat
sed -i "s/#mml/mml/" inpsd.dat
sed -i "s/MMLFILE/mmlfile.nosym/" inpsd.dat
sed -i "s/NSTEP/100000/" inpsd.dat
sed -i "s/TIMESTEP/1e-3/" inpsd.dat
sed -i "s/DAMPING/0.00/" inpsd.dat
sed -i "s/LATTDAMP/0.00/" inpsd.dat
sed -i "s/TEMP/0.00/" inpsd.dat
sed -i "s/SAMPSTEP/100/" inpsd.dat

sed -i "s/MODE/R/" plotTwoion.m
sed -i "s/AUNITS/Y/" plotTwoion.m
sed -i "s/NSTEP/100000/" plotTwoion.m
sed -i "s/TIMESTEP/1e-3/" plotTwoion.m
sed -i "s/LATTDAMP/0.00/" plotTwoion.m
sed -i "s/SAMPSTEP/100/" plotTwoion.m

../../../source/sd > out.log
cd ..