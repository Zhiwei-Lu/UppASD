#!/bin/sh
#SBATCH -J asd
#SBATCH -t 03-00:00:00
#SBATCH -N 1
#SBATCH --exclusive
#SBATCH -U snic2018-7-7
#SBATCH --mail-type=ALL
#SBATCH --mail-user=johan.hellsvik@physics.uu.se

echo "Script initiated at `date` on `hostname`"

time ../../../source/sd > out.log

echo "Script finished at `date` on `hostname`"
