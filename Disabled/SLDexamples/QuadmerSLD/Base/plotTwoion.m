% Plot displacements and velocities for a coupled two-ion system
% that is oscillating along the x-direction

% Clear variables
clear

% Flags to govern script execution
mode = 'MODE'
aunits = 'AUNITS'

% LOADING OF DATA
% Load displacement data
load disp.scnobase.out
disp=disp_scnobase;
[tmpstr,lattenergy]=hdrload('lattenergy.scnobase.out');
[tmpstr,lattaverages]=hdrload('lattaverages.scnobase.out');

% Size of displacement data
[na,nb]=size(disp)
Natom=2
ne=na/Natom

% Separate displacement and velocities data for each particle
dispm=zeros(ne,8,Natom);
for i=1:ne
    for j=1:Natom
        ia=(i-1)*Natom+j;
        dispm(i,1:8,j) = disp(ia,1:8);
    end
end

% If SLD simulation, load magnetic trajectories
if strcmp(mode,'R')
[tmpstr,lattmomenta]=hdrload('lattmomenta.scnobase.out');
load moment.scnobase.out
moment=moment_scnobase;
% Separate magnetic moment trajectories for each particle
momentm=zeros(ne,6,Natom);
for i=1:ne
    for j=1:Natom
        ia=(i-1)*Natom+j;
        dispm(i,1:8,j) = disp(ia,1:8);
        momentm(i,1:6,j) = moment(ia,1:6);
    end
end
end

%%% SIMULATION UNITS %%%
if strcmp(aunits,'Y')
% Change constants to atomic units
gama=1d0
k_bolt=1d0
mub = 1d0
mu0=1d0
mry=1d0
hbar_mev=1d0
k_bolt_ev=1d0
ry_ev=1.0d0
amu=1d0
angstrom=1d0
ev=1d0

else 

% Constants in physical units
gama=1.76d11 
k_bolt=1.38d-23 
k_bolt_ev=8.61734d-5
mub=9.274d-24
mu0=1.2566d-6
mry=2.1799d-21
hbar=6.62606d-34
hbar_mev=6.58211899d-13
Joule_ev=6.24150934d18
ry_ev=13.605698066
ev=1.602176565d-19
amu=1.660539040d-27
angstrom=1.0d-10

end


%%% SIMULATION INPUT PARAMETERS %%%
Nstep=NSTEP;
stepsize = SAMPSTEP
delta_t = TIMESTEP

mion=10;
ktot=2;
mmom=1
J=1; 

% Initial displacement in Angstrom
u0=0.02
% Lattice damping in kg / s
lambda=LATTDAMP

Nw=Nstep/stepsize % Number of samples (=number of frequencies)
Ts=stepsize * delta_t
f=(1:Nw)/(Nw*Ts);
if strcmp(aunits,'Y')
t=(0:Nw)*Ts;
else
t=(0:Nw)*Ts*1e12;
end

%%% ANALYTICAL RESULTS %%%
% Peak velocity in Angstrom / s
vmax0=sqrt(ktot * mry / angstrom^2 / mion / amu) * u0
vmax=max(dispm(:,6,1))
vquota=vmax0/vmax

% Lattice dynamics frequencies and period time
T=2*pi*sqrt(mion*amu/(ktot*mry/angstrom^2))
f=1/T
w0=2*pi*f

% Spin dynamics frequencies and period time
Bex=J*2*mry/(mub*mmom) %for m=1 muB and with double counting
wL0=gama*Bex
fL0=wL0/(2*pi)
TL0=1/fL0
Tdimer=TL0 / 2 %The precession period observed in the lab frame

zetanom=lambda
zetaden=2*sqrt(mion*amu*ktot*mry/angstrom^2)
zeta=zetanom/zetaden
w=w0*sqrt(1-zeta^2)
tt=(0:Nw)*Ts;
y=u0*exp(-zeta*w0*tt).*cos(w0*tt);


% Lattice dynamics displacement trajectories
figure(1); clf; hold on
plot(t,dispm(:,3,1),'b','LineWidth',2)
plot(t,dispm(:,3,2),'r','LineWidth',2)
plot(t,dispm(:,3,1)+dispm(:,3,2),'k','LineWidth',2)
plot(t,y,'g','LineWidth',2)
legend('Ion 1', 'Ion 2', 'Ion 1 and 2','Analytical','FontSize',16)
if strcmp(aunits,'Y')
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Displacement along x (a.u.)','FontSize',16)
else
xlabel('Time steps (ps)','FontSize',16)
ylabel('Displacement along x (Angstrom)','FontSize',16)
end
axis tight
aa=axis;
aa(1)=0;
aa(2)=max(t);
aa(3)=aa(3)*1.1;
aa(4)=aa(4)*1.1;
axis(aa);
box on
set(gca,'FontSize',16)
print -depsc2 xdisp


% Lattice dynamics velocity trajectories
figure(2); clf; hold on
plot(t,dispm(:,6,1),'b','LineWidth',2)
plot(t,dispm(:,6,2),'r','LineWidth',2)
plot(t,dispm(:,6,1)+dispm(:,6,2),'k','LineWidth',2)
legend('Ion 1', 'Ion 2', 'Ion 1 and 2','FontSize',16)
if strcmp(aunits,'Y')
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Velocities along x (a.u.)','FontSize',16)
else
xlabel('Time steps (ps)','FontSize',16)
ylabel('Velocities along x (Angstrom/s)','FontSize',16)
end
axis tight
aa=axis;
aa(1)=0;
aa(2)=max(t);
aa(3)=aa(3)*1.1;
aa(4)=aa(4)*1.1;
axis(aa)
box on
set(gca,'FontSize',16)
print -depsc2 xvel



% Spin-lattice dynamics energies
%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
figure(3); clf; hold on
plot(t,lattenergy(:,2),'b','LineWidth',2)
plot(t,lattenergy(:,3),'r','LineWidth',2)
plot(t,lattenergy(:,4),'g','LineWidth',2)
plot(t,lattenergy(:,6),'m','LineWidth',2)
plot(t,lattenergy(:,7),'k','LineWidth',2)
legend('V_{LD}', 'V_{SD}', 'V_{SLD}', 'T', 'E_{tot}', 'FontSize',16)
if strcmp(aunits,'Y')
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Energy (a.u.)','FontSize',16)
else
xlabel('Time steps (ps)','FontSize',16)
ylabel('Energy (mRyd)','FontSize',16)
end
box on
axis tight
aa=axis;
aa(1)=0;
aa(2)=max(t);
aa(3)=aa(3)*1.1-0.01;
aa(4)=aa(4)*1.1+0.01;
axis(aa)
set(gca,'FontSize',16)
print -depsc2 sldenergy1


% Lattice dynamics energies
%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
figure(4); clf; hold on
subplot(2,1,1); hold on
plot(t,lattenergy(:,2),'b','LineWidth',2)
plot(t,lattenergy(:,6),'r','LineWidth',2)
plot(t,lattenergy(:,2)+lattenergy(:,6),'k','LineWidth',2)
legend('V_{LD}', 'T', 'V_{LD}+T','FontSize',16)
if strcmp(aunits,'Y')
ylabel('Ionic energy (a.u.)','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
else
ylabel('Ionic energy (mRyd)','FontSize',16)
xlabel('Time steps (ps)','FontSize',16)
end
axis tight
aa=axis;
aa(1)=0;
aa(2)=max(t);
aa(3)=aa(3)*1.1-0.01;
aa(4)=aa(4)*1.1+0.01;
axis(aa)
box on
set(gca,'FontSize',16)
subplot(2,1,2); hold on
plot(t,lattenergy(:,7),'k','LineWidth',2)
legend('E_{tot}','FontSize',16)
if strcmp(aunits,'Y')
ylabel('Ionic energy (a.u.)','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
else
ylabel('Ionic energy (mRyd)','FontSize',16)
xlabel('Time steps (ps)','FontSize',16)
end
axis tight
aa=axis;
aa(1)=0;
aa(2)=max(t);
%aa(3)=aa(3)*1.1-0.01;
%aa(4)=aa(4)*1.1+0.01;
axis(aa)
box on
set(gca,'FontSize',16)
print -depsc2 sldenergy2


% Spin dynamics trajectories
if strcmp(mode,'R')
figure(5); clf
subplot(3,1,1); hold on
plot(t,momentm(:,3,1),'b','LineWidth',2)
plot(t,momentm(:,4,1),'b:','LineWidth',2)
%plot(t,momentm(:,5,1),'b--','LineWidth',2)
legend('m^x_1', 'm^y_1', 'm^z_1', 'FontSize',16)
ylabel('m_1 ','FontSize',16)
axis([0 max(t) -0.5 1.2])
box on
subplot(3,1,2); hold on
plot(t,momentm(:,3,2),'r','LineWidth',2)
plot(t,momentm(:,4,2),'r:','LineWidth',2)
%plot(t,momentm(:,5,2),'r--','LineWidth',2)
legend('m^x_1', 'm^y_1', 'm^z_1', 'FontSize',16)
ylabel('m_2 ','FontSize',16)
axis([0 max(t) -0.5 1.2])
box on
subplot(3,1,3); hold on
plot(t,momentm(:,3,1)+momentm(:,3,2),'k','LineWidth',2)
plot(t,momentm(:,4,1)+momentm(:,4,2),'k:','LineWidth',2)
plot(t,momentm(:,5,1)+momentm(:,5,2),'k--','LineWidth',2)
legend('m^x_1+m^x_2', 'm^y_1+m^y_2', 'm^z_1+m^z_2', 'FontSize',16)
ylabel('m_1+m_2 ','FontSize',16)
axis([0 max(t) -0.2 2.2])
box on
if strcmp(aunits,'Y')
xlabel('Time steps (a.u.)','FontSize',16)
else
xlabel('Time steps (ps)','FontSize',16)
end
box on
set(gca,'FontSize',16)
print -depsc2 magtraj1
end


% Spin dynamics trajectories
if strcmp(mode,'R')
figure(6); clf; hold on
plot(t,momentm(:,3,1),'r','LineWidth',2)
plot(t,momentm(:,4,1),'g:','LineWidth',2)
plot(t,momentm(:,5,1),'b--','LineWidth',2)
legend('m^x_1', 'm^y_1', 'm^z_1', 'FontSize',16)
ylabel('m_1 ','FontSize',16)
if strcmp(aunits,'Y')
xlabel('Time steps (a.u.)','FontSize',16)
else
xlabel('Time steps (ps)','FontSize',16)
end
axis([0 max(t) -1.1 1.1])
box on
set(gca,'FontSize',16)
print -depsc2 magtraj2
end


% Spin-lattice dynamics trajectories
if strcmp(mode,'R')
figure(7); clf
subplot(2,1,1); hold on
plot(t,momentm(:,3,1),'r','LineWidth',2)
plot(t,momentm(:,4,1),'g:','LineWidth',2)
plot(t,momentm(:,5,1),'b--','LineWidth',2)
legend('m^x_1', 'm^y_1', 'm^z_1', 'FontSize',16)
ylabel('m_1 ','FontSize',16)
aa=axis;
aa(1)=0;
aa(2)=max(t);
aa(3)=aa(3)*1.1;
aa(4)=aa(4)*1.1;
axis(aa)
box on
set(gca,'FontSize',16)
subplot(2,1,2); hold on
plot(t,dispm(:,3,1),'b','LineWidth',2)
plot(t,dispm(:,3,2),'r','LineWidth',2)
plot(t,dispm(:,3,1)+dispm(:,3,2),'k','LineWidth',2)
if strcmp(aunits,'Y')
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Displacement along x (a.u.)','FontSize',16)
else
xlabel('Time steps (ps)','FontSize',16)
ylabel('Displacement along x (Angstrom)','FontSize',16)
end
legend('Ion 1', 'Ion 2', 'Ion 1 and 2','FontSize',16)
axis tight
aa=axis;
aa(1)=0;
aa(2)=max(t);
aa(3)=aa(3)*1.1;
aa(4)=aa(4)*1.1;
axis(aa)
box on
set(gca,'FontSize',16)
print -depsc2 dispmagtraj1
end


% Spin-lattice dynamics energies
if strcmp(mode,'R')
%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
figure(8); clf; hold on
subplot(2,1,1); hold on
plot(t,lattenergy(:,3),'b','LineWidth',2)
legend('V_{SD}','FontSize',16)
if strcmp(aunits,'Y')
ylabel('Magnetic energy (a.u.)','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
else
ylabel('Magnetic energy (mRyd)','FontSize',16)
xlabel('Time steps (ps)','FontSize',16)
end
axis tight
aa=axis;
aa(1)=0;
aa(2)=max(t);
%aa(3)=aa(3)*1.1-0.01;
%aa(4)=aa(4)*1.1+0.01;
axis(aa)
box on
set(gca,'FontSize',16)
%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
subplot(2,1,2); hold on
plot(t,lattenergy(:,7),'k','LineWidth',2)
legend('E_{tot}','FontSize',16)
if strcmp(aunits,'Y')
ylabel('Energy (a.u.)','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
else 
ylabel('Energy (mRyd)','FontSize',16)
xlabel('Time steps (ps)','FontSize',16)
end
axis tight
aa=axis;
aa(1)=0;
aa(2)=max(t);
%aa(3)=aa(3)*1.1-0.01;
%aa(4)=aa(4)*1.1+0.01;
axis(aa)
box on
set(gca,'FontSize',16)
print -depsc2 sldenergy3
end
