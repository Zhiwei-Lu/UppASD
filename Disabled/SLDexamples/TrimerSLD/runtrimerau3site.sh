#!/bin/bash

mkdir Trimerau3site
cd Trimerau3site
cp -p ../Base/* .
sed -i "s/AUNITS/Y/" inpsd.dat
sed -i "s/#mml/mml/" inpsd.dat
sed -i "s/MMLFILE/mmlfile3site/" inpsd.dat
sed -i "s/NSTEP/10000000/" inpsd.dat
sed -i "s/TIMESTEP/1e-3/" inpsd.dat
sed -i "s/DAMPING/0.00/" inpsd.dat
sed -i "s/LATTDAMP/0.00/" inpsd.dat
sed -i "s/TEMP/0.00/" inpsd.dat
sed -i "s/SAMPSTEP/100/" inpsd.dat

sed -i "s/MODE/R/" plotTrimerSLD.m
sed -i "s/AUNITS/Y/" plotTrimerSLD.m
sed -i "s/NSTEP/100000/" plotTrimerSLD.m
sed -i "s/TIMESTEP/1e-3/" plotTrimerSLD.m
sed -i "s/LATTDAMP/0.00/" plotTrimerSLD.m
sed -i "s/SAMPSTEP/100/" plotTrimerSLD.m

../../../source/sd > out.log
cd ..