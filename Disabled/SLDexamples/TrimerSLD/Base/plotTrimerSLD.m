%Plot displacements and velocities for a MML-coupled system
clear

load disptraj.trimeSLD.1.1.out
load disptraj.trimeSLD.2.1.out
load disptraj.trimeSLD.3.1.out

traj(:,:,1)=disptraj_trimeSLD_1_1;
traj(:,:,2)=disptraj_trimeSLD_2_1;
traj(:,:,3)=disptraj_trimeSLD_3_1;

[tmpstr,totenergy]=hdrload('totenergy.trimeSLD.out');
[tmpstr,lattenergy]=hdrload('lattenergy.trimeSLD.out');

load moment.trimeSLD.out
moment=moment_trimeSLD;

[na,nb]=size(moment)
Natom=3
ne=na/Natom

%dispm=zeros(ne,8,Natom);
momentm=zeros(ne,6,Natom);
for i=1:ne
    for j=1:Natom
        ia=(i-1)*Natom+j;
        %dispm(i,1:8,j) = disp(ia,1:8);
        momentm(i,1:6,j) = moment(ia,1:6);
    end
end

figure(1); clf
plot(traj(:,3,1),'b','LineWidth',2)
hold on
plot(traj(:,3,2),'r','LineWidth',2)
plot(traj(:,3,3),'g','LineWidth',2)
plot(traj(:,3,1)+traj(:,3,2)+traj(:,3,3),'k','LineWidth',2)
%plot(x,y,'m')
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Displacement along x','FontSize',16)
legend('Ion 1', 'Ion 2', 'Ion 3', 'Ion 1+2+3','FontSize',16)
axis tight
box on
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*1.1
aa(4)=aa(4)*1.1
axis(aa)
print -depsc2 xdisp


figure(2); clf
plot(traj(:,6,1),'b','LineWidth',2)
hold on
plot(traj(:,6,2),'r','LineWidth',2)
plot(traj(:,6,3),'g','LineWidth',2)
plot(traj(:,6,1)+traj(:,6,2)+traj(:,6,3),'k','LineWidth',2)
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Momentum along x','FontSize',16)
legend('Ion 1', 'Ion 2', 'Ion 3', 'Ion 1+2+3','FontSize',16)
axis tight
box on
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*1.1
aa(4)=aa(4)*1.1
axis(aa)
print -depsc2 xvel


figure(3); clf
plot(traj(:,4,1),'b','LineWidth',2)
hold on
plot(traj(:,4,2),'r','LineWidth',2)
plot(traj(:,4,3),'g','LineWidth',2)
plot(traj(:,4,1)+traj(:,4,2)+traj(:,4,3),'k','LineWidth',2)
%plot(x,y,'m')
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Displacement along y','FontSize',16)
legend('Ion 1', 'Ion 2', 'Ion 3', 'Ion 1+2+3','FontSize',16)
axis tight
box on
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*1.1
aa(4)=aa(4)*1.1
axis(aa)
print -depsc2 xdisp


figure(4); clf
plot(traj(:,7,1),'b','LineWidth',2)
hold on
plot(traj(:,7,2),'r','LineWidth',2)
plot(traj(:,7,3),'g','LineWidth',2)
plot(traj(:,7,1)+traj(:,7,2)+traj(:,7,3),'k','LineWidth',2)
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Momentum along y','FontSize',16)
legend('Ion 1', 'Ion 2', 'Ion 3', 'Ion 1+2+3','FontSize',16)
axis tight
box on
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*1.1
aa(4)=aa(4)*1.1
axis(aa)
print -depsc2 yvel


figure(5); clf
subplot(4,1,1); hold on
plot(momentm(:,3,1),'b','LineWidth',2)
plot(momentm(:,4,1),'b:','LineWidth',2)
plot(momentm(:,5,1),'b--','LineWidth',2)
legend('m^x_1', 'm^y_1', 'm^z_1', 'FontSize',16)
ylabel('m_1 ','FontSize',16)
axis([0 200 -0.5 1.2])
box on
subplot(4,1,2); hold on
plot(momentm(:,3,2),'r','LineWidth',2)
plot(momentm(:,4,2),'r:','LineWidth',2)
plot(momentm(:,5,2),'r--','LineWidth',2)
legend('m^x_1', 'm^y_1', 'm^z_1', 'FontSize',16)
ylabel('m_2 ','FontSize',16)
axis([0 200 -0.5 1.2])
box on
subplot(4,1,3); hold on
plot(momentm(:,3,3),'g','LineWidth',2)
plot(momentm(:,4,3),'g:','LineWidth',2)
plot(momentm(:,5,3),'g--','LineWidth',2)
legend('m^x_3', 'm^y_3', 'm^z_3', 'FontSize',16)
ylabel('m_3 ','FontSize',16)
axis([0 200 -0.5 1.2])
box on
subplot(4,1,4); hold on
plot(momentm(:,3,1)+momentm(:,3,2)+momentm(:,3,3),'k','LineWidth',2)
plot(momentm(:,4,1)+momentm(:,4,2)+momentm(:,4,3),'k:','LineWidth',2)
plot(momentm(:,5,1)+momentm(:,5,2)+momentm(:,5,3),'k--','LineWidth',2)
legend('m^x', 'm^y', 'm^z', 'FontSize',16)
ylabel('m','FontSize',16)
axis([0 200 -0.2 2.2])
box on
xlabel('Time steps (a.u.)','FontSize',16)
%legend('m^y_1', 'm^z_1', 'm^y_2', 'm^z_2', 'm^x_1+m^x_2', 'm^y_1+m^y_2', 'm^z_1+m^z_2', 'FontSize',16)
%legend('m^x_1', 'm^y_1', 'm^z_1', 'm^x_2', 'm^y_2', 'm^z_2', 'm^y_1+m^y_2', 'FontSize',16)
%legend('Ion 1', 'Ion 2', 'Ion 1 and 2','FontSize',16)
box on
set(gca,'FontSize',16)
print -depsc2 magtraj1


figure(6); clf; hold on
plot(momentm(:,3,1),'b','LineWidth',2)
plot(momentm(:,4,1),'b:','LineWidth',2)
plot(momentm(:,5,1),'b--','LineWidth',2)
plot(momentm(:,3,2),'r','LineWidth',2)
plot(momentm(:,4,2),'r:','LineWidth',2)
plot(momentm(:,5,2),'r--','LineWidth',2)
plot(momentm(:,3,3),'g','LineWidth',2)
plot(momentm(:,4,3),'g:','LineWidth',2)
plot(momentm(:,5,3),'g--','LineWidth',2)
%plot(momentm(:,3,1)+momentm(:,3,2)+momentm(:,3,3),'k','LineWidth',2)
%plot(momentm(:,4,1)+momentm(:,4,2)+momentm(:,4,3),'k:','LineWidth',2)
%plot(momentm(:,5,1)+momentm(:,5,2)+momentm(:,5,3),'k--','LineWidth',2)
axis([0 200 -0.5 1.2])
legend('m^x_1', 'm^y_1', 'm^z_1', 'm^x_2', 'm^y_2', 'm^z_2', 'm^x_3', 'm^y_3', 'm^z_3', 'FontSize',16)
%legend('m^x', 'm^y', 'm^z', 'FontSize',16)
%axis([0 200 -0.2 2.2])
box on
ylabel('m','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
set(gca,'FontSize',16)
print -depsc2 magtraj2


%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
figure(7); clf; hold on
plot(lattenergy(:,2),'b','LineWidth',2)
plot(lattenergy(:,3),'r','LineWidth',2)
plot(lattenergy(:,4),'g','LineWidth',2)
plot(lattenergy(:,6),'m','LineWidth',2)
plot(lattenergy(:,7),'k','LineWidth',2)
%plot(x,y,'m')
xlabel('Time steps (a.u.)','FontSize',16)
ylabel('Energy','FontSize',16)
legend('V_{LD}', 'V_{SD}', 'V_{SLD}', 'T', 'E_{tot}', 'FontSize',16)
box on
axis tight
axis tight
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*0.9
aa(4)=aa(4)*1.1
axis(aa)
%axis([0 200 0 5e-4])
set(gca,'FontSize',16)
print -depsc2 sldenergy1


%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
figure(8); clf; hold on
subplot(2,1,1); hold on
plot(lattenergy(:,2),'b','LineWidth',2)
plot(lattenergy(:,6),'r','LineWidth',2)
plot(lattenergy(:,2)+lattenergy(:,6),'k','LineWidth',2)
ylabel('Ionic energy','FontSize',16)
legend('V_{LD}', 'T', 'V_{LD}+T','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
axis tight
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*0.9
aa(4)=aa(4)*1.1
axis(aa)
box on
set(gca,'FontSize',16)

%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
subplot(2,1,2); hold on
plot(lattenergy(:,7),'k','LineWidth',2)
ylabel('Energy','FontSize',16)
legend('E_{tot}','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
axis tight
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*0.9
aa(4)=aa(4)*1.1
axis(aa)
box on
set(gca,'FontSize',16)
print -depsc2 sldenergy2

%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
figure(9); clf; hold on
subplot(2,1,1); hold on
plot(lattenergy(:,3),'b','LineWidth',2)
ylabel('Magnetic energy','FontSize',16)
legend('V_{SD}','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
axis tight
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*0.9
aa(4)=aa(4)*1.1
axis(aa)
box on
set(gca,'FontSize',16)

%write (ofileno3,10004) int(indxb_uavrg(i)), ldpotenrgm, sdpotenrgm, sldpotenrgm, totpotenrgm, kinenrgm, totenrgm, totenrgs
subplot(2,1,2); hold on
plot(lattenergy(:,7),'k','LineWidth',2)
ylabel('Energy','FontSize',16)
legend('E_{tot}','FontSize',16)
xlabel('Time steps (a.u.)','FontSize',16)
axis tight
aa=axis;
aa(1)=0;
aa(2)=200;
aa(3)=aa(3)*0.9
aa(4)=aa(4)*1.1
axis(aa)
box on
set(gca,'FontSize',16)
print -depsc2 sldenergy3
