#!/bin/bash

# Setup of simulation input files
mkdir bccFMN4SLDpm
cd bccFMN4SLDpm
cp -p ../Base/* .
mv inpsd.dat.pm inpsd.dat
sed -i "s/NX/4/g" inpsd.dat
sed -i "s/AUNITS/N/" inpsd.dat
sed -i "s/DO_N3/Y/" inpsd.dat
sed -i "s/#mml/mml/" inpsd.dat
sed -i "s/MMLFILE/mmlfile100/" inpsd.dat
sed -i "s/NSTEP/10000000/" inpsd.dat
sed -i "s/TIMESTEP/1e-18/" inpsd.dat
sed -i "s/SAMPSTEP/10/" inpsd.dat
sed -i "s/QFILE/qfileN10/" inpsd.dat

# Damping and temperature
sed -i "s/DAMPING/0.00/" inpsd.dat
sed -i "s/LATTDAMP/0.00/" inpsd.dat
sed -i "s/TEMP/1.00/" inpsd.dat

# Multiple replicas
#sed -i "s/#Mensemble/Mensemble/" inpsd.dat
#sed -i "s/MENSEMBLE/16/" inpsd.dat

# Initial SLD Langevin dynamics phase
#sed -i "s/#ip_mode/ip_mode/" inpsd.dat
#sed -i "s/IP_MODE/R/" inpsd.dat
#sed -i "s/#ip_nphase/ip_nphase/" inpsd.dat
#sed -i "s/#IPSTEP/100000/" inpsd.dat
#sed -i "s/#iplattdamp/iplattdamp/" inpsd.dat
#sed -i "s/IPLDDAMP/1e-15/" inpsd.dat

# Initial SLD Monte Carlo mode
#sed -i "s/#ip_mode/ip_mode/" inpsd.dat
#sed -i "s/IP_MODE/B/" inpsd.dat
#sed -i "s/#ip_temp/ip_temp/" inpsd.dat
#sed -i "s/#IPSTEP/100000/" inpsd.dat

# Canonical velocity rescaling
#sed -i "s/DO_VELRSC/Y/" inpsd.dat
#sed -i "s/#velrsc_step/velrsc_step/" inpsd.dat
#sed -i "s/#velrsc_taut/velrsc_taut/" inpsd.dat

# Setup of Matlab plot scripts
sed -i "s/MODE/R/" *.m
sed -i "s/AUNITS/N/" *.m
sed -i "s/NSTEP/200000/" *.m
sed -i "s/TIMESTEP/1e-16/" *.m
sed -i "s/LATTDAMP/0.00/" *.m
sed -i "s/SAMPSTEP/10/" *.m
sed -i "s/NW/10000/" *.m
sed -i "s/NQ/21/" *.m

../../../source/sd > out.log
cd ..