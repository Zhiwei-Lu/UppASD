#!/bin/bash

# Setup of simulation input files
mkdir bccFMN10field
cd bccFMN10field
cp -p ../Base/* .
sed -i "s/NX/10/g" inpsd.dat
sed -i "s/AUNITS/N/" inpsd.dat
sed -i "s/#mml/mml/" inpsd.dat
sed -i "s/MMLFILE/mmlfile001/" inpsd.dat
sed -i "s/NSTEP/2000/" inpsd.dat
sed -i "s/TIMESTEP/1e-17/" inpsd.dat
sed -i "s/SAMPSTEP/100/" inpsd.dat
sed -i "s/QFILE/qfileN10/" inpsd.dat

# Damping and temperature
sed -i "s/DAMPING/0.00/" inpsd.dat
sed -i "s/LATTDAMP/0.00/" inpsd.dat
sed -i "s/TEMP/1.00/" inpsd.dat

sed -i "s/#do_prn_eeff/do_prn_eeff/" inpsd.dat
sed -i "s/#eeff_step/eeff_step/" inpsd.dat
sed -i "s/#eeff_buff/eeff_buff/" inpsd.dat

sed -i "s/#do_prn_einteff/do_prn_einteff/" inpsd.dat
sed -i "s/#einteff_step/einteff_step/" inpsd.dat
sed -i "s/#einteff_buff/einteff_buff/" inpsd.dat

#do_prn_eeff  Y
#eeff_step    SAMPSTEP
#eeff_buff    10

#do_prn_einteff  Y
#einteff_step    SAMPSTEP
#einteff_buff    10

# Multiple replicas
#sed -i "s/#Mensemble/Mensemble/" inpsd.dat
#sed -i "s/MENSEMBLE/16/" inpsd.dat

# Initial SLD Langevin dynamics phase
sed -i "s/#ip_mode/ip_mode/" inpsd.dat
sed -i "s/IP_MODE/R/" inpsd.dat
sed -i "s/#ip_nphase/ip_nphase/" inpsd.dat
sed -i "s/#IPSTEP/1000/" inpsd.dat
sed -i "s/#iplattdamp/iplattdamp/" inpsd.dat
sed -i "s/IPLDDAMP/1e-15/" inpsd.dat

# Initial SLD Monte Carlo mode
#sed -i "s/#ip_mode/ip_mode/" inpsd.dat
#sed -i "s/IP_MODE/B/" inpsd.dat
#sed -i "s/#ip_temp/ip_temp/" inpsd.dat
#sed -i "s/#IPSTEP/100000/" inpsd.dat

# Canonical velocity rescaling
#sed -i "s/DO_VELRSC/Y/" inpsd.dat
#sed -i "s/#velrsc_step/velrsc_step/" inpsd.dat
#sed -i "s/#velrsc_taut/velrsc_taut/" inpsd.dat

# Setup of Matlab plot scripts
sed -i "s/MODE/R/" *.m
sed -i "s/AUNITS/N/" *.m
sed -i "s/NSTEP/20000/" *.m
sed -i "s/TIMESTEP/1e-17/" *.m
sed -i "s/LATTDAMP/0.00/" *.m
sed -i "s/SAMPSTEP/100/" *.m
sed -i "s/NW/1000/" *.m
sed -i "s/NQ/21/" *.m

../../../source/sd > out.log
cd ..