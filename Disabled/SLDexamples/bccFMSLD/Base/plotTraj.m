% Plots spin trajectories

% Clear variables
clear

% Flags to govern script execution
mode = 'MODE'
aunits = 'AUNITS'

load trajectory.bccFMSLD.1.1.out
trajectory1=trajectory_bccFMSLD_1_1;
load trajectory.bccFMSLD.2.1.out
trajectory2=trajectory_bccFMSLD_2_1;

%Make sure Nw, Nq, Ts matches entries in inpsd.dat
Nw=NW % uc_nsteps Number of frequencies (=number of samples)
Nq=NQ % Number of q-values
Nqhalf = Nq/2
t0=1;
t1=Nw;
stepsize = SAMPSTEP
delta_t = TIMESTEP
Ts=stepsize * delta_t
if strcmp(aunits,'Y')
f=(1:Nw)/(Nw*Ts);
t=(t0:t1)*Ts;
else
f=(1:Nw)/(Nw*Ts)*1e-12; % in THz
t=(t0:t1)*Ts*1e12; % in ps
end

figure(1);clf
plot(t(t0:t1),trajectory1(t0:t1,3),'r','LineWidth',2)
hold on
plot(t(t0:t1),trajectory1(t0:t1,4),'g','LineWidth',2)
plot(t(t0:t1),trajectory1(t0:t1,5),'b','LineWidth',2)
box on
legend('m^x','m^y','m^z', 'Location','SouthEast','FontSize',16)
if strcmp(aunits,'Y')
xlabel('t (a.u.)','FontSize',16)
else
xlabel('t (ps)','FontSize',16)
end
ylabel('Magnetic moment','FontSize',16)
set(gca,'LineWidth',2)
set(gca,'FontSize',16)
print -depsc2 traj1
