%Plots the velocity-velocity dynamic structure factor S(q,w)
%Reads the vqw-file

% Clear variables
clear

% Flags to govern script execution
mode = 'MODE'
aunits = 'AUNITS'

%Make sure Nw, Nq, Ts matches entries in inpsd.dat
Nw=NW % uc_nsteps Number of frequencies (=number of samples)
Nq=NQ % Number of q-values
Nqhalf = Nq/2
stepsize = SAMPSTEP
delta_t = TIMESTEP
Ts=stepsize * delta_t
if strcmp(aunits,'Y')
f=(1:Nw)/(Nw*Ts);
else
f=(1:Nw)/(Nw*Ts)*1e-12; % in THz
end

load phondisp.bccFMSLD.out
phondisp_bccFMSLD = phondisp_bccFMSLD;
load qpoints.out

figure(12); clf
plot(qpoints(:,1),phondisp_bccFMSLD(:,2:4),'.-','LineWidth',2)
xlabel('Wavevector Q','FontSize',16)
ylabel('w(Q) meV','FontSize',16)
box on
set(gca,'FontSize',16)
set(gca,'LineWidth',2)
if Nq==21
set(gca,'XTick',[1 6 11 16 21])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'H' 'P' 'G' 'N']')%'
axis tight
print -depsc2 phondisp

disp('Start reading sqw file')
load vqw.bccFMSLD.out
sqw=vqw_bccFMSLD;
for i=1:Nq
ns=(i-1)*Nw+1;
ne=i*Nw;
sqwm(:,:,i)=sqw(ns:ne,:);
end
disp('Finished reading sqw file')

%Normalize separately each component of S(q,w) for each q-value
for j=6:9
for i=1:Nq
sqwmmax = max(max(sqwm(1:Nw,j,i)));
sqwm(:,j,i)=sqwm(:,j,i)/sqwmmax;
end
end
disp('Finished normalizing sqw')
sqwmav = sqwm;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) for fixed q as a function of w
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%which q-values to plot for
for i=1:6
figure(50+i)
clf
hold on
plot(f,sqwmav(:,9,i),'k','LineWidth',2)
plot(f,sqwmav(:,6,i),'.r','LineWidth',2)
plot(f,sqwmav(:,7,i),'.-g','LineWidth',2)
plot(f,sqwmav(:,8,i),':b','LineWidth',2)
legend('S(q,\omega)','S^x(q,\omega)','S^y(q,\omega)','S^z(q,\omega)', 'Location','SouthEast','FontSize',16)
xlabel('f (THz)','FontSize',16)
ylabel('Normalized structure factor S(q,\omega)','FontSize',16)
box on
axis ([0 f(Nw) 0 1])
set(gca,'FontSize',16)
prnstr=['print -depsc2 vqw' num2str(i-1)];
eval(prnstr);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Common variables for S(q,w) plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

hbar=6.582e-13; %in meVs
fmax=Nw/4
qmin=1
qmax=Nq %full range, plots for all q-values


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in frequency f (THz)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(41)
clf
hold on

y=f(1:fmax);
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,6,qmin:qmax));
surf(X,Y,Z)
grid on
box on
axis([qmin qmax f(1) f(fmax)])
xlabel('Wave vector q')
ylabel('f (THz)','FontSize',16)
set(gca,'FontSize',16)
shading interp
print -depsc2 sqwvxthz


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in energy (meV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(42)
clf
hold on

y=f(1:fmax)*2*pi*hbar*1e12;
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,6,qmin:qmax));
surf(X,Y,Z)
grid on
box on
axis([qmin qmax f(1) f(fmax)*2*pi*hbar*1e12])
xlabel('Wave vector q')
ylabel('E (meV)','FontSize',16)
set(gca,'FontSize',16)
if Nq==21
set(gca,'XTick',[1 6 11 16 21])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'H' 'P' 'G' 'N']')%'
set(gca,'XTick',[1 6 11 16 21])
set(gca,'XTickLabel',['G' 'H' 'P' 'G' 'N']')%'
shading interp
print -depsc2 sqwvxmev


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in energy (meV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(43)
clf
hold on

y=f(1:fmax)*2*pi*hbar*1e12;
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,6,qmin:qmax));
surf(X,Y,Z)
grid on
box on
axis([qmin qmax f(1) f(fmax)*2*pi*hbar*1e12])
xlabel('Wave vector q')
ylabel('E (meV)','FontSize',16)
set(gca,'FontSize',16)
if Nq==21
set(gca,'XTick',[1 6 11 16 21])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'H' 'P' 'G' 'N']')%'
shading interp
print -depsc2 sqwvxmev


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot S(q,w) dispersion relation as a 2D-plot.
%The y-axis is in energy (meV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(44)
clf
hold on

y=f(1:fmax)*2*pi*hbar*1e12;
x=qmin:qmax;
[X,Y]=meshgrid(x,y);
Z=squeeze(sqwmav(1:fmax,7,qmin:qmax));
surf(X,Y,Z)
grid on
box on
axis([qmin qmax f(1) f(fmax)*2*pi*hbar*1e12])
xlabel('Wave vector q')
ylabel('E (meV)','FontSize',16)
set(gca,'FontSize',16)
if Nq==21
set(gca,'XTick',[1 6 11 16 21])
elseif Nq==31
set(gca,'XTick',[1 11 16 21 31])
else
set(gca,'XTick',[1 51 76 101 151])
end
set(gca,'XTickLabel',['G' 'H' 'P' 'G' 'N']')%'
shading interp
print -depsc2 sqwvzmev
