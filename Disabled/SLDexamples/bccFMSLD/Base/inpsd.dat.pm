simid     bccFMSLD
ncell     NX        NX        NX                System size            
BC        P         P         P                 Boundary conditions (0=vacuum, P=periodic)
cell      -1.0000000000000003    1.0000000000000003    1.0000000000000003         
           1.0000000000000003   -1.0000000000000003    1.0000000000000003
           1.0000000000000003    1.0000000000000003   -1.0000000000000003
Sym       1                                     Symmetry of lattice
do_prnstruct 1
do_hoc_debug 0
aunits    AUNITS
do_N3     DO_N3

posfiletype  D
posfile   ./posfile
momfile   ./momfile
exchange  ./jfileDIr6dir.dat
Initmag   1
#restartfile ./restart.dat

phonfile  ./phonfile
ll        ./llfile
ll_phonopy  ./FORCE_CONSTANTS
ll_phonopycoordfile  ./POSCAR.dat
i0phonopy   43
radius_phonopy  0.35
scalefac_phonopy 4

#mml       ./MMLFILE
Initlatt  1
#lattrestartfile ./lattrestart.dat

#ip_mode    IP_MODE
#ip_nphase  1
#IPSTEP    TEMP   1e-16   0.1

#ip_temp     TEMP
#ip_mcNstep  IPSTEP
#iplattdamp  IPLDDAMP

do_ld     Y
mode      R
Nstep     NSTEP
timestep  TIMESTEP
damping   DAMPING
lattdamp  LATTDAMP
Temp      TEMP
#Mensemble  MENSEMBLE

#do_velrsc    DO_VELRSC
#velrsc_step  1
#velrsc_taut  10

lntraj     2
1    SAMPSTEP    10
2    SAMPSTEP    10

#do_prn_eeff  Y
#eeff_step    SAMPSTEP
#eeff_buff    10

#do_prn_einteff  Y
#einteff_step    SAMPSTEP
#einteff_buff    10

#do_ltottraj Y
#ltottraj_step SAMPSTEP
#ltottraj_buff 10

do_lavrg   Y
do_proj_lavrg   Y
lavrg_step   SAMPSTEP 
lavrg_buff   10

#do_prn_beff  Y
#beff_step    SAMPSTEP
#beff_buff    10

#do_prn_binteff  Y
#binteff_step    SAMPSTEP
#binteff_buff    10

ntraj     2
1    SAMPSTEP    10
2    SAMPSTEP    10

do_avrg   Y
do_proj_avrg   Y
avrg_step   SAMPSTEP
avrg_buff   10

plotenergy   1
do_spintemp  Y

do_ams      Y
do_magdos   Y

do_phonspec Y
do_phondos  Y

do_sc Q                                         Measure spin correlation
sc_nstep 1000                                   Number of steps to sample
sc_step SAMPSTEP                                Number of time steps between each sampling
sc_sep SAMPSTEP                                 Number of time steps between the start of subsequent sc measurements
qpoints D                                       Flag for q-point generation (F=file,A=automatic,C=full cell)
qfile   ./QFILE

do_uc Q                                         Measure spin correlation
uc_nstep 1000                                   Number of steps to sample
uc_step SAMPSTEP                                Number of time steps between each sampling
uc_sep SAMPSTEP                                 Number of time steps between the start of subsequent sc measurements
