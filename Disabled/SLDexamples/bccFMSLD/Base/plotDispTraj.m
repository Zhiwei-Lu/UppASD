% Plots displacement and velocity trajectories

% Clear variables
clear

% Flags to govern script execution
mode = 'MODE'
aunits = 'AUNITS'

load disptraj.bccFMSLD.1.1.out
disptraj1=disptraj_bccFMSLD_1_1;
load disptraj.bccFMSLD.2.1.out
disptraj2=disptraj_bccFMSLD_2_1;

%Make sure Nw, Nq, Ts matches entries in inpsd.dat
Nw=NW % uc_nsteps Number of frequencies (=number of samples)
Nq=NQ % Number of q-values
Nqhalf = Nq/2
t0=1;
t1=Nw;
stepsize = SAMPSTEP
delta_t = TIMESTEP
Ts=stepsize * delta_t
if strcmp(aunits,'Y')
f=(1:Nw)/(Nw*Ts);
t=(t0:t1)*Ts;
else
f=(1:Nw)/(Nw*Ts)*1e-12; % in THz
t=(t0:t1)*Ts*1e12; % in ps
end

figure(2);clf
plot(t(t0:t1),disptraj1(t0:t1,3),'r','LineWidth',2)
hold on
plot(t(t0:t1),disptraj1(t0:t1,4),'g','LineWidth',2)
plot(t(t0:t1),disptraj1(t0:t1,5),'b','LineWidth',2)
legend('u_x', 'u_y', 'u_z','FontSize',16)
if strcmp(aunits,'Y')
xlabel('t (a.u.)','FontSize',16)
ylabel('Displacement u (a.u.)','FontSize',16)
else
xlabel('t (ps)','FontSize',16)
ylabel('Displacement u (Angstrom)','FontSize',16)
end

axis tight
box on
set(gca,'FontSize',16)
print -depsc2 utraj1

figure(3);clf
plot(t(t0:t1),disptraj1(t0:t1,6),'r','LineWidth',2)
hold on
plot(t(t0:t1),disptraj1(t0:t1,7),'g','LineWidth',2)
plot(t(t0:t1),disptraj1(t0:t1,8),'b','LineWidth',2)
legend('v_x', 'v_y', 'v_z','FontSize',16)
if strcmp(aunits,'Y')
xlabel('t (a.u.)','FontSize',16)
ylabel('Velocity v (a.u.)','FontSize',16)
else
xlabel('t (ps)','FontSize',16)
ylabel('Velocity v (Angstrom/s)','FontSize',16)
end
axis tight
box on
set(gca,'FontSize',16)
print -depsc2 vtraj2
