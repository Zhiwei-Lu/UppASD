!> Data and routines for Monte Carlo simulations of spin-lattice Hamiltonians
!! Adopted from the MonteCarlo and MonteCarlo_Common modules for simulations
!! of spin Hamiltonians.
!> @author
!> J. Hellsvik, A. Bergman, L. Bergqvist, Fan Pan, Jonathan Chico
!> -----------------------------------------------------------------------------------
module LatticeMonteCarlo
   use Parameters
   use Profiling

   implicit none

   integer :: sldmcmstep !< Current Monte Carlo step
   integer :: ntrial !< Number of trial moves
   integer :: nsuccess !< Number of successful trial moves
   !
   integer, dimension(:), allocatable :: iflip_m !< Array of atoms to flip spins and displacements for

   private

   ! public variables
   public :: iflip_m, ntrial, nsuccess

   ! public subroutines
   public :: sld_mc_evolve, sld_choose_random_atom_x, sld_calculate_kinenergy, sld_allocate_mcdata


contains


   !--------------------------------------------------------------------------
   ! DESCRIPTION
   !> @brief
   !> Main driver for Monte Carlo simulations of spin-lattice Hamiltonians using the Metropolis algorithm
   !---------------------------------------------------------------------------------
   !subroutine sld_mc_evolve(Natom, k, k, iflip, iflip, &
   subroutine sld_mc_evolve(Natom, Mensemble, temperature, temprescale, &
         do_ll, do_lll, do_llll, do_ml, do_mml, do_mmll, mode, &
         uvec, uvec2, emomM, emom, emom2, mmom, latt_external_field, &
         latt_time_external_field, external_field, time_external_field, &
         ll_energy, lll_energy, llll_energy, ml_energy, mml_energy, mmll_energy, &
           ldpot_energy, sdpot_energy, sldpot_energy, totpot_energy, sld_single_energy, &
           mm_energy0, ammom_inp, aemom_inp, NA)
           !ldpot_energy, sdpot_energy, sldpot_energy, totpot_energy, sld_single_energy)
      !
      use RandomNumbers, only: rng_uniformP, rng_gaussian, use_vsl
      use LatticeHamiltonianActions
      use LatticeInputData, only : uvec_inp

      !
      implicit none

      integer, intent(in) :: Natom     !< Number of atoms in system
      integer, intent(in) :: Mensemble !< Number of ensembles
      real(dblprec), intent(in) :: temperature !< Temperature
      real(dblprec), intent(in) :: temprescale !< Temperature rescaling according to QHB

      integer, intent(in) :: do_ll   !< Add LL term to lattice Hamiltonian (0/1)
      integer, intent(in) :: do_lll   !< Add LLL term to lattice Hamiltonian (0/1)
      integer, intent(in) :: do_llll   !< Add LLLL term to lattice Hamiltonian (0/1)
      integer, intent(in) :: do_ml   !< Add ML term to lattice Hamiltonian (0/1)
      integer, intent(in) :: do_mml   !< Add MML term to lattice Hamiltonian (0/1)
      integer, intent(in) :: do_mmll   !< Add MMLL term to lattice Hamiltonian (0/1)
      character(len=1) :: mode   !< Simulation mode (S=SD, M=MC, H=MC Heat Bath, P=LD, R=SLD, B=SLD MC)

      real(dblprec), dimension(3,Natom,Mensemble), intent(inout) :: uvec   !< Current ionic displacement
      real(dblprec), dimension(3,Natom,Mensemble), intent(inout) :: uvec2  !< Temporary ionic displacement
      real(dblprec), dimension(3,Natom,Mensemble), intent(inout) :: emomM  !< Current magnetic moment vector
      real(dblprec), dimension(3,Natom,Mensemble), intent(inout) :: emom   !< Current unit moment vector
      real(dblprec), dimension(3,Natom,Mensemble), intent(inout) :: emom2  !< Temporary unit moment vector
      real(dblprec), dimension(Natom,Mensemble), intent(inout) :: mmom !< Magnitude of magnetic moments

      real(dblprec), dimension(3,Natom,Mensemble), intent(in) :: latt_external_field !< External electric field
      real(dblprec), dimension(3,Natom,Mensemble), intent(in) :: latt_time_external_field !< External time-dependent electric field
      real(dblprec), dimension(3,Natom,Mensemble), intent(in) :: external_field !< External magnetic field
      real(dblprec), dimension(3,Natom,Mensemble), intent(in) :: time_external_field !< External time-dependent magnetic field

      real(dblprec), dimension(Mensemble), intent(out) :: ll_energy        !< Total LL energy
      real(dblprec), dimension(Mensemble), intent(out) :: lll_energy       !< Total LLL energy
      real(dblprec), dimension(Mensemble), intent(out) :: llll_energy      !< Total LLLL energy
      real(dblprec), dimension(Mensemble), intent(out) :: ml_energy        !< Total ML energy
      real(dblprec), dimension(Mensemble), intent(out) :: mml_energy       !< Total MML energy
      real(dblprec), dimension(Mensemble), intent(out) :: mmll_energy      !< Total MMLL energy
      real(dblprec), dimension(Mensemble), intent(out) :: ldpot_energy     !< LD potential energy
      real(dblprec), dimension(Mensemble), intent(in)  :: sdpot_energy     !< SD potential energy
      real(dblprec), dimension(Mensemble), intent(out) :: sldpot_energy    !< SLD potential energy (without pure LD or SD potential energies)
      real(dblprec), dimension(Mensemble), intent(out) :: totpot_energy    !< Total potential energy: LD + SD + SLD. No kinetic energy!
      real(dblprec), dimension(Mensemble), intent(out) :: sld_single_energy !< Trial potential energy: LD + Heisenberg + SLD. No kinetic energy!
    real(dblprec), dimension(Mensemble), intent(out) :: mm_energy0        !< Total MM ground state energy     ***************************

    real(dblprec), intent(in), dimension(:,:,:) :: ammom_inp  !< Magnetic moment magnitudes from input (for alloys)
    real(dblprec), intent(in), allocatable, dimension(:,:,:,:) :: aemom_inp  !< Magnetic moment directions from input (for alloys)
    integer, intent(in) :: NA  !< Number of atoms in one cell

      !.. Local variables
      integer :: i, k

      !real(dblprec) :: de !< Energy difference
      real(dblprec) :: uvar !< Variance of random displacements

      !.. Local arrays
      real(dblprec), dimension(3) :: newmom !< New trial moment
      real(dblprec), dimension(3) :: newuvec !< New trial moment
      real(dblprec),dimension(natom,mensemble) :: flipprob_m
      real(dblprec),dimension(3,natom,mensemble) :: newmom_m
      real(dblprec),dimension(3,natom,mensemble) :: newuvec_m

      integer, external :: omp_get_thread_num

      call rng_uniformP(flipprob_m(:,:),natom*mensemble)

      ! Copy uvec to uvec2, and emom to emom2
      uvec2 = uvec
      emom2 = emom

      ! Change variable name, this is the variance
      uvar = uvec_inp(1,1,1)**2

      if(use_vsl) then

#ifdef VSL
         !!!$omp parallel do default(shared),private(i,k,newmom),schedule(auto),collapse(2)
#endif
         do i=1,Natom
            do k=1,mensemble
               call sld_choose_random_flip(Natom, Mensemble, emom, newmom, uvec, newuvec, uvar, i, k, temperature, flipprob_m(i,k))
               !call sld_choose_random_flip(Natom, Mensemble, emom, newmom, uvec(1:3,i,k), newuvec, uvar, i, k, temperature, flipprob_m(i,k))
               !call sld_choose_random_flip(Natom, Mensemble, emom, newmom, newuvec, uvar, i, k, temperature, flipprob_m(i,k))
               newmom_m(1:3,i,k) = newmom(1:3)
               newuvec_m(1:3,i,k) = newuvec(1:3)
            enddo
         enddo
#ifdef VSL
         !!!$omp end parallel do
#endif

      else

         do i=1,Natom
            do k=1,mensemble
               call sld_choose_random_flip(Natom, Mensemble, emom, newmom, uvec, newuvec, uvar, i, k, temperature, flipprob_m(i,k))
               !call sld_choose_random_flip(Natom, Mensemble, emom, newmom, uvec(1:3,i,k), newuvec, uvar, i, k, temperature, flipprob_m(i,k))
               !call sld_choose_random_flip(Natom, Mensemble, emom, newmom, newuvec, uvar, i, k, temperature, flipprob_m(i,k))
               newmom_m(1:3,i,k) = newmom(1:3)
               newuvec_m(1:3,i,k) = newuvec(1:3)
            enddo
         enddo

      end if


      ! Calculate energy and flip spin-lattice position if preferred
      ! The loop over sld_calculate_trialmove is at present not thread-safe
      !!!$omp parallel do default(shared), private(i,k), schedule(auto),collapse(2)
      do i=1, Natom
         do k=1,mensemble

            call sld_calculate_trialmove(i, k)

         enddo
      enddo
      !!!$omp end parallel do


   contains


      subroutine sld_calculate_trialmove(iflip, k)

         use Constants

         implicit none

         integer, intent(in) :: iflip !< Current atom
         integer, intent(in) :: k !< Current ensemble

         !.. Local scalars
         real(dblprec) :: de, current_energy, trial_energy
         real(dblprec) :: beta

         ! Calculate the current energy of particle i
         !write(*,'(a)') 'Current state'
         !write(*,'(a,3f10.6,a,3f10.6)') 'emomM ', emomM(1:3,iflip,k), ' uvec ', uvec(1:3,iflip,k)
         call calc_lattenergies(Natom, Mensemble, k, k, iflip, iflip, &
            do_ll, do_lll, do_llll, do_ml, do_mml, do_mmll, mode, &
            uvec, emomM, latt_external_field, latt_time_external_field, &
            ll_energy, lll_energy, llll_energy, ml_energy, mml_energy, mmll_energy, &
           ldpot_energy, sdpot_energy, sldpot_energy, totpot_energy, sld_single_energy, &
           mm_energy0, ammom_inp, aemom_inp, NA)
      !ldpot_energy, sdpot_energy, sldpot_energy, totpot_energy, sld_single_energy)
         current_energy = sld_single_energy(k)

         ! Copy the trial move to the primary state arrays emomM and uvec.
         ! This allows for the trial energy to be calculated with a call to calc_lattenergies
         emom(1:3,iflip,k) = newmom_m(1:3,iflip,k)
         emomM(1:3,iflip,k) = mmom(iflip,k)*newmom_m(1:3,iflip,k)
         uvec(1:3,iflip,k) = newuvec_m(1:3,iflip,k)

         ! Calculate the trial move energy of particle i
         !write(*,'(a)') 'Trial move'
         !write(*,'(a,3f10.6,a,3f10.6)') 'emomM ', emomM(1:3,iflip,k), ' uvec ', uvec(1:3,iflip,k)
         call calc_lattenergies(Natom, Mensemble, k, k, iflip, iflip, &
            do_ll, do_lll, do_llll, do_ml, do_mml, do_mmll, mode, &
            uvec, emomM, latt_external_field, latt_time_external_field, &
            ll_energy, lll_energy, llll_energy, ml_energy, mml_energy, mmll_energy, &
           ldpot_energy, sdpot_energy, sldpot_energy, totpot_energy, sld_single_energy, &
           mm_energy0, ammom_inp, aemom_inp, NA)
           !ldpot_energy, sdpot_energy, sldpot_energy, totpot_energy, sld_single_energy)
         trial_energy = sld_single_energy(k)

         ! Energy difference
         de = trial_energy - current_energy
         !!! TEST !!!
         !de = de / 2   ! THIS DIVISION BY TWO SEEMS TO BE NEEDED. STRANGE.
         !!! TEST !!!

         ! The Metropolis criterion
         beta=1_dblprec/k_bolt/(temprescale*Temperature+1.0d-15)
         !write(*,'(a,es16.8,a,es16.8,a,es16.8)') 'trial_energy ', trial_energy, ' current_energy ', current_energy, ' de ', de
         !write(*,'(a,f10.6,a,f10.6,a,f10.6)') 'trial_energy ', trial_energy, ' current_energy ', current_energy, ' de ', de
         !write(*,'(a,i8,a,f10.6,a,f10.6)') 'iflip ', iflip, ' beta ', beta, ' flipprob_m(iflip,k) ', flipprob_m(iflip,k)
         if(de<=0.0_dblprec .or. flipprob_m(iflip,k)<exp(-beta*de)) then
            ! Not needed, these were already copied
            !emom(1:3,iflip,k) = newmom_m(1:3,iflip,k)
            !emomM(1:3,iflip,k) = mmom(iflip,k)*newmom_m(1:3,iflip,k)
            !uvec(1:3,iflip,k) = newuvec_m(1:3,iflip,k)
            ! but the emom2 and uvec2 arrays need to be updated
            emom2(1:3,iflip,k) = newmom_m(1:3,iflip,k)
            uvec2(1:3,iflip,k) = newuvec_m(1:3,iflip,k)
            nsuccess = nsuccess + 1
         else
            ! If the trial move is rejected, copy back from emom2 and uvec2
            emom(1:3,iflip,k) = emom2(1:3,iflip,k)
            emomM(1:3,iflip,k) = mmom(iflip,k)*emom2(1:3,iflip,k)
            uvec(1:3,iflip,k) = uvec2(1:3,iflip,k)
         endif
         ntrial = ntrial + 1

      end subroutine sld_calculate_trialmove


   end subroutine sld_mc_evolve


   !> Sets up an array with flip pattern for trial spin-lattice flips
   subroutine sld_choose_random_atom_x(Natom,iflip_m)

      use RandomNumbers, only : rng_uniform

      !.. Implicit declarations
      implicit none

      integer, intent(in) :: Natom !< Number of atoms in system
      integer,dimension(natom),intent(out) :: iflip_m !< Flip pattern

      real(dblprec) :: dshift(natom)
      integer :: i,ishift,itmp

      do i=1,natom
         iflip_m(i)=i
      end do
      call rng_uniform(dshift,natom)
      do i=1,natom
         ishift=int(dshift(i)*Natom)
         itmp=iflip_m(i)
         iflip_m(i)=iflip_m(mod(ishift,Natom)+1)
         iflip_m(mod(ishift,Natom)+1)=itmp
      end do

   end subroutine sld_choose_random_atom_x


   !> Calculates a random spin direction
   subroutine sld_choose_random_flip(Natom, Mensemble, emom, newmom, olduvec, newuvec, uvar, iflip, k, temperature, rn)

      use RandomNumbers, only : rng_uniform,rng_gaussian
      use Constants,only : mub,k_bolt

      !.. Implicit declarations
      implicit none

      integer, intent(in) :: Natom !< Number of atoms in the sample
      integer, intent(in) :: Mensemble !< Number of ensembles
      real(dblprec), dimension(3,Natom,Mensemble), intent(in) :: emom   !< Current unit moment vector
      real(dblprec), dimension(3), intent(out) :: newmom  !< New trial moment
      real(dblprec), dimension(3,Natom,Mensemble), intent(in) :: olduvec  !< Current displacement (single ion)
      !real(dblprec), dimension(3), intent(in) :: olduvec  !< Current displacement (single ion)
      real(dblprec), dimension(3), intent(out) :: newuvec !< New trial displacement
      real(dblprec), intent(in):: uvar !< Variance of trial displacements
      integer, intent(in) :: iflip     !< Atom to flip
      integer, intent(in) :: k         !< current ensemble
      real(dblprec), intent(in):: temperature !< Temperature
      real(dblprec), intent(in):: rn !< Random number

      ! Local variables
      real(dblprec), dimension(3) :: unimom !< 3-vector of uniformly distributed pRN
      real(dblprec), dimension(3) :: gasmom !< 3-vector of Gaussian distributed pRN
      real(dblprec) :: newmoml, delta
      integer :: ftype

      ftype=floor(3*rn)

      !  Hinzke-Nowak update, random between uniform rotation, gaussian rotation and spin-flips
      !  LB 150306

      ! Uniform rotation
      if (ftype==0) then

         call rng_uniform(unimom,3)
         newmom(:)=2.0_dblprec*unimom(:)-1.0_dblprec
         do while (newmom(1)**2+newmom(2)**2+newmom(3)**2>1.00_dblprec .or. newmom(1)**2+newmom(2)**2+newmom(3)**2<dbl_tolerance)
            call rng_uniform(unimom,3)
            newmom(:)=2.0_dblprec*unimom(:)-1.0_dblprec
         end do
         newmoml=sqrt(newmom(1)**2+newmom(2)**2+newmom(3)**2)
         newmom(:) = newmom(:)/newmoml

         ! Gaussian rotation
      elseif (ftype==1) then

         delta=(2.0/25.0)*(k_bolt*temperature/mub)**(0.2_dblprec)
         call rng_gaussian(gasmom,3,delta)
         newmoml=sqrt((emom(1,iflip,k)+gasmom(1))**2+(emom(2,iflip,k)+gasmom(2))**2+(emom(3,iflip,k)+gasmom(3))**2)
         newmom(:)=(emom(:,iflip,k)+gasmom(:))/newmoml

         !spin-flip
      else

         newmom(:)=-emom(:,iflip,k)

      endif

      ! Trial move as suggested for the Lennard-Jones potential in Chapter 6
      ! of Landau and Binder, A guide to Monte Carlo simulations in statistical physics
      call rng_uniform(unimom,3)
      newuvec(1:3) = olduvec(1:3,iflip,k) + uvar * (2.0_dblprec*unimom(1:3)-1.0_dblprec)
      !newuvec(1:3) = olduvec(1:3) + uvar * (2.0_dblprec*unimom(1:3)-1.0_dblprec)
      !write(*,'(a,3f10.6)') 'uvar * 2.. ', uvar * (2.0_dblprec*unimom(1:3)-1.0_dblprec)
      !newuvec = olduvec + uvar * unimom

      ! Normal distributed trial move
      !call rng_gaussian(gasmom,3,uvar)
      !newuvec = olduvec + gasmom


   end subroutine sld_choose_random_flip


   subroutine sld_calculate_kinenergy(Natom, Mensemble, temperature, mioninv, vvec)

      use RandomNumbers, only : rng_gaussian
      use Constants, only : k_bolt, amu, angstrom

      !.. Implicit declarations
      implicit none

      integer, intent(in) :: Natom !< Number of atoms in the sample
      integer, intent(in) :: Mensemble !< Number of ensembles
      real(dblprec), intent(in) :: temperature !< Temperature
      real(dblprec), dimension(Natom,Mensemble), intent(in) :: mioninv   !< Inverse ionic mass
      real(dblprec), dimension(3,Natom,Mensemble), intent(out) :: vvec   !< Ionic velocity

      ! Local variables
      integer :: i, k
      real(dblprec), dimension(3) :: gasmom !< 3-vector of Gaussian distributed pRN
      real(dblprec) :: delta
      real(dblprec) amuinv
      real(dblprec) angstrominv

      amuinv = 1.0_dblprec/amu
      angstrominv = 1.0_dblprec/angstrom

      do i=1,Natom
         do k=1,mensemble

            delta = sqrt( k_bolt * temperature * angstrominv**2 * amuinv * mioninv(i,k) )
            !write(765,'(a,i8,a,i8,a,f10.6)') 'i ', i, ' k ', k, ' delta ', delta

            call rng_gaussian(gasmom,3,delta)
            vvec(1:3,i,k) = gasmom(1:3)
            !write(765,'(a,3f10.6)') 'vvec ', vvec(1:3,i,k)

         end do
      end do


   end subroutine sld_calculate_kinenergy


   !> Allocates SLD MC arrays
   subroutine sld_allocate_mcdata(Natom,flag)

      implicit none

      integer, intent(in) :: Natom !< Number of atoms in system
      integer, intent(in) :: flag !< Allocate or deallocate (1/-1)

      integer :: i_all, i_stat

      if(flag>0) then
         allocate(iflip_m(natom),stat=i_stat)
         call memocc(i_stat,product(shape(iflip_m))*kind(iflip_m),'iflip_m','sld_allocate_mcdata')
         ntrial = 0
         nsuccess = 0
      else
         i_all=-product(shape(iflip_m))*kind(iflip_m)
         deallocate(iflip_m,stat=i_stat)
         call memocc(i_stat,i_all,'iflip_m','sld_allocate_mcdata')
      endif
   end subroutine sld_allocate_mcdata


end module LatticeMonteCarlo
