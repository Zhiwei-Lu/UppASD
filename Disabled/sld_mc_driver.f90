!******************************************************************
!*                     *** UppASD ***                             *
!*                                                                *
!*               Uppsala Atomic Spin Dynamics                     *
!*                                                                *
!*                   Version 5.0 Mar 2017                         *
!*                                                                *
!*       Anders Bergman                                           *
!*       Johan Hellsvik             Lars Bergqvist                *
!*       Jonathan Chico             Thomas Nystrand               *
!*       Bjorn Skubic               Olle Eriksson                 *
!*       Andrea Taroni              Lars Nordstrom                *
!*       + other contributors.                                    *
!*                                                                *
!*       Division of Materials Theory                             *
!*       Department of Physics and Materials Science              *
!*       Uppsala University                                       *
!*       Sweden                                                   *
!*                                                                *
!*       and                                                      *
!*                                                                *
!*       Department of Materials and Nano Science                 *
!*       KTH Royal Institute of Technology                        *
!*       Sweden                                                   *
!*                                                                *
!******************************************************************
module sld_mc_driver

   implicit none

   public


contains


   !---------------------------------------------------------------------------
   !> @brief
   !> Monte Carlo initial phase for spin-lattice Hamiltonians
   !> This phase of the program is used for equilibration.
   !> No measurements are performed.
   !> Choose between Monte Carlo, Heat Baths or Spin Dynamics.
   !
   !> @author
   !> Anders Bergman, Lars Bergqvist, Jonathan Chico
   !---------------------------------------------------------------------------
   subroutine sld_mc_iphase()

      use InputData
      use LatticeInputData
      use SystemData
      use MonteCarlo
      use LatticeMonteCarlo
      use DemagField
      use MomentData
      use LatticeData
      use LatticeHamiltonianActions
      use HamiltonianData
      use FieldData
      use LatticeFieldData
      use Measurements, only : calc_mavrg
      use AMS
      use QHB
      use Restart
      use Verlet, only : set_avrgu0, set_avrgp0

      integer :: i, k, ipmcstep
      real(dblprec) :: energy,temprescale,temprescalegrad,dummy
      character(len=30) :: filn
      integer :: mc_nmeas,mc_nsamp
      real(dblprec) :: mc_mavg,mc_mavg2,mc_mavg4,mc_minst
      real(dblprec) :: mc_avcum,mc_avsus
      real(dblprec) :: mavg

      real(dblprec), dimension(Mensemble) :: ll_energy
      real(dblprec), dimension(Mensemble) :: lll_energy
      real(dblprec), dimension(Mensemble) :: llll_energy
      real(dblprec), dimension(Mensemble) :: ml_energy
      real(dblprec), dimension(Mensemble) :: mml_energy
      real(dblprec), dimension(Mensemble) :: mmll_energy
      real(dblprec), dimension(Mensemble) :: ldpot_energy
      real(dblprec), dimension(Mensemble) :: sdpot_energy
      real(dblprec), dimension(Mensemble) :: sldpot_energy
      real(dblprec), dimension(Mensemble) :: totpot_energy
      real(dblprec), dimension(Mensemble) :: sld_single_energy
      real(dblprec), dimension(Mensemble) :: mm_energy0

      ! Write header for moment file
      write (filn,'(''mcinitial.'',a8,''.out'')') simid
      open(11, file=filn, position="append")
      write(11,'(a)') "#  Iter.   M_avg.    U_Bind.    Susc."

      ! Allocate work arrays for Metropolis algorithm
      call sld_allocate_mcdata(Natom,1)

      do i=1,ipmcnphase

         ! Write output to stdout
         write (*,'(a28,i3,a10,G11.4,a10,i10,a10,a10)') &
            "Performing MC initial phase: ", i ," Temp: ", ipTemp(i), "MCS/s: ", ipmcnstep(i), " Mode: ", ipmode

         ! Calculate demagnetization field
         if(demag=='Y') then
            call calc_demag(Natom, Mensemble, demag1, demag2, demag3, demagvol, emomM)
         endif

         ! Rescaling of temperature according to Quantum Heat bath
         temprescale=1.0_dblprec
         temprescalegrad=0.0_dblprec
         if (do_qhb=='Q' .or. do_qhb=='R' .or. do_qhb=='P' .or. do_qhb=='T') then
            if(qhb_mode=='MT') then
               call calc_mavrg(Natom,Mensemble,emomM,mavg)
               call qhb_rescale(iptemp(i),temprescale,temprescalegrad,do_qhb,qhb_mode,mavg)
            else
               call qhb_rescale(iptemp(i),temprescale,temprescalegrad,do_qhb,qhb_mode,dummy)
            endif
         endif

         ! Set up order for Metropolis sweeps
         call sld_choose_random_atom_x(Natom,iflip_m)

         ! Zero data for averaging
         mc_nmeas=0
         mc_nsamp=0
         mc_mavg=0.0_dblprec
         mc_mavg2=0.0_dblprec
         mc_mavg4=0.0_dblprec
         mc_avcum=0.0_dblprec
         mc_avsus=0.0_dblprec
         !
         ! Set energy to zero
         energy = 0.0_dblprec

         ! Loop over steps of sweeps
         do ipmcstep=1,ipmcnstep(i)

            !write (*,'(a,i8,a,i8)') 'ipmcstep ', ipmcstep, ' ipmcnstep ', ipmcnstep
            ! Perform one Metropolis Monte Carlo sweep
            call sld_mc_evolve(Natom,Mensemble,iptemp(i),temprescale,do_ll,do_lll,  &
               do_llll,do_ml,do_mml,do_mmll,ipmode,uvec,uvec2,emomM,emom,emom2,mmom,&
               latt_external_field,latt_time_external_field,external_field,         &
               time_external_field,ll_energy,lll_energy,llll_energy,ml_energy,      &
               mml_energy,mmll_energy,ldpot_energy,sdpot_energy,sldpot_energy,      &
               totpot_energy,sld_single_energy,mm_energy0,ammom_inp,aemom_inp,NA)
               !ldpot_energy, sdpot_energy, sldpot_energy, totpot_energy, sld_single_energy)

            ! Sample m, m2, m4 for second half of run (every tenth step)
            if(ipmcstep>ipmcnstep(i)/2.and.mod(ipmcstep-1,10)==0) then

               ! Calculate m_avg
               mc_minst=0.0_dblprec
               do k=1,Mensemble
                  mc_minst=sqrt(sum(emomM(1,:,k))**2+sum(emomM(2,:,k))**2+sum(emomM(3,:,k))**2)/natom/Mensemble
                  mc_mavg=mc_mavg+mc_minst
                  mc_mavg2=mc_mavg2+mc_minst*mc_minst
                  mc_mavg4=mc_mavg4+mc_minst*mc_minst*mc_minst*mc_minst
                  mc_nmeas=mc_nmeas+1
               end do

               ! Calculate Binder cumulant for final quarter of run
               if(ipmcstep>3*ipmcnstep(i)/4) then
                  mc_avcum=mc_avcum+1.0_dblprec-(mc_mavg4/mc_nmeas)/((mc_mavg2/mc_nmeas)**2)/3.0_dblprec
                  mc_avsus=mc_avsus+((mc_mavg2/mc_nmeas)-(mc_mavg/mc_nmeas)**2)
                  mc_nsamp=mc_nsamp+1

                  ! Print Binder cumulant and susceptibility for final tenth of run
                  if(ipmcstep>ipmcnstep(i)*0.9_dblprec) then
                     write(11,'(i8,4f10.6,3f18.6)') ipmcstep,mc_mavg/mc_nmeas,&
                        mc_avcum/mc_nsamp, mc_avsus/mc_nsamp/(8.617343d-5)/ (ipTemp(i)+1.0d-15) *Mensemble
                  end if
               end if
            end if

            ! Print m_avg
            if (mod(ipmcstep,ipmcnstep(i)/10)==0) then

               call calc_mavrg(Natom,Mensemble,emomM,mavg)
               if(ipmcstep<=3*ipmcnstep(i)/4) then
                  write(*,'(2x,a,i5,a,f10.6)') "IP MC ",ipmcstep*100/ipmcnstep(i),"% done. Mbar:", mavg
               else
                  write(*,'(2x,a,i5,a,f10.6,a,f10.6,a,f10.6)') "IP MC ",ipmcstep*100/ipmcnstep(i),"% done. Mbar:", mavg,&
                     "  U(L):",mc_avcum/mc_nsamp, &
                     "  X:",mc_avsus/mc_nsamp/(8.617343d-5)/ (ipTemp(i)+1.0d-15)*Mensemble
               end if

               call sld_choose_random_atom_x(Natom,iflip_m)
            end if

            !Adjust QHB
            if(do_qhb=='Q' .or. do_qhb=='R' .or. do_qhb=='P' .or. do_qhb=='T') then
               if (qhb_mode=='MT') then
                  if (mod(ipmcstep,ipmcnstep(i)/20)==0) then
                     call calc_mavrg(Natom,Mensemble,emomM,mavg)
                     call qhb_rescale(iptemp(i),temprescale,temprescalegrad,do_qhb,qhb_mode,mavg)
                  endif
               endif
            endif

         enddo

         call timing(0,'Initial       ','OF')
         call timing(0,'PrintRestart  ','ON')
         if (do_mom_legacy.ne.'Y') then 
            call prn_mag_conf(Natom,0,Mensemble,'R',simid,mmom,emom,'',mode)
         else
            call prnrestart(Natom, Mensemble, simid, 0, emom, mmom)
         endif
         call timing(0,'PrintRestart  ','OF')
         call timing(0,'Initial       ','ON')
      enddo

      ! Copy equilibrated moments for use in measurement phase
      emom2=emom
      mmom2=mmom

      write(*,'(a)') 'Spin-lattice Monte Carlo initial phase finished'
      write(*,'(a,i10,a,i10,a,f10.6)') 'Ntrial ', Ntrial, ' Nsuccess ', nsuccess, &
         ' SuccessRatio ', real(nsuccess)/real(ntrial)

      ! Calculate Maxvell-Boltzmann distributed ionic velocities
      call sld_calculate_kinenergy(Natom, Mensemble, temp, mioninv, vvec)

      ! Set average ionic displacement to zero
      call set_avrgu0(Natom, Mensemble, uvec, vvec)

      ! Set average linear momenta to zero
      call set_avrgp0(Natom, Mensemble, uvec, vvec)

      ! Deallocate work arrays
      call sld_allocate_mcdata(Natom,-1)

      close(11)
      !
   end subroutine sld_mc_iphase



   !---------------------------------------------------------------------------
   !> @brief
   !> Monte Carlo measurement phase
   !
   !> @author
   !> Anders Bergman, Lars Bergqvist
   !---------------------------------------------------------------------------
   subroutine sld_mc_mphase()
      !
      use Energy,          only : calc_energy
      !      use Ewaldmom
      use InputData
      use LatticeInputData
      use FieldData,       only : external_field, sitefld, time_external_field,&
         allocation_field_time, thermal_field, beff, beff1, beff3
      use LatticeFieldData
      use SystemData
      use MonteCarlo
      use LatticeMonteCarlo
      use DemagField
      use MomentData
      use LatticeData
      use LatticeHamiltonianActions
      use FieldPulse
      use Correlation
      use Polarization
      use prn_averages
      use Measurements
      use ChemicalData,    only : achem_ch, asite_ch
      use MicroWaveField
      use CalculateFields
      use HamiltonianData
      use AutoCorrelation, only : autocorr_sample
      use ChemicalData, only : achtype
      use QHB, only : qhb_rescale, do_qhb, qhb_mode
      use macrocells
      use optimizationRoutines

      !
      implicit none

      !
      integer :: cgk_flag, scount_pulse, bcgk_flag, cgk_flag_pc, mcmstep
      real(dblprec) :: temprescale,temprescalegrad,dummy,totenergy

      real(dblprec), dimension(Mensemble) :: ll_energy
      real(dblprec), dimension(Mensemble) :: lll_energy
      real(dblprec), dimension(Mensemble) :: llll_energy
      real(dblprec), dimension(Mensemble) :: ml_energy
      real(dblprec), dimension(Mensemble) :: mml_energy
      real(dblprec), dimension(Mensemble) :: mmll_energy
      real(dblprec), dimension(Mensemble) :: ldpot_energy
      real(dblprec), dimension(Mensemble) :: sdpot_energy
      real(dblprec), dimension(Mensemble) :: sldpot_energy
      real(dblprec), dimension(Mensemble) :: totpot_energy
      real(dblprec), dimension(Mensemble) :: sld_single_energy
      real(dblprec), dimension(Mensemble) :: mm_energy0

      call timing(0,'MonteCarlo    ','ON')

      ! Flag for G(q) sampling and G(r) calculation
      cgk_flag=0 ; scount_pulse=1 ; bcgk_flag=0 ; cgk_flag_pc=0

      ! Allocate work arrays for MC
      call sld_allocate_mcdata(Natom,1)

      ! Calculation of cumulant
      if(do_cumu .ne. 'A') do_cumu = 'Y'

      ! Setup order for Metropolis sweeps
      call sld_choose_random_atom_x(Natom,iflip_m)

      ! Calculate demagnetizing field
      if(demag=='Y') then
         call calc_demag(Natom, Mensemble, demag1, demag2, demag3, demagvol, emomM)
      endif

      ! Rescaling of temperature according to Quantum Heat bath
      temprescale=1.0_dblprec
      temprescalegrad=0.0_dblprec
      if (do_qhb=="Q" .or. do_qhb=='R' .or. do_qhb=='P' .or. do_qhb=='T') then
         if(qhb_mode=='MT') then
            call calc_mavrg(Natom,Mensemble,emomM,mavg)
            call qhb_rescale(Temp,temprescale,temprescalegrad,do_qhb,qhb_mode,mavg)
         else
            call qhb_rescale(Temp,temprescale,temprescalegrad,do_qhb,qhb_mode,dummy)
         endif
      endif

      if (do_pol=='Y') then
         call init_polarization(Natom,Mensemble,ham%max_no_neigh,ham%nlist,coord,1)
      end if

      ! Calculate the static magnetic fields which will be calculated only once as they are not time dependent
      call calc_external_fields(Natom, Mensemble, NA, hfield, anumb, external_field,&
         do_bpulse,sitefld,sitenatomfld)

      ! Perform MC sweeps
      do mcmstep=1,mcnstep

         call timing(0,'MonteCarlo    ','OF')
         call timing(0,'Measurement   ','ON')

         ! Measure averages and trajectories
         call measure(Natom,Mensemble,NT,NA,nHam,N1,N2,N3,simid,mcmstep,emom,emomM, &
            mmom,Nchmax,do_ralloy,Natom_full,asite_ch,achem_ch,atype,plotenergy,    &
            Temp,temprescale,temprescalegrad,'N',1.0_dblprec,logsamp,               &
            ham%max_no_neigh,ham%nlist,ham%ncoup,      &
            ham%nlistsize,ham%aham,thermal_field,beff,beff1,beff3,coord,            &
            ham%ind_list_full,ham%ind_nlistsize,ham%ind_nlist,ham%max_no_neigh_ind, &
            ham%sus_ind,do_mom_legacy,mode)

         ! Calculate total and term resolved energies
         if(plotenergy>0.and.mod(mcmstep-1,cumu_step)==0) then

            call timing(0,'Measurement   ','OF')
            call timing(0,'Energy        ','ON')
            totenergy=0.0_dblprec

            call calc_energy(nHam,mcmstep,Natom,Nchmax,  &
               conf_num,Mensemble,Natom,Num_macro,1,             &
               plotenergy,Temp,1.0_dblprec,do_lsf,        &
               lsf_field,lsf_interpolate,'N',simid,cell_index,macro_nlistsize,mmom,         &
               emom,emomM,emomM_macro,external_field,time_external_field,                   &
               max_no_constellations,maxNoConstl,unitCellType,constlNCoup,                  &
               constellations,OPT_flag,constellationsNeighType,totenergy,NA,N1,N2,N3)
            call timing(0,'Energy        ','OF')
            call timing(0,'Measurement   ','ON')
         endif

         ! Spin correlation
         ! Sample magnetic moments for correlation functions
         ! Sample S(r) through S(q)
         call correlation_wrapper(Natom,Mensemble,coord,simid,emomM,mcmstep,delta_t,&
            NT,atype,Nchmax,achtype,sc,do_sc,do_sr,cgk_flag)

         call timing(0,'Measurement   ','OF')
         call timing(0,'MonteCarlo    ','ON')

         ! Perform one Metropolis Monte Carlo sweep
         call sld_mc_evolve(Natom,Mensemble,temp,temprescale,do_ll,do_lll,do_llll,  &
            do_ml,do_mml,do_mmll,ipmode,uvec,uvec2,emomM,emom,emom2,mmom,           &
            latt_external_field,latt_time_external_field,external_field,            &
            time_external_field,ll_energy,lll_energy,llll_energy,ml_energy,         &
            mml_energy,mmll_energy,ldpot_energy,sdpot_energy,sldpot_energy,         &
            totpot_energy,sld_single_energy,mm_energy0,ammom_inp,aemom_inp,NA)
            !ldpot_energy, sdpot_energy, sldpot_energy, totpot_energy, sld_single_energy)

         ! Calculate and print m_avg
         if(mcnstep>20) then
            if(mod(mcmstep,mcnstep/20)==0) then

               call calc_mavrg(Natom,Mensemble,emomM,mavg)
               write(*,'(2x,a,i3,a,f10.6)',advance='no') &
                  "MP MC ",mcmstep*100/(mcnstep),"% done. Mbar:",mavg
               if(plotenergy>0) then
                  write(*,'(a,f12.6,a,f8.5,a)') ". Ebar:", totenergy,". U:",binderc,"."
               else
                  write(*,'(a,f8.5,a)') ". U:",binderc,"."
               end if
            end if
         else
            write(*,'(2x,a,i3,a,G13.6)')   "Iteration",mcmstep," Mbar ",mavg
         end if

         !Adjust QHB
         if(do_qhb=='Q' .or. do_qhb=='R' .or. do_qhb=='P' .or. do_qhb=='T') then
            if(qhb_mode=='MT') then
               if (mod(mcmstep,mcnstep/40)==0) then
                  call calc_mavrg(Natom,Mensemble,emomM,mavg)
                  call qhb_rescale(Temp,temprescale,temprescalegrad,do_qhb,qhb_mode,mavg)
               endif
            endif
         endif

         if(mod(mcmstep,mcnstep/10)==0) then
            ! Change order for Metropolis sweeps
            call sld_choose_random_atom_x(Natom,iflip_m)
         end if

      enddo

      call timing(0,'MonteCarlo    ','OF')
      call timing(0,'Measurement   ','ON')

      ! Print remaining measurements
      call flush_measurements(Natom,Mensemble,NT,NA,N1,N2,N3,simid,mcmstep,emom,    &
         mmom,Nchmax,atype,'N',mcnstep,ham%ind_list_full,do_mom_legacy,mode)

      if (do_pol=='Y') then
         call init_polarization(Natom,Mensemble,ham%max_no_neigh,ham%nlist,coord,-1)
      end if

      ! Deallocate work arrays
      call sld_allocate_mcdata(Natom,-1)

      close(11)

      call timing(0,'Measurement   ','OF')

   end subroutine sld_mc_mphase


end module sld_mc_driver
