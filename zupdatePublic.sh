#!/bin/bash

# Copies contents from the UppASD GitLab master branch to the
# UppASD GitHub master branch

# Requires local directory structure
# ../../MasterUppASD/UppASD/  (The GitLab master branch)
# ../../ReleasedUppASD/UppASD/ (The GitHub master branch)

# Enter repository for GitHub master branch
cd ../../ReleasedUppASD/UppASD/

# Copy files from the GitLab master branch
basename=../../MasterUppASD/UppASD/

# Files in root directory
cp -p $basename/AUTHORS .
cp -p $basename/CMakeLists.txt .
cp -p $basename/CONTRIBUTING.md .
cp -p $basename/LICENSE .
cp -p $basename/Makefile .
cp -p $basename/README.md .
cp -p $basename/requirements.txt .
cp -p $basename/setup_UppASD.sh .

# Subdirectories
for dirname in ASD_Tools \
		   bin \
		   cmake \
		   codeTester \
		   docs \
		   examples_revision_controlled \
		   source
do
    cp -rp $basename$dirname .
done

# Return to the repository for the GitLab master branch
cd ../../MasterUppASD/UppASD/

# Files and directories that are not included
# ASD_GUI/
# Disabled/
# ASD_GUI.tar.gz
