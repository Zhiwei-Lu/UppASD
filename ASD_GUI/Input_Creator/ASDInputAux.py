""" @package ASDInputAux
Set of auxiliary functions to write restartfiles .
It has functions to generate the coordinate file, as well as to write the
following magnetic configurations:
    - Domain walls:
        - Neel planar wall.
        - Bloch planar wall.
        - Vortex wall.
    - Skyrmion states:
        - Neel skyrmion.
        - Bloch skyrmion.
    - Helical spin spirals.

Author
----------
Jonathan Chico
"""


def write_domain_wall(num_atoms=1, Mensemble=1, coord=[None], DWInfo=None):
    """Function to generate domain wall profiles.
    The domain walls can be centered at a certain point in the lattice and with
    a given width.
    The chirality and type of wall (Neel and Bloch) can be chosen.

    Arguments:
        num_atoms {int} -- Number of atoms in the sample
        Mensemble {int} -- Number of ensembles
        coord {np.array float [3,num_atoms]} -- Positions of each of the atoms
        DWInfo {dict} -- dictionary containing the DW information

    Returns:
        mag {np.array float [3,num_atoms,Mensemble]} -- Magnetic configuration
        for each atom per ensemble
    """
    import numpy as np
    import itertools as itr
    tol = 1e-9
    # Magnetization of the system
    mag = np.zeros([Mensemble, num_atoms, 3], dtype=np.float64)
    # Setup a planar domain wall type, i.e. a 1D object
    if DWInfo['type'] == 'planar':
        # Plane indicates which is the "propagation direction of the DW"
        # The sign in front of the 1/cosh determines the chirality of the wall
        if DWInfo['DWtype'] == 'Bloch':
            if DWInfo['easy_axis'] == 'x':
                # Loop over the atoms in the system
                for jj, ii in itr.product(range(Mensemble), range(num_atoms)):
                    arg = (DWInfo['center']-coord[ii, DWInfo['plane']]) /\
                        DWInfo['width']
                    mag[:, ii, 0] = np.tanh(arg)
                    mag[:, ii, 1] = 0.0
                    mag[:, ii, 2] = DWInfo['chirality']*1.0/np.cosh(arg)
                    mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
                    # Normalization of the spins
                    mag[jj, ii, :] = mag[jj, ii, :]/mod
            elif DWInfo['easy_axis'] == 'y':
                # Loop over the atoms in the system
                for jj, ii in itr.product(range(Mensemble), range(num_atoms)):
                    arg = (DWInfo['center']-coord[ii, DWInfo['plane']]) /\
                        DWInfo['width']
                    mag[:, ii, 0] = 0.0
                    mag[:, ii, 1] = np.tanh(arg)
                    mag[:, ii, 2] = DWInfo['chirality']*1.0/np.cosh(arg)
                    mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
                    # Normalization of the spins
                    mag[jj, ii, :] = mag[jj, ii, :]/mod
            elif DWInfo['easy_axis'] == 'z':
                # Loop over the atoms in the system
                for jj, ii in itr.product(range(Mensemble), range(num_atoms)):
                    arg = (DWInfo['center']-coord[ii, DWInfo['plane']]) /\
                        DWInfo['width']
                    mag[:, ii, 0] = 0.0
                    mag[:, ii, 1] = DWInfo['chirality']*1.0/np.cosh(arg)
                    mag[:, ii, 2] = np.tanh(arg)
                    mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
                    # Normalization of the spins
                    mag[jj, ii, :] = mag[jj, ii, :]/mod
        elif DWInfo['DWtype'] == 'Neel':
            if DWInfo['easy_axis'] == 'x':
                # Loop over the atoms in the system
                for jj, ii in itr.product(range(Mensemble), range(num_atoms)):
                    arg = (DWInfo['center']-coord[ii, DWInfo['plane']]) /\
                        DWInfo['width']
                    mag[:, ii, 0] = np.tanh(arg)
                    mag[:, ii, 1] = DWInfo['chirality']*1.0/np.cosh(arg)
                    mag[:, ii, 2] = 0.0
                    mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
                    # Normalization of the spins
                    mag[jj, ii, :] = mag[jj, ii, :]/mod
            elif DWInfo['easy_axis'] == 'y':
                # Loop over the atoms in the system
                for jj, ii in itr.product(range(Mensemble), range(num_atoms)):
                    arg = (DWInfo['center']-coord[ii, DWInfo['plane']]) /\
                        DWInfo['width']
                    mag[jj, ii, 0] = DWInfo['chirality']*1.0/np.cosh(arg)
                    mag[jj, ii, 1] = np.tanh(arg)
                    mag[jj, ii, 2] = 0.0
                    mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
                    # Normalization of the spins
                    mag[jj, ii, :] = mag[jj, ii, :]/mod
            elif DWInfo['easy_axis'] == 'z':
                # Loop over the atoms in the system
                for jj, ii in itr.product(range(Mensemble), range(num_atoms)):
                    arg = (DWInfo['center']-coord[ii, DWInfo['plane']]) /\
                        DWInfo['width']
                    mag[jj, ii, 0] = DWInfo['chirality']*1.0/np.cosh(arg)
                    mag[jj, ii, 1] = 0.0
                    mag[jj, ii, 2] = np.tanh(arg)
                    mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
                    # Normalization of the spins
                    mag[jj, ii, :] = mag[jj, ii, :]/mod
    # Setup a vortex domain wall type, i.e. a 2D object
    elif DWInfo['type'] == 'vortex':
        # For the vortex wall one must introduce a rotation like term
        # Loop over the atoms in the system
        for ii in range(0, num_atoms):
            r_x = coord[ii, 0]-DWInfo['center'][0]
            r_y = coord[ii, 1]-DWInfo['center'][1]
            mod_r2 = np.sqrt(r_x**2 + r_y**2)
            arg = mod_r2/DWInfo['radius']
            if mod_r2 > tol:
                theta = np.arctan2(r_x, r_y)
            else:
                theta = 0.0
            mag[:, ii, 0] = DWInfo['chirality']*np.cos(theta)*np.tanh(arg)
            mag[:, ii, 1] = DWInfo['chirality']*np.sin(theta)*np.tanh(arg)
            mag[:, ii, 2] = DWInfo['polarity']*1.0/np.cosh(arg)
            mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
            # Normalization of the spins
            mag[jj, ii, :] = mag[jj, ii, :]/mod
    return mag


def write_skyrmion(num_atoms=1, Mensemble=1, coord=[None], SkxInfo=None):
    """Generates a skyrmion profile making use of the skyrmion profiles
    defined in Nat. Commun. 7, 13613 (2016):
    .. math::
    m_x = \\cos(m\\phi+\\gamma)\\sin(\\theta(r))
    .. math::
    m_y = \\sin(m\\phi+\\gamma)\\sin(\\theta(r))
    .. math::
    m_z = \\cos(\\theta(r))

    With the out of plane angle :math:`\\theta` being given by
    .. math::
    \\theta(r)=\\pi + \\arcsin(\\tanh((r+c)/w)) + \\arcsin(\\tanh((r-c)/w))

    with :math:`c` being the center of the skyrmion and :math:`w` the radius.
    The polar angle :math:`\\phi` is determined from the skyrmion center,
    :math:`\\gamma` determined the type of skyrmion with :math:`\\gamma`
        implies a
    Neel Skyrmion and :math:`\\gamma=\\frac{\\pi}{2}` is a Bloch Skyrmion.
    The skyrmion is assumed to have its core parallel to the z-axis.

    Arguments:
        num_atoms {int} -- Number of atoms in the sample
        Mensemble {int} -- Number of ensembles
        coord {np.array float [3,num_atoms]} -- Positions of each of the atoms
        SkxInfo {dict} -- dictionary containing the Skyrmion information

    Returns:
        mag {np.array float [3,num_atoms,Mensemble]} -- Magnetic configuration
        for each atom per ensemble
    """

    import numpy as np
    import itertools as itr
    tol = 1e-9
    # Magnetization of the system
    mag = np.zeros([Mensemble, num_atoms, 3], dtype=np.float64)
    # Loop over the atoms in the system
    for jj, ii in itr.product(range(Mensemble), range(num_atoms)):
        # Define the factors for the in-plane phi angle
        r_x = coord[ii, 0]-SkxInfo['center'][0]
        r_y = coord[ii, 1]-SkxInfo['center'][1]
        mod_r = np.sqrt(r_x**2 + r_y**2)
        # The distance from the center of the skyrmion to which the DW walls
        # are set
        rad = SkxInfo['width']*0.5
        r_p = (mod_r + rad)/SkxInfo['width']
        r_n = (mod_r - rad)/SkxInfo['width']
        # Out of plane angle
        theta = (np.pi + np.arcsin(np.tanh(r_p)) + np.arcsin(np.tanh(r_n))
                 + SkxInfo['polarity'])
        # In-plane skyrmion profile angle
        if mod_r > tol:
            phi = np.arctan2(r_y, r_x)
        else:
            phi = 0.0
        # Magnetization per site
        mag[jj, ii, 0] = SkxInfo['handness']*np.sin(theta) *\
            np.cos(SkxInfo['order']*phi + SkxInfo['type'])
        mag[jj, ii, 1] = SkxInfo['handness']*np.sin(theta) *\
            np.sin(SkxInfo['order']*phi + SkxInfo['type'])
        mag[jj, ii, 2] = np.cos(theta)
        mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
        # Normalization of the spins
        mag[jj, ii, :] = mag[jj, ii, :]/mod
    return mag


def create_spiral(num_atoms=1, Mensemble=1, coord=[None], HLInfo=None):
    """Function to generate a generalized spin spiral configuration.
    This function creates a generalized spin spiral via Rodrigues rotations

    .. math::
    \\mathbf{v}_{rot}= \\mathbf{v}\\cos\\theta
    +\\left(\\mathbf{v}\\times\\mathbf{k}\\right)\\sin\\theta
    +\\mathbf{k}\\left(\\mathbf{k}\\cdot\\mathbf{v}\\right)
    \\left(1-\\cos\\theta\\right)

    First a rotation is done to generate the cone angle, this is done by
    generating an
    axis that is perpendicular to the **pitch vector** (that is :math:v in
    the Rodrigues formula)
    which is dubbed the **cone axis**.
    The pitch vector is then rotated by that axis by an angle :math:\\theta,
    i.e. the cone angle.
    The obtained vector is then the initial spin direction, that will be
    rotated by the pitch vector, with an angle given by
    .. math::
    \\theta=\\mathbf{q}\\cdot\\mathbf{r}

    with :math:\\mathbf{q} being the spiral wavevector.

    Arguments:
        num_atoms {int} -- Number of atoms in the sample
        Mensemble {int} -- Number of ensembles
        coord {np.array float [3,num_atoms]} -- Positions of each of the atoms
        HLInfo {dict} -- dictionary containing the spin spiral information

    Returns:
        mag {np.array float [3,num_atoms,Mensemble]} -- Magnetic configuration
        for each atom per ensemble
    """
    import numpy as np
    import itertools as itr
    # Transform lists to np arrays to avoid problems
    rot_vector = np.asarray(HLInfo['pitch_vector'], dtype=np.float64)
    prop_vector = np.asarray(HLInfo['prop_vector'], dtype=np.float64)
    # Magnetization of the system
    mag = np.zeros([Mensemble, num_atoms, 3], dtype=np.float64)
    # First do a rotation to find the rotates spin for the conical phase
    # First create a vector perpendicular to the rotation axis
    test_r = np.random.rand(3)
    # Normalize the vector
    test_r = test_r/np.sqrt(test_r.dot(test_r))
    # Axis which one will use to rotate the spins to get the conical phase
    cone_axis = np.cross(rot_vector, test_r)
    # Rotate the spin first to find the needed cone angle using Rodriges
    # rotation
    init_spin = rot_vector*np.cos(HLInfo['cone_angle'])\
        + np.cross(cone_axis, rot_vector)*np.sin(HLInfo['cone_angle'])\
        + cone_axis*(cone_axis.dot(rot_vector)) *\
        (1-np.cos(HLInfo['cone_angle']))
    # Loop over the ensembles and atoms of the system
    for jj, ii in itr.product(range(Mensemble), range(num_atoms)):
        theta = prop_vector.dot(coord[ii, :])*2.0*np.pi*HLInfo['handness']
        # Do a Rodrigues rotation to get the helical spiral state
        mag[jj, ii, :] = init_spin*np.cos(theta)\
            + np.cross(rot_vector, init_spin)*np.sin(theta)\
            + rot_vector*(rot_vector.dot(init_spin))*(1-np.cos(theta))
        mod = np.sqrt(mag[jj, ii].dot(mag[jj, ii]))
        # Normalization of the spins
        mag[jj, ii, :] = mag[jj, ii, :]/mod
    return mag


def create_coord(cell=None, ncell=[None], Bas=[None], block_size=0,
                 mom=[None]):
    """ Function to generate the coordinated for a given lattice following the
    same structure than in `UppASD`.
    Routine taken from the `UppASD` `geometry.f90`

    Arguments:
        cell {np.array float [3,3]} -- lattice vectors (default: {None})
        ncell {np.array float [3]} -- Repetitions of the unit cell
        (default: {[None]})
        Bas {np.array float [3,na_bas]} -- Positions of the basis atoms
        (default: {[None]})
        block_size {int} -- Size of the block used for coordinates generation
        (default: {0})
        mom {np.array float [na_bas]} -- Magnitude of the magnetic moment in
        the unit cell (default: {[None]})

    Returns:
        coord {np.array float [3,num_atoms]} -- Positions of the atoms in the
        system
        mom_mag {np.array float [num_atoms]} -- Magnetic moment for each atom
    """

    import numpy as np
    import itertools as itr
    tol = 1e-9

    na_bas = len(Bas)

    invmatrix = np.zeros([3, 3], dtype=np.float64)
    detmatrix = np.linalg.det(cell)
    if (abs(detmatrix) > tol):
        invmatrix = np.linalg.inv(cell)

    icvec = np.zeros([3], dtype=np.float64)
    bsf = np.zeros([3], dtype=np.float64)
    for I0 in range(0, na_bas):
        icvec[0] = np.sum(Bas[I0, :]*invmatrix[:, 0])
        icvec[1] = np.sum(Bas[I0, :]*invmatrix[:, 1])
        icvec[2] = np.sum(Bas[I0, :]*invmatrix[:, 2])
        bsf[:] = np.floor(icvec[:] + 1e-7)
        for mu in range(0, 3):
            Bas[I0, mu] = Bas[I0, mu] - np.sum(bsf[:]*cell[:, mu])

    ii = 0
    coord = np.zeros([na_bas*ncell[0]*ncell[1]*ncell[2], 3], dtype=np.float64)
    mom_mag = np.zeros([na_bas*ncell[0]*ncell[1]*ncell[2]], dtype=np.float64)
    for II3, II2, II1 in itr.product(range(0, ncell[2], block_size),
                                     range(0, ncell[1], block_size),
                                     range(0, ncell[0], block_size)):
        for I3, I2, I1, I0 in\
            itr.product(range(II3, min(II3 + block_size, ncell[2])),
                        range(II2, min(II2 + block_size, ncell[1])),
                        range(II1, min(II1 + block_size, ncell[0])),
                        range(0, na_bas)):
            mom_mag[ii] = mom[I0]
            coord[ii, :] = I1*cell[0, :] + I2*cell[1, :] + I3*cell[2, :]\
                + Bas[I0, :]
            ii = ii + 1
    return coord, mom_mag
