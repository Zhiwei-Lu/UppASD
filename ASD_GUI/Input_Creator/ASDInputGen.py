"""@package ASDInputGen
Class containing the main defintions for the inpsd.dat writer.

This class has the information needed to generate the dictionary used to print
the inpsd.dat, collect the data from the GUI and print it to file, both in
inpsd.dat format and in .yaml format.

Author
----------
Jonathan Chico
"""


class ASDInputGen():
    """Class containing the structures needed for the creation of the inpsd.dat
    via the GUI. It defines all the variables set in the GUI as part of a
    dictionary which is populated by certain default values. These are then
    purged to ensure that one keeps only the minimal set of input variables
    needed.

    Author:
        Jonathan Chico
    """
    def __init__(self):
        import collections
        from PyQt5.QtGui import QIntValidator, QDoubleValidator
        self.UppASDKeywords = collections.OrderedDict()
        self.IntegerValidator = QIntValidator()
        self.IntegerValidator.setRange(0, 99999999)
        self.PosDoubleValidator = QDoubleValidator()
        self.PosDoubleValidator.setRange(0, 99999999.9999)
        self.PosDoubleValidator.setDecimals(10)
        self.DoubleValidator = QDoubleValidator()
        self.DoubleValidator.setDecimals(10)
        self.momfile = []
        self.momfile_in = []
        self.momfile_fi = []
        self.posfile = []
        self.jfile = []
        self.dmfile = []
        self.kfile = []
        self.restartfile = []
        self.pdfile = []
        self.bqfile = []
        self.bqdmfile = []
        self.momfile_gotten = False
        self.posfile_gotten = False
        return

    def getFileName(self, window=None):
        """Function to find the needed file names for the input file created.

        Keyword Arguments:
            window {[type]} -- [description] (default: {None})
        """
        from PyQt5 import QtWidgets

        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.AnyFile)
        dlg.setDirectory('.')
        if dlg.exec_():
            if window.sender() == window.InpPosButtonSelect:
                self.posfile_gotten = False
                self.posfile = dlg.selectedFiles()[0]
                self.posfile_gotten = True
            if window.sender() == window.InpMomButtonSelect:
                self.momfile_gotten = False
                self.momfile = dlg.selectedFiles()[0]
                self.momfile_gotten = True
            if window.sender() == window.InpInitMag4ReadButton:
                self.restartfile = dlg.selectedFiles()[0]
            if window.sender() == window.InpXCCheck and\
               window.InpXCCheck.isChecked():
                self.jfile = dlg.selectedFiles()[0]
            if window.sender() == window.InpDMCheck and\
               window.InpDMCheck.isChecked():
                self.dmfile = dlg.selectedFiles()[0]
            if window.sender() == window.InpMAECheck and\
               window.InpMAECheck.isChecked():
                self.kfile = dlg.selectedFiles()[0]
            if window.sender() == window.InpPseudoCheck and\
               window.InpPseudoCheck.isChecked():
                self.pdfile = dlg.selectedFiles()[0]
            if window.sender() == window.InpBqCheck and\
               window.InpBqCheck.isChecked():
                self.bqfile = dlg.selectedFiles()[0]
            if window.sender() == window.InpBqDMCheck and\
               window.InpBqDMCheck.isChecked():
                self.bqdmfile = dlg.selectedFiles()[0]
            if window.sender() == window.InpSetIniMomfileButton:
                self.momfile_in = dlg.selectedFiles()[0]
            if window.sender() == window.InpSetFinMomfileButton:
                self.momfile_fi = dlg.selectedFiles()[0]
        return

    def update_file_name(self, window=None):
        """Updating the file names in the input data when files are generated
        by the GUI.

        Keyword Arguments:
            window {[type]} -- [description] (default: {None})
        """
        if window.sender() == window.Posfile_Window.InpPosDone:
            self.posfile = window.Posfile_Window.posfile_name
        if window.sender() == window.Momfile_Window.InpMomDone:
            self.momfile = window.Momfile_Window.momfile_name
        if window.sender() == window.Restart_Window.InpRestartDone:
            self.restartfile = window.Restart_Window.restartfile_name
        return

    def ASDInputGatherer(self, window=None):
        """Function to get the information needed for the `inpsd.dat` from the
        GUI to pass it to the dictionary.

        Keyword Arguments:
            window {[type]} -- [description] (default: {None})
        """
        # Find the simulation name
        if len(window.InpLineEditSimid.text()) > 0:
            self.UppASDKeywords['general']['simid'] =\
                str(window.InpLineEditSimid.text())
        # Obtain the lattice constant of the system
        if len(window.InpLineEditAlat.text()) > 0:
            self.UppASDKeywords['geometry']['alat'] =\
                float(self.text_to_num(window.InpLineEditAlat.text()))
        # The number of repetitions of the unit cell
        if len(window.InpN1.text()) > 0:
            self.UppASDKeywords['geometry']['ncell'][0] =\
                int(self.text_to_num(window.InpN1.text()))
        if len(window.InpN2.text()) > 0:
            self.UppASDKeywords['geometry']['ncell'][1] =\
                int(self.text_to_num(window.InpN2.text()))
        if len(window.InpN3.text()) > 0:
            self.UppASDKeywords['geometry']['ncell'][2] =\
                int(self.text_to_num(window.InpN3.text()))
        # Set the boundary conditions
        if window.InpPBCCheckC1.isChecked():
            self.UppASDKeywords['geometry']['BC'][0] = 'P'
        else:
            self.UppASDKeywords['geometry']['BC'][0] = 0
        if window.InpPBCCheckC2.isChecked():
            self.UppASDKeywords['geometry']['BC'][1] = 'P'
        else:
            self.UppASDKeywords['geometry']['BC'][1] = 0
        if window.InpPBCCheckC3.isChecked():
            self.UppASDKeywords['geometry']['BC'][2] = 'P'
        else:
            self.UppASDKeywords['geometry']['BC'][2] = 0
        # Filling up the lattice vectors
        self.UppASDKeywords['geometry']['cell'] =\
            [[float(self.text_to_num(window.InpLineEditC1_x.text())),
              float(self.text_to_num(window.InpLineEditC1_y.text())),
              float(self.text_to_num(window.InpLineEditC1_z.text()))],
             [float(self.text_to_num(window.InpLineEditC2_x.text())),
              float(self.text_to_num(window.InpLineEditC2_y.text())),
              float(self.text_to_num(window.InpLineEditC2_z.text()))],
             [float(self.text_to_num(window.InpLineEditC3_x.text())),
              float(self.text_to_num(window.InpLineEditC3_y.text())),
              float(self.text_to_num(window.InpLineEditC3_z.text()))]]
        # Check for random alloys
        if window.InpCheckRandAlloy.isChecked():
            self.UppASDKeywords['geometry']['do_ralloy'] = 1
        else:
            self.UppASDKeywords['geometry']['do_ralloy'] = 0
        if len(self.posfile) > 0:
            self.UppASDKeywords['geometry']['posfile'] = self.posfile
        else:
            print('No posfile name given, assuming "./posfile"')
        if len(self.momfile) > 0:
            self.UppASDKeywords['geometry']['momfile'] = self.momfile
        else:
            print('No momfile name given, assuming "./momfile"')
        # Check for Hamiltonian interactions
        if window.InpXCCheck.isChecked():
            if len(self.jfile) > 0:
                self.UppASDKeywords['Hamiltonian']['exchange'] = self.jfile
            else:
                print('No exchange file name given, assuming "./jfile"')
        if window.InpDMCheck.isChecked():
            self.UppASDKeywords['Hamiltonian']['dm'] = self.dmfile
            self.UppASDKeywords['Hamiltonian']['do_dm'] = 1
        else:
            self.UppASDKeywords['Hamiltonian']['do_dm'] = 0
        if window.InpMAECheck.isChecked():
            self.UppASDKeywords['Hamiltonian']['anisotropy'] = self.kfile
            self.UppASDKeywords['Hamiltonian']['do_anisotropy'] = 1
        else:
            self.UppASDKeywords['Hamiltonian']['do_anisotropy'] = 0
        if window.InpPseudoCheck.isChecked():
            self.UppASDKeywords['Hamiltonian']['pd'] = self.pdfile
            self.UppASDKeywords['Hamiltonian']['do_pd'] = 1
        else:
            self.UppASDKeywords['Hamiltonian']['do_pd'] = 0
        if window.InpBqCheck.isChecked():
            self.UppASDKeywords['Hamiltonian']['bq'] = self.bqfile
            self.UppASDKeywords['Hamiltonian']['do_bq'] = 1
        else:
            self.UppASDKeywords['Hamiltonian']['do_bq'] = 0
        if window.InpBqCheck.isChecked():
            self.UppASDKeywords['Hamiltonian']['biqdm'] = self.bqdmfile
            self.UppASDKeywords['Hamiltonian']['do_biqdm'] = 1
        else:
            self.UppASDKeywords['Hamiltonian']['do_biqdm'] = 0
        # Filling up the magnetic field in the measurement phase
        self.UppASDKeywords['Hamiltonian']['hfield'] =\
            [float(self.text_to_num(window.InpBextMeasure_x.text())),
             float(self.text_to_num(window.InpBextMeasure_y.text())),
             float(self.text_to_num(window.InpBextMeasure_z.text()))]
        # Filling up the magnetic field in the initial phase
        self.UppASDKeywords['Hamiltonian']['ip_hfield'] =\
            [float(self.text_to_num(window.InpBextInit_x.text())),
             float(self.text_to_num(window.InpBextInit_y.text())),
             float(self.text_to_num(window.InpBextInit_z.text()))]
        # Damping for an SD measurement phase
        if len(window.InpASDLLGDamp.text()) > 0:
            self.UppASDKeywords['LLG_mphase']['damping'] =\
                float(self.text_to_num(window.InpASDLLGDamp.text()))
        # Time step for an SD measurement phase
        if len(window.InpASDLLGDT.text()) > 0:
            self.UppASDKeywords['LLG_mphase']['timestep'] =\
                float(self.text_to_num(window.InpASDLLGDT.text()))
        # Number of steps for an SD measurement phase
        self.UppASDKeywords['LLG_mphase']['Nstep'] =\
            int(self.text_to_num(window.InpASDLLGSteps.text()))
        # Number of MC steps in the measurement phase
        self.UppASDKeywords['MC_mphase']['mcnstep'] =\
            int(self.text_to_num(window.InpMCSteps.text()))
        # Number of ensembles in the simulation
        if len(window.InpMensemble.text()) > 0:
            self.UppASDKeywords['Mag']['Mensemble'] =\
                int(self.text_to_num(window.InpMensemble.text()))
        # Check if the trajectories will be printed
        if window.InpGlobalTrajBox.isChecked():
            self.UppASDKeywords['trajectories']['do_tottraj'] = 'Y'
        else:
            self.UppASDKeywords['trajectories']['do_tottraj'] = 'N'
        # Information about the printing of the trajectories
        if len(window.InitTTrajStep.text()) > 0:
            self.UppASDKeywords['trajectories']['tottraj_step'] =\
                int(self.text_to_num(window.InitTTrajStep.text()))
        if len(window.InitTTrajBuff.text()) > 0:
            self.UppASDKeywords['trajectories']['tottraj_buff'] =\
                int(self.text_to_num(window.InitTTrajBuff.text()))
        # Check if the cumulants will be printed
        if window.InpCumuBox.isChecked():
            self.UppASDKeywords['cumulants']['do_cumu'] = 'Y'
        else:
            self.UppASDKeywords['cumulants']['do_cumu'] = 'N'
        # Options for the printing of the cumulants
        if len(window.InpCumuStep.text()) > 0:
            self.UppASDKeywords['cumulants']['cumu_step'] =\
                int(self.text_to_num(window.InpCumuStep.text()))
        if len(window.InpCumuBuff.text()) > 0:
            self.UppASDKeywords['cumulants']['cumu_buff'] =\
                int(self.text_to_num(window.InpCumuBuff.text()))
        # Check if the averages should be printed
        if window.InpAveBox.isChecked():
            self.UppASDKeywords['averages']['do_avrg'] = 'Y'
        if not window.InpAveBox.isChecked():
            self.UppASDKeywords['averages']['do_avrg'] = 'N'
        # Options for the printing of the averages
        if len(window.InpAveStep.text()) > 0:
            self.UppASDKeywords['averages']['avrg_step'] =\
                int(self.text_to_num(window.InpAveStep.text()))
        if len(window.InpAveBuff.text()) > 0:
            self.UppASDKeywords['averages']['avrg_buff'] =\
                int(self.text_to_num(window.InpAveBuff.text()))
        # Check if the site projected average will be printed
        if window.InpSiteAveCheck.isChecked():
            self.UppASDKeywords['averages']['do_proj_avrg'] = 'A'
        # Check if the type projected average will be printed
        elif window.InpTypeAveCheck.isChecked():
            self.UppASDKeywords['averages']['do_proj_avrg'] = 'Y'
        else:
            self.UppASDKeywords['averages']['do_proj_avrg'] = 'N'
        # Check if the chemically projected average will be printed
        if window.InpChemAveCheck.isChecked():
            self.UppASDKeywords['averages']['do_projch_avrg'] = 'Y'
        else:
            self.UppASDKeywords['averages']['do_projch_avrg'] = 'N'
        # Check if the dynamical structure factor will be calculated
        if window.ImpSqwBox.isChecked():
            self.UppASDKeywords['Mag_corr']['do_sc'] = 'Q'
        if not window.ImpSqwBox.isChecked():
            self.UppASDKeywords['Mag_corr']['do_sc'] = 'N'
        # Check if the static correlation is calculated
        if window.InpScCheck.isChecked():
            self.UppASDKeywords['Mag_corr']['do_sc'] = 'C'
        # Options for the structure factor
        if len(window.InpScStep.text()) > 0:
            self.UppASDKeywords['Mag_corr']['sc_step'] =\
                int(self.text_to_num(window.InpScStep.text()))
        if len(window.InpScNStep.text()) > 0:
            self.UppASDKeywords['Mag_corr']['sc_nstep'] =\
                int(self.text_to_num(window.InpScNStep.text()))
        # Check if the AMS will be printed
        if window.InpAMSCheck.isChecked():
            self.UppASDKeywords['Mag_corr']['do_ams'] = 'Y'
        else:
            self.UppASDKeywords['Mag_corr']['do_ams'] = 'N'
        # Check if the STT is going to be considered
        if window.InpSTTBox.isChecked():
            if window.InpZhangLiCheck.isChecked():
                self.UppASDKeywords['spintorque']['stt'] = 'A'
            if window.InpSlonwceskiCheck.isChecked():
                self.UppASDKeywords['spintorque']['stt'] = 'Y'
            # Check if the SHE is considered
            if window.InpSHEBox.isChecked():
                self.UppASDKeywords['spintorque']['do_she'] = 'Y'
            else:
                self.UppASDKeywords['spintorque']['do_she'] = 'N'
        else:
            self.UppASDKeywords['spintorque']['stt'] = 'N'
        # Non-adiabatic parameter for the STT
        self.UppASDKeywords['spintorque']['adibeta'] =\
            float(self.text_to_num(window.InpBeta.text()))
        # Fill up the spin current vector for the STT
        self.UppASDKeywords['spintorque']['jvec'] =\
            [float(self.text_to_num(window.InpJvec_x.text())),
             float(self.text_to_num(window.InpJvec_y.text())),
             float(self.text_to_num(window.InpJvec_z.text()))]
        # Parameters for the SHE
        self.UppASDKeywords['spintorque']['thick_ferro'] =\
            float(self.text_to_num(window.InpSheThickness.text()))
        self.UppASDKeywords['spintorque']['she_angle'] =\
            float(self.text_to_num(window.InpShe.text()))
        # Check if SOT is going to be considered
        if window.InpSOTBox.isChecked():
            self.UppASDKeywords['spintorque']['do_sot'] = 'Y'
        else:
            self.UppASDKeywords['spintorque']['do_sot'] = 'N'
        # Parameters for the SOT
        self.UppASDKeywords['spintorque']['sot_field'] =\
            float(self.text_to_num(window.InpSOTFL.text()))
        self.UppASDKeywords['spintorque']['sot_damping'] =\
            float(self.text_to_num(window.InpSOTDL.text()))
        # Fill up the spin polarization vector for the SOT
        self.UppASDKeywords['spintorque']['sot_pol_vec'] =\
            [float(self.text_to_num(window.InpSOTPol_x.text())),
             float(self.text_to_num(window.InpSOTPol_y.text())),
             float(self.text_to_num(window.InpSOTPol_z.text()))]
        self.UppASDKeywords['LLG_mphase']['SDEAlgh'] =\
            int(window.InpASDLLGAlgh.value())
        # Set the symmetry of the exchange Hamiltonian
        self.UppASDKeywords['Hamiltonian']['Sym'] =\
            int(window.InpPairSym.value())
        # Set the maptype of the pairwise Hamiltonian
        self.UppASDKeywords['Hamiltonian']['maptype'] =\
            int(window.ImpPairMaptype.value())
        # Check if the dipole-dipole interaction should be considered
        if window.InpDipBruteForceCheck.isChecked():
            self.UppASDKeywords['Hamiltonian']['do_dip'] = 1
        if window.InpDipMacroCheck.isChecked():
            self.UppASDKeywords['Hamiltonian']['do_dip'] = 2
        if window.InpDipFFTCheck.isChecked():
            self.UppASDKeywords['Hamiltonian']['do_dip'] = 3
        if not window.InpDipBox.isChecked():
            self.UppASDKeywords['Hamiltonian']['do_dip'] = 0
        # Check which mode is going to be used for the measurement phase
        if window.InpMeasureLLG.isChecked():
            self.UppASDKeywords['general']['mode'] = 'S'
        if window.InpMeasureMCMet.isChecked():
            self.UppASDKeywords['general']['mode'] = 'M'
        if window.InpMeasureMCHeat.isChecked():
            self.UppASDKeywords['general']['mode'] = 'H'
        if window.InpMeasureGNEB.isChecked():
            self.UppASDKeywords['general']['mode'] = 'G'
            if window.InpGNEBMeasureBox.isChecked():
                self.UppASDKeywords['GNEB_mphase']['do_gneb'] = 'Y'
                self.UppASDKeywords['GNEB_mphase']['mep_ftol'] =\
                    float(self.text_to_num(window.InpGNEBMEPTol.text()))
                self.UppASDKeywords['GNEB_mphase']['mep_itrmax'] =\
                    int(self.text_to_num(window.InpGNEBMEPSteps.text()))
                self.UppASDKeywords['GNEB_mphase']['eig_zero'] =\
                    float(self.text_to_num(window.InpMinEig.text()))
            else:
                self.UppASDKeywords['GNEB_mphase']['do_gneb'] = 'N'
            if window.InpMeasureGNEBCI.isChecked():
                self.UppASDKeywords['GNEB_mphase']['do_gneb_ci'] = 'Y'
                self.UppASDKeywords['GNEB_mphase']['mep_ftol_ci'] =\
                    float(self.text_to_num(window.InpGNEBCITol.text()))
            else:
                self.UppASDKeywords['GNEB_mphase']['do_gneb_ci'] = 'N'
        # Check which is the initial magnetization of the system
        if window.InpInitmag1Check.isChecked():
            self.UppASDKeywords['Mag']['initmag'] = 1
        if window.InpInitmag2Box.isChecked():
            self.UppASDKeywords['Mag']['initmag'] = 2
        if window.InpInitmag3Check.isChecked():
            self.UppASDKeywords['Mag']['initmag'] = 3
        if window.InpInitMag4Check.isChecked():
            self.UppASDKeywords['Mag']['initmag'] = 4
            if len(self.restartfile) > 0:
                self.UppASDKeywords['Mag']['restartfile'] = self.restartfile
            else:
                print("No restartfile name given assuming"
                      "'./restart.dummy.dat'")
                self.UppASDKeywords['Mag']['restartfile'] =\
                    './restart.dummy.dat'
        if window.InpInitmag7Check.isChecked():
            self.UppASDKeywords['Mag']['initmag'] = 7
            if len(self.restartfile) > 0:
                self.UppASDKeywords['Mag']['restartfile'] = self.restartfile
            else:
                print("No restartfile name given assuming"
                      "'./restart.dummy.dat'")
                self.UppASDKeywords['Mag']['restartfile'] =\
                    './restart.dummy.dat'
        if window.InpInitMag6Check.isChecked():
            self.UppASDKeywords['Mag']['initmag'] = 6
            if len(self.momfile_in) > 0:
                self.UppASDKeywords['Mag']['momfile_i'] = self.momfile_in
            else:
                print('No momfile_i name given assuming "./momfile_i.dat"')
                self.UppASDKeywords['Mag']['momfile_i'] = './momfile_i.dat'
            if len(self.momfile_fi) > 0:
                self.UppASDKeywords['Mag']['momfile_f'] = self.momfile_fi
            else:
                print('No momfile_f name given assuming "./momfile_f.dat"')
                self.UppASDKeywords['Mag']['momfile_f'] = 'momfile_f.dat'
        if window.InpIniFinCheck.isChecked():
            self.UppASDKeywords['Mag']['initpath'] = 1
        if window.InpFullPathChek.isChecked():
            self.UppASDKeywords['Mag']['initpath'] = 2
        if window.InpRelGNEBCheck.isChecked():
            self.UppASDKeywords['Mag']['relaxed_if'] = 'Y'
        else:
            self.UppASDKeywords['Mag']['relaxed_if'] = 'N'
        # Check which is the initial phase of the calculation
        if window.InpInitBox.isChecked():
            if window.InpInitLLG.isChecked():
                self.UppASDKeywords['general']['ip_mode'] = 'S'
                self.UppASDKeywords['LLG_iphase']['ip_nphase'] =\
                    window.init_phase_data[0]
                self.UppASDKeywords['LLG_iphase'][''] =\
                    window.init_phase_data[1:]
            if window.InpInitMcMet.isChecked():
                self.UppASDKeywords['general']['ip_mode'] = 'M'
                self.UppASDKeywords['MC_iphase']['ip_mcanneal'] =\
                    window.init_phase_data[0]
                self.UppASDKeywords['MC_iphase'][''] =\
                    window.init_phase_data[1:]
            if window.InpInitMcHeat.isChecked():
                self.UppASDKeywords['general']['ip_mode'] = 'H'
                self.UppASDKeywords['MC_iphase']['ip_mcanneal'] =\
                    window.init_phase_data[0]
                self.UppASDKeywords['MC_iphase'][''] =\
                    window.init_phase_data[1:]
            if window.InpInitVPO.isChecked():
                self.UppASDKeywords['general']['ip_mode'] = 'G'
                self.UppASDKeywords['VPO_iphase']['min_itrmax'] =\
                    window.init_phase_data[0][0]
                self.UppASDKeywords['VPO_iphase']['spring'] =\
                    window.init_phase_data[0][1]
                self.UppASDKeywords['VPO_iphase']['vpo_mass'] =\
                    window.init_phase_data[0][2]
                self.UppASDKeywords['VPO_iphase']['vpo_dt'] =\
                    window.init_phase_data[0][3]
                self.UppASDKeywords['VPO_iphase']['min_ftol'] =\
                    window.init_phase_data[0][4]
        else:
            self.UppASDKeywords['general']['ip_mode'] = 'N'
        # Check if the energy will be printed
        if window.InpEneTotalEneCheck.isChecked():
            self.UppASDKeywords['energy']['plotenergy'] = 1
        if window.InpEneSiteEneCheck.isChecked():
            self.UppASDKeywords['energy']['plotenergy'] = 2
        if not window.InpEneTotalEneCheck.isChecked() and\
           not window.InpEneSiteEneCheck.isChecked():
            self.UppASDKeywords['energy']['plotenergy'] = 0
        # Check if the skyrmion number should be printed
        if window.SkxNumBox.isChecked():
            self.UppASDKeywords['topology']['skyrno'] = 'Y'
        else:
            self.UppASDKeywords['topology']['skyrno'] = 'N'
        # Check for the Hessian options
        if window.HessFinCheck.isChecked():
            self.UppASDKeywords['Hessians']['do_hess_fin'] = 'Y'
        else:
            self.UppASDKeywords['Hessians']['do_hess_fin'] = 'N'
        if window.HessInitCheck.isChecked():
            self.UppASDKeywords['Hessians']['do_hess_ini'] = 'Y'
        else:
            self.UppASDKeywords['Hessians']['do_hess_ini'] = 'N'
        if window.HessSPCheck.isChecked():
            self.UppASDKeywords['Hessians']['do_hess_sp'] = 'Y'
        else:
            self.UppASDKeywords['Hessians']['do_hess_sp'] = 'N'
        # Options for the printing of the skyrmion number
        if len(window.InpSkxStep.text()) > 0:
            self.UppASDKeywords['topology']['skyno_step'] =\
                int(self.text_to_num(window.InpSkxStep.text()))
        if len(window.InitSkxBuff.text()) > 0:
            self.UppASDKeywords['topology']['skyno_buff'] =\
                int(self.text_to_num(window.InitSkxBuff.text()))
        if len(window.InpDipBlockSizeLineEdit.text()) > 0:
            self.UppASDKeywords['Hamiltonian']['block_size'] =\
                int(self.text_to_num(window.InpDipBlockSizeLineEdit.text()))
        else:
            self.UppASDKeywords['Hamiltonian']['block_size'] = 1
        return

    def text_to_num(self, text=" "):
        """Function to ensure that if there is no entry it is set to zero

        Keyword Arguments:
            text {str} -- [description] (default: {" "})

        Returns:
            num {float} -- text converted to float
        """
        if len(text) > 0:
            num = float(text)
        else:
            num = 0.0
        return num

    def ASDInputConstrainer(self, window):
        """Function to constrain the data that can be put into the line-edits.
        The GUI has several editable entries, to ensure that one can
        only set the correct type of data validators are defined to ensure that
        only the correct data types can be entered.

        Arguments:
            window {[type]} -- [description]
        """
        window.InpN1.setValidator(self.IntegerValidator)
        window.InpN2.setValidator(self.IntegerValidator)
        window.InpN3.setValidator(self.IntegerValidator)
        window.InpDipBlockSizeLineEdit.setValidator(self.IntegerValidator)
        window.InpMensemble.setValidator(self.IntegerValidator)
        window.InpASDLLGSteps.setValidator(self.IntegerValidator)
        window.InpMCSteps.setValidator(self.IntegerValidator)
        window.InpGNEBMEPSteps.setValidator(self.IntegerValidator)
        window.InpAveStep.setValidator(self.IntegerValidator)
        window.InpAveBuff.setValidator(self.IntegerValidator)
        window.InpCumuStep.setValidator(self.IntegerValidator)
        window.InpCumuBuff.setValidator(self.IntegerValidator)
        window.InitTTrajStep.setValidator(self.IntegerValidator)
        window.InitTTrajBuff.setValidator(self.IntegerValidator)
        window.InpLineEditC1_x.setValidator(self.DoubleValidator)
        window.InpLineEditC1_y.setValidator(self.DoubleValidator)
        window.InpLineEditC1_z.setValidator(self.DoubleValidator)
        window.InpLineEditC2_x.setValidator(self.DoubleValidator)
        window.InpLineEditC2_y.setValidator(self.DoubleValidator)
        window.InpLineEditC2_z.setValidator(self.DoubleValidator)
        window.InpLineEditC3_x.setValidator(self.DoubleValidator)
        window.InpLineEditC3_y.setValidator(self.DoubleValidator)
        window.InpLineEditC3_z.setValidator(self.DoubleValidator)
        window.InpLineEditAlat.setValidator(self.PosDoubleValidator)
        window.InpBextInit_x.setValidator(self.DoubleValidator)
        window.InpBextInit_y.setValidator(self.DoubleValidator)
        window.InpBextInit_z.setValidator(self.DoubleValidator)
        window.InpBextMeasure_x.setValidator(self.DoubleValidator)
        window.InpBextMeasure_y.setValidator(self.DoubleValidator)
        window.InpBextMeasure_z.setValidator(self.DoubleValidator)
        window.InpInitMag2Theta.setValidator(self.DoubleValidator)
        window.InpInitMag2Phi.setValidator(self.DoubleValidator)
        window.InpASDLLGTemp.setValidator(self.PosDoubleValidator)
        window.InpASDLLGDT.setValidator(self.PosDoubleValidator)
        window.InpASDLLGDamp.setValidator(self.PosDoubleValidator)
        window.InpMCTemp.setValidator(self.PosDoubleValidator)
        window.InpMinEig.setValidator(self.PosDoubleValidator)
        window.InpGNEBMEPTol.setValidator(self.PosDoubleValidator)
        window.InpGNEBCITol.setValidator(self.PosDoubleValidator)
        window.InpShe.setValidator(self.DoubleValidator)
        window.InpSheThickness.setValidator(self.PosDoubleValidator)
        window.InpBeta.setValidator(self.PosDoubleValidator)
        window.InpJvec_x.setValidator(self.DoubleValidator)
        window.InpJvec_y.setValidator(self.DoubleValidator)
        window.InpJvec_z.setValidator(self.DoubleValidator)
        window.InpSOTFL.setValidator(self.DoubleValidator)
        window.InpSOTDL.setValidator(self.DoubleValidator)
        window.InpSOTPol_x.setValidator(self.DoubleValidator)
        window.InpSOTPol_y.setValidator(self.DoubleValidator)
        window.InpSOTPol_z.setValidator(self.DoubleValidator)
        window.InpScNStep.setValidator(self.IntegerValidator)
        window.InpScStep.setValidator(self.IntegerValidator)
        return

    def ASDSetDefaults(self):
        """Creates a dictionary with the entries needed to generate the
        `inpsd.dat`
        The dictionary is created and default values are setup so that
        a minimal simulation can be run.
        """
        import numpy as np
        import collections
        # General simulation variables
        self.UppASDKeywords['general'] = collections.OrderedDict()
        self.UppASDKeywords['general']['simid'] = '_UppASD_'
        self.UppASDKeywords['general']['mode'] = 'S'
        self.UppASDKeywords['general']['ip_mode'] = 'N'
        self.UppASDKeywords['general']['Temp'] = 0.001
        # Geometry variables
        self.UppASDKeywords['geometry'] = collections.OrderedDict()
        self.UppASDKeywords['geometry']['ncell'] = [1, 1, 1]
        self.UppASDKeywords['geometry']['BC'] = ['P', 'P', 'P']
        self.UppASDKeywords['geometry']['cell'] = np.identity(3)
        self.UppASDKeywords['geometry']['posfile'] = './posfile'
        self.UppASDKeywords['geometry']['momfile'] = './momfile'
        self.UppASDKeywords['geometry']['do_ralloy'] = 0
        self.UppASDKeywords['geometry']['alat'] = 3e-10
        # Hamiltonian variables
        self.UppASDKeywords['Hamiltonian'] = collections.OrderedDict()
        self.UppASDKeywords['Hamiltonian']['maptype'] = 2
        self.UppASDKeywords['Hamiltonian']['Sym'] = 0
        self.UppASDKeywords['Hamiltonian']['exchange'] = './jfile'
        self.UppASDKeywords['Hamiltonian']['do_dm'] = 0
        self.UppASDKeywords['Hamiltonian']['dm'] = './dmfile'
        self.UppASDKeywords['Hamiltonian']['do_anisotropy'] = 0
        self.UppASDKeywords['Hamiltonian']['anisotropy'] = './kfile'
        self.UppASDKeywords['Hamiltonian']['do_pd'] = 0
        self.UppASDKeywords['Hamiltonian']['pd'] = './pdfile'
        self.UppASDKeywords['Hamiltonian']['do_bq'] = 0
        self.UppASDKeywords['Hamiltonian']['bq'] = './bqfile'
        self.UppASDKeywords['Hamiltonian']['do_biqdm'] = 0
        self.UppASDKeywords['Hamiltonian']['biqdm'] = './biqdmfile'
        self.UppASDKeywords['Hamiltonian']['do_dip'] = 0
        self.UppASDKeywords['Hamiltonian']['block_size'] = 1
        self.UppASDKeywords['Hamiltonian']['hfield'] =\
            np.zeros([3], dtype=np.float64)
        self.UppASDKeywords['Hamiltonian']['ip_hfield'] =\
            np.zeros([3], dtype=np.float64)
        # LLG measure  variables
        self.UppASDKeywords['LLG_mphase'] = collections.OrderedDict()
        self.UppASDKeywords['LLG_mphase']['SDEAlgh'] = 1
        self.UppASDKeywords['LLG_mphase']['damping'] = 0.01
        self.UppASDKeywords['LLG_mphase']['timestep'] = 1e-16
        self.UppASDKeywords['LLG_mphase']['Nstep'] = 1000
        # LLG measure  variables
        self.UppASDKeywords['GNEB_mphase'] = collections.OrderedDict()
        self.UppASDKeywords['GNEB_mphase']['do_gneb'] = 'N'
        self.UppASDKeywords['GNEB_mphase']['do_gneb_ci'] = 'N'
        self.UppASDKeywords['GNEB_mphase']['mep_ftol'] = 0.001
        self.UppASDKeywords['GNEB_mphase']['mep_ftol_ci'] = 0.00000001
        self.UppASDKeywords['GNEB_mphase']['mep_itrmax'] = 10000000
        self.UppASDKeywords['GNEB_mphase']['eig_zero'] = 0.0001
        # LLG initial phase variables
        self.UppASDKeywords['LLG_iphase'] = collections.OrderedDict()
        self.UppASDKeywords['LLG_iphase']['ip_nphase'] = 0
        # VPO initial phase variables
        self.UppASDKeywords['VPO_iphase'] = collections.OrderedDict()
        # MC measure variables
        self.UppASDKeywords['MC_mphase'] = collections.OrderedDict()
        self.UppASDKeywords['MC_mphase']['mcnstep'] = 10000
        # MC initial phase variables
        self.UppASDKeywords['MC_iphase'] = collections.OrderedDict()
        self.UppASDKeywords['MC_iphase']['ip_mcanneal'] = 0
        # Magnetization variables
        self.UppASDKeywords['Mag'] = collections.OrderedDict()
        self.UppASDKeywords['Mag']['Mensemble'] = 1
        self.UppASDKeywords['Mag']['initmag'] = 3
        self.UppASDKeywords['Mag']['restartfile'] = './restart.dummy.dat'
        # Correlation variables
        self.UppASDKeywords['Mag_corr'] = collections.OrderedDict()
        self.UppASDKeywords['Mag_corr']['do_sc'] = 'N'
        self.UppASDKeywords['Mag_corr']['sc_step'] = 10
        self.UppASDKeywords['Mag_corr']['sc_nstep'] = 100
        self.UppASDKeywords['Mag_corr']['qpoints'] = 'F'
        self.UppASDKeywords['Mag_corr']['qfile'] = './qfile'
        self.UppASDKeywords['Mag_corr']['do_ams'] = 'N'
        # Spin-Torques variables
        self.UppASDKeywords['spintorque'] = collections.OrderedDict()
        self.UppASDKeywords['spintorque']['stt'] = 'N'
        self.UppASDKeywords['spintorque']['adibeta'] = 0.01
        self.UppASDKeywords['spintorque']['jvec'] =\
            np.zeros([3], dtype=np.float64)
        self.UppASDKeywords['spintorque']['do_sot'] = 'N'
        self.UppASDKeywords['spintorque']['do_she'] = 'N'
        self.UppASDKeywords['spintorque']['sot_field'] = 0.0
        self.UppASDKeywords['spintorque']['sot_damping'] = 0.0
        self.UppASDKeywords['spintorque']['sot_pol_vec'] =\
            np.zeros([3], dtype=np.float64)
        self.UppASDKeywords['spintorque']['thick_ferro'] = 1.0
        self.UppASDKeywords['spintorque']['she_angle'] = 0.0
        # Prn avrg variables
        self.UppASDKeywords['averages'] = collections.OrderedDict()
        self.UppASDKeywords['averages']['do_avrg'] = 'Y'
        self.UppASDKeywords['averages']['avrg_step'] = 1000
        self.UppASDKeywords['averages']['avrg_buff'] = 100
        self.UppASDKeywords['averages']['do_proj_avrg'] = 'N'
        self.UppASDKeywords['averages']['do_projch_avrg'] = 'N'
        # Prn tottraj variables
        self.UppASDKeywords['trajectories'] = collections.OrderedDict()
        self.UppASDKeywords['trajectories']['do_tottraj'] = 'N'
        self.UppASDKeywords['trajectories']['tottraj_step'] = 1000
        self.UppASDKeywords['trajectories']['tottraj_buff'] = 100
        # Prn cumulants variables
        self.UppASDKeywords['cumulants'] = collections.OrderedDict()
        self.UppASDKeywords['cumulants']['do_cumu'] = 'Y'
        self.UppASDKeywords['cumulants']['cumu_step'] = 1000
        self.UppASDKeywords['cumulants']['cumu_buff'] = 100
        # Prn skyrmion
        self.UppASDKeywords['topology'] = collections.OrderedDict()
        self.UppASDKeywords['topology']['skyrno'] = 'N'
        self.UppASDKeywords['topology']['skyno_step'] = 1000
        self.UppASDKeywords['topology']['skyno_buff'] = 100
        # Prn energy
        self.UppASDKeywords['energy'] = collections.OrderedDict()
        self.UppASDKeywords['energy']['plotenergy'] = 1
        # Hessian
        self.UppASDKeywords['Hessians'] = collections.OrderedDict()
        self.UppASDKeywords['Hessians']['do_hess_ini'] = 'N'
        self.UppASDKeywords['Hessians']['do_hess_fin'] = 'N'
        self.UppASDKeywords['Hessians']['do_hess_sp'] = 'N'
        return

    def clean_var(self):
        """The function makes sure to eliminate un-needed entries to get
        the minimal `inpsd.dat`
        """
        import numpy as np
        tol = 1e-10
        if self.UppASDKeywords['spintorque']['stt'] == 'N' and\
           self.UppASDKeywords['spintorque']['do_she'] == 'N':
            del self.UppASDKeywords['spintorque']['jvec']
        if self.UppASDKeywords['spintorque']['stt'] == 'N':
            del self.UppASDKeywords['spintorque']['adibeta']
            del self.UppASDKeywords['spintorque']['stt']
        if self.UppASDKeywords['spintorque']['do_she'] == 'N':
            del self.UppASDKeywords['spintorque']['she_angle']
            del self.UppASDKeywords['spintorque']['thick_ferro']
            del self.UppASDKeywords['spintorque']['do_she']
        if self.UppASDKeywords['spintorque']['do_sot'] == 'N':
            del self.UppASDKeywords['spintorque']['sot_pol_vec']
            del self.UppASDKeywords['spintorque']['sot_damping']
            del self.UppASDKeywords['spintorque']['sot_field']
            del self.UppASDKeywords['spintorque']['do_sot']
        # S(q,w) and AMS flags
        if self.UppASDKeywords['Mag_corr']['qpoints'] != 'F':
            del self.UppASDKeywords['Mag_corr']['qfile']
        if self.UppASDKeywords['Mag_corr']['do_sc'] == 'N' and\
           self.UppASDKeywords['Mag_corr']['do_ams'] == 'N':
            del self.UppASDKeywords['Mag_corr']['qfile']
            del self.UppASDKeywords['Mag_corr']['qpoints']
        if self.UppASDKeywords['Mag_corr']['do_sc'] == 'N':
            del self.UppASDKeywords['Mag_corr']['sc_step']
            del self.UppASDKeywords['Mag_corr']['sc_nstep']
            del self.UppASDKeywords['Mag_corr']['do_sc']
        if self.UppASDKeywords['Mag_corr']['do_ams'] == 'N':
            del self.UppASDKeywords['Mag_corr']['do_ams']
        # dipolar flags
        if self.UppASDKeywords['Hamiltonian']['do_dip'] != 2:
            del self.UppASDKeywords['Hamiltonian']['block_size']
        if self.UppASDKeywords['Hamiltonian']['do_dip'] == 0:
            del self.UppASDKeywords['Hamiltonian']['do_dip']
        # DMI flags
        if self.UppASDKeywords['Hamiltonian']['do_dm'] == 0:
            del self.UppASDKeywords['Hamiltonian']['do_dm']
            del self.UppASDKeywords['Hamiltonian']['dm']
        # Anisotropy flags
        if self.UppASDKeywords['Hamiltonian']['do_anisotropy'] == 0:
            del self.UppASDKeywords['Hamiltonian']['do_anisotropy']
            del self.UppASDKeywords['Hamiltonian']['anisotropy']
        # Biquadratic interaction flags
        if self.UppASDKeywords['Hamiltonian']['do_bq'] == 0:
            del self.UppASDKeywords['Hamiltonian']['do_bq']
            del self.UppASDKeywords['Hamiltonian']['bq']
        # Pseudo dipolar flags
        if self.UppASDKeywords['Hamiltonian']['do_pd'] == 0:
            del self.UppASDKeywords['Hamiltonian']['do_pd']
            del self.UppASDKeywords['Hamiltonian']['pd']
        # Biquadratic DM interaction flags
        if self.UppASDKeywords['Hamiltonian']['do_biqdm'] == 0:
            del self.UppASDKeywords['Hamiltonian']['do_biqdm']
            del self.UppASDKeywords['Hamiltonian']['biqdm']
        # IP Mode flags
        if self.UppASDKeywords['general']['ip_mode'] == 'N':
            del self.UppASDKeywords['general']['ip_mode']
            del self.UppASDKeywords['Hamiltonian']['ip_hfield']
            del self.UppASDKeywords['MC_iphase']
            del self.UppASDKeywords['LLG_iphase']
            del self.UppASDKeywords['VPO_iphase']
        else:
            if self.UppASDKeywords['general']['ip_mode'] != 'M' and\
               self.UppASDKeywords['general']['ip_mode'] != 'H':
                del self.UppASDKeywords['MC_iphase']
            if self.UppASDKeywords['general']['ip_mode'] != 'S':
                del self.UppASDKeywords['LLG_iphase']
            if self.UppASDKeywords['general']['ip_mode'] != 'G':
                del self.UppASDKeywords['VPO_iphase']
        # Mode flags
        if self.UppASDKeywords['general']['mode'] != 'M' and\
           self.UppASDKeywords['general']['mode'] != 'H':
            del self.UppASDKeywords['MC_mphase']
        if self.UppASDKeywords['general']['mode'] != 'S':
            del self.UppASDKeywords['LLG_mphase']
        if self.UppASDKeywords['general']['mode'] != 'G':
            del self.UppASDKeywords['GNEB_mphase']
        # initmag flags
        if self.UppASDKeywords['Mag']['initmag'] != 4 and\
           self.UppASDKeywords['Mag']['initmag'] != 7:
            del self.UppASDKeywords['Mag']['restartfile']
        # Measurement field flag
        if np.linalg.norm(self.UppASDKeywords['Hamiltonian']
                          ['hfield'][:]) < tol:
            del self.UppASDKeywords['Hamiltonian']['hfield']
        # Random alloy flags
        if self.UppASDKeywords['geometry']['do_ralloy'] == 0:
            del self.UppASDKeywords['geometry']['do_ralloy']
        # Prn trajectories
        if self.UppASDKeywords['trajectories']['do_tottraj'] == 'N':
            del self.UppASDKeywords['trajectories']
        # Prn averages
        if self.UppASDKeywords['averages']['do_avrg'] == 'N':
            del self.UppASDKeywords['averages']
        # Prn topology
        if self.UppASDKeywords['topology']['skyrno'] == 'N':
            del self.UppASDKeywords['topology']
        # Prn cumulants
        if self.UppASDKeywords['cumulants']['do_cumu'] == 'N':
            del self.UppASDKeywords['cumulants']
        # Prn averages
        if self.UppASDKeywords['averages']['do_avrg'] == 'N':
            del self.UppASDKeywords['averages']
        else:
            if self.UppASDKeywords['averages']['do_proj_avrg'] == 'N':
                del self.UppASDKeywords['averages']['do_proj_avrg']
            if self.UppASDKeywords['averages']['do_projch_avrg'] == 'N':
                del self.UppASDKeywords['averages']['do_projch_avrg']
        # Hessians
        if self.UppASDKeywords['Hessians']['do_hess_ini'] == 'N':
            del self.UppASDKeywords['Hessians']['do_hess_ini']
        if self.UppASDKeywords['Hessians']['do_hess_fin'] == 'N':
            del self.UppASDKeywords['Hessians']['do_hess_fin']
        if self.UppASDKeywords['Hessians']['do_hess_sp'] == 'N':
            del self.UppASDKeywords['Hessians']['do_hess_sp']
        if self.UppASDKeywords['Mag']['relaxed_if'] == 'N':
            del self.UppASDKeywords['Mag']['relaxed_if']
        #for name in self.UppASDKeywords:
        #    if len(self.UppASDKeywords[name]) == 0:
        #        del self.UppASDKeywords[name]
        return

    def write_inpsd(self):
        """Function to write a standard `inpsd.dat` and a `inpsd.yaml` file

        Author:
            Jonathan Chico
        """
        import collections
        import yaml
        yaml.add_representer(collections.OrderedDict, lambda dumper, data:
                             dumper.represent_mapping('tag:yaml.org,2002:map',
                                                      data.items()))
        with open('inpsd.yaml', 'w') as outfile:
            yaml.dump(self.UppASDKeywords, outfile, default_flow_style=False)

        inpsd_file = open('inpsd.dat', 'w')
        for name in self.UppASDKeywords:
            for descriptor in self.UppASDKeywords[name]:
                current = self.UppASDKeywords[name][descriptor]
                if isinstance(current, list):
                    if len(descriptor) > 0:
                        inpsd_file.write('{descriptor}  '.format(**locals()))
                    for ii in range(len(current)):
                        line = current[ii]
                        if isinstance(line, list):
                            for jj in range(len(line)):
                                entry = line[jj]
                                inpsd_file.write('{entry}  '
                                                 .format(**locals()))
                            inpsd_file.write('\n')
                        else:
                            inpsd_file.write('{line}  '.format(**locals()))
                    inpsd_file.write('\n')
                elif isinstance(current, tuple):
                    inpsd_file.write('{descriptor} '.format(**locals()))
                    for ii in range(len(current)):
                        entry = current[ii]
                        inpsd_file.write('{entry}  '.format(**locals()))
                    inpsd_file.write('\n')
                else:
                    inpsd_file.write('{descriptor}  {current}\n'
                                     .format(**locals()))
            inpsd_file.write('\n')
        return
