"""@package ASDVTKMomActors
Wrapper class to add the VTK  actors for the visualization of UppASD data in
moment visualization mode.
It contains the needed data to add the actors, modify them, as well as some
helper functions to change them.

Author
----------
Jonathan Chico
"""


class ASDMomActors():

    def __init__(self):
        import numpy as np
        self.timer_count = 0
        self.camera_pos = np.zeros(3, dtype=np.float32)
        self.camera_focal = np.zeros(3, dtype=np.float32)
        self.camera_yaw = 0.0
        self.camera_roll = 0.0
        self.camera_pitch = 0.0
        self.camera_azimuth = 0.0
        self.camera_elevation = 0.0

        return

    def Add_MomActors(self, ren, renWin, iren, ASDdata, window):
        """Main wrapper to add the needed actors for visualization of the
        moments.
        Class that contains the data structures for creation of the glyphs or
        the visualization of the magnetic moments. It also has the capacity to
        create tessellations for the visualization of volume rendering.

        Arguments:
            ren {[type]} -- current renderer.
            renWin {vtkWidget render window} -- current rendering window.
            iren {[type]} -- current interactor for the renderer.
            ASDdata {class} -- class where the data read from the ASD
            simulations is stored.
            window {QMainWindow object} -- QMainWindow object where the
            visualization is performed.

        Author:
            Jonathan Chico
        """

        import vtk
        import numpy as np

        self.kmc_disp = ASDdata.kmc_flag
        self.cluster_disp = ASDdata.cluster_flag
        self.glob_color = ASDdata.colors
        # Look up tables for colors
        # This is a diverging RWB color mapping based on the work of Kenneth
        # Moreland and with the vtk examples provided by Andrew Maclean
        if ASDdata.flag2D:
            self.lut = vtk.vtkLookupTable()
            num_colors = 256
            self.lut.SetNumberOfTableValues(num_colors)
            self.transfer_func = vtk.vtkColorTransferFunction()
            self.transfer_func.SetColorSpaceToDiverging()
            self.transfer_func.AddRGBPoint(0, 0.230, 0.299, 0.754)
            self.transfer_func.AddRGBPoint(1, 0.706, 0.016, 0.150)
            for ii, ss in enumerate([float(xx)/float(num_colors) for xx in
                                     range(num_colors)]):
                cc = self.transfer_func.GetColor(ss)
                self.lut.SetTableValue(ii, cc[0], cc[1], cc[2], 1.0)
            self.lut.Build()
        else:
            self.lut = vtk.vtkLookupTable()
            num_colors = 256
            self.lut.SetNumberOfTableValues(num_colors)
            self.transfer_func = vtk.vtkColorTransferFunction()
            self.transfer_func.SetColorSpaceToDiverging()
            self.transfer_func.AddRGBPoint(-0, 0.230, 0.299, 0.754)
            self.transfer_func.AddRGBPoint(1, 0.706, 0.016, 0.150)
            for ii, ss in enumerate([float(xx)/float(num_colors) for xx in
                                     range(num_colors)]):
                cc = self.transfer_func.GetColor(ss)
                self.lut.SetTableValue(ii, cc[0], cc[1], cc[2], 1.0)
            self.lut.Build()
        # Data structures for the generation of the smooth grid
        # Passing the data from the full system to the PolyData
        self.src = vtk.vtkPolyData()
        self.src.SetPoints(ASDdata.coord)
        self.src.GetPointData().SetScalars(ASDdata.colors[2])
        self.src.GetPointData().SetVectors(ASDdata.moments)
        scalar_range = self.src.GetScalarRange()
        # Finding useful geometrical information of the sample
        # Finding the middle of the sample
        # Also making sure that if the sample is 2D one has no problem with
        # bounds this is mostly useful if splatters are used
        (self.xmin, self.xmax, self.ymin, self.ymax, self.zmin, self.zmax) =\
            self.src.GetBounds()
        if self.xmin == self.xmax:
            self.xmin = 0.0
            self.xmax = 1.0
        if self.ymin == self.ymax:
            self.ymin = 0.0
            self.ymax = 1.0
        if self.zmin == self.zmax:
            self.zmin = 0.0
            self.zmax = 1.0
        self.xmid = (self.xmin + self.xmax)*0.5
        self.ymid = (self.ymin + self.ymax)*0.5
        self.zmid = (self.zmin + self.zmax)*0.5
        self.height = np.amax([self.xmax, self.ymax, self.zmax])*1.75
        self.dist_x = np.absolute(self.xmax - self.xmin)
        self.dist_y = np.absolute(self.ymax - self.ymin)
        self.dist_z = np.absolute(self.zmax - self.zmin)
        self.camera_pos[:] = [self.xmid, self.ymid, self.height]
        self.camera_focal[:] = [self.xmid, self.ymid, self.zmid]
        # The delaunay tessellation seems to be the best way to transform the
        # point cloud to a surface for volume rendering, the problem is that
        # it is too slow for large data sets, meaning that the best option is
        # first to prune out the data to ensure that one has a manageable
        # number of data points over which to do the construction surface
        # reconstruction and splatter techniques also can be used to generate
        # something akin to the kind of surfaces we want.
        # The issue is that they transform the data to a regular mesh by
        # default. And thus it is a problem for most kind of systems
        if ASDdata.flag2D:
            # Passing the data to generate a triangulation of the data
            self.MagDensMethod = vtk.vtkDelaunay2D()
            self.MagDensMethod.SetInputData(self.src)
            self.MagDensMethod.BoundingTriangulationOff()
            self.MagDensMethod.SetTolerance(0.005)
            # Time the execution of the delaunay tessellation
            sm_timer = vtk.vtkExecutionTimer()
            sm_timer.SetFilter(self.MagDensMethod)
            self.MagDensMethod.Update()
            sm = sm_timer.GetElapsedWallClockTime()
            print("2D Delaunay:", sm)
            # Creating the mapper for the smooth surfaces
            self.MagDensMap = vtk.vtkDataSetMapper()
            self.MagDensMap.SetScalarRange(scalar_range)
            self.MagDensMap.\
                SetInputConnection(self.MagDensMethod.GetOutputPort())
            self.MagDensMap.SetLookupTable(self.lut)
            self.MagDensMap.SetColorModeToMapScalars()
            self.MagDensMap.Update()
            # Creating the actor for the smooth surfaces
            self.MagDensActor = vtk.vtkLODActor()
            self.MagDensActor.SetMapper(self.MagDensMap)
            self.MagDensActor.GetProperty().SetOpacity(0.75)
            self.MagDensActor.GetProperty().EdgeVisibilityOff()
            if window.DensBox.isChecked():
                self.MagDensActor.VisibilityOn()
            else:
                self.MagDensActor.VisibilityOff()
        else:
            # Setting the parameters for the visualization of 3D structures
            # with splatters
            self.MagDensMethod = vtk.vtkGaussianSplatter()
            self.MagDensMethod.SetInputData(self.src)
            # Options for the Gaussian splatter. These determine the quality
            # of the rendering, increasing the radius smoothens out the volume
            # but performance decreases rapidly
            dist = np.asarray(self.src.GetPoint(0))\
                - np.asarray(self.src.GetPoint(1))
            norm = np.sqrt(dist.dot(dist))
            if norm < 1:
                rad_fac = 0.040
            else:
                rad_fac = 0.40
            self.MagDensMethod.SetRadius(rad_fac)
            self.MagDensMethod.ScalarWarpingOn()
            # The exponent factor determines how fast the gaussian splatter
            # decay they again can be used to improve quality at the sake of
            # rendering time
            self.MagDensMethod.SetExponentFactor(-10)
            self.MagDensMethod.NormalWarpingOn()
            self.MagDensMethod.SetEccentricity(10)
            # The Null value can be used to try to eliminate contributions not
            # belonging to the actual sample
            self.MagDensMethod.SetNullValue(-10)
            # Set the actual size of the rendering model
            self.MagDensMethod.SetModelBounds(self.xmin, self.xmax, self.ymin,
                                              self.ymax, self.zmin, self.zmax)
            # This should get rid of the problems when trying to map very thin
            # structures in 2D
            _min = np.amin([self.dist_x, self.dist_y, self.dist_z])
            if self.dist_x == _min and self.dist_x < 3:
                self.MagDensMethod.SetSampleDimensions(3, int(self.ymax),
                                                       int(self.zmax))
            elif self.dist_y == _min and self.dist_y < 3:
                self.MagDensMethod.SetSampleDimensions(int(self.xmax), 3,
                                                       int(self.zmax))
            elif self.dist_z == _min and self.dist_z < 3:
                self.MagDensMethod.SetSampleDimensions(int(self.xmax),
                                                       int(self.ymax), 3)
            # Timming for the execution of the volume creation
            sp_timer = vtk.vtkExecutionTimer()
            sp_timer.SetFilter(self.MagDensMethod)
            self.MagDensMethod.Update()
            sp = sp_timer.GetElapsedWallClockTime()
            print("3D vtkGaussianSplatter Method:", sp)
            # Scalar opacities
            funcOpacityScalar = vtk.vtkPiecewiseFunction()
            funcOpacityScalar.AddPoint(-1.00, 0.00)
            funcOpacityScalar.AddPoint(0.00, 0.05)
            funcOpacityScalar.AddPoint(0.50, 0.50)
            funcOpacityScalar.AddPoint(0.75, 1.00)
            # Gradient opacities
            volumeGradientOpacity = vtk.vtkPiecewiseFunction()
            volumeGradientOpacity.AddPoint(0.000, 0.0)
            volumeGradientOpacity.AddPoint(0.001, 1.0)
            volumeGradientOpacity.AddPoint(1.000, 1.0)
            # Volume properties
            self.volumeProperty = vtk.vtkVolumeProperty()
            self.volumeProperty.SetColor(self.transfer_func)
            self.volumeProperty.SetInterpolationTypeToLinear()
            self.volumeProperty.SetAmbient(0.6)
            self.volumeProperty.SetDiffuse(0.6)
            self.volumeProperty.SetSpecular(0.1)
            self.volumeProperty.SetGradientOpacity(volumeGradientOpacity)
            self.volumeProperty.SetScalarOpacity(funcOpacityScalar)
            # Volume Mapper
            self.MagDensMap = vtk.vtkSmartVolumeMapper()
            self.MagDensMap.\
                SetInputConnection(self.MagDensMethod.GetOutputPort())
            # Volume Actor
            self.MagDensActor = vtk.vtkVolume()
            self.MagDensActor.SetMapper(self.MagDensMap)
            self.MagDensActor.SetProperty(self.volumeProperty)
            if window.DensBox.isChecked():
                self.MagDensActor.VisibilityOn()
            else:
                self.MagDensActor.VisibilityOff()
        # Data structures for the spins
        # Passing the data from the full system to the PolyData
        self.src_spins = vtk.vtkPolyData()
        self.src_spins.SetPoints(ASDdata.coord)
        self.src_spins.GetPointData().SetScalars(ASDdata.colors[2])
        self.src_spins.GetPointData().SetVectors(ASDdata.moments)
        scalar_range_spins = self.src_spins.GetScalarRange()
        # Data structures for the contours
        # Define the contour filters
        contours = vtk.vtkContourFilter()
        contours.SetInputConnection(self.MagDensMethod.GetOutputPort())
        # This generates the contours, it will do 5 between the -1 and 0.5
        # range
        cont_num = 5
        range_cont = (-1.0, 0.5)
        contours.GenerateValues(cont_num, range_cont)
        # Map the contours to graphical primitives
        contMapper = vtk.vtkPolyDataMapper()
        contMapper.SetInputConnection(contours.GetOutputPort())
        contMapper.SetScalarVisibility(False)  # colored contours
        contMapper.SetScalarRange(scalar_range)
        # Create an actor for the contours
        self.contActor = vtk.vtkLODActor()
        self.contActor.SetMapper(contMapper)
        self.contActor.GetProperty().SetColor(0, 0, 0)
        self.contActor.GetProperty().SetLineWidth(1.0)
        self.contActor.VisibilityOff()
        # Setting information of the directions
        # Create vectors
        arrow = vtk.vtkArrowSource()
        arrow.SetTipRadius(0.20)
        arrow.SetShaftRadius(0.10)
        arrow.SetTipResolution(20)
        arrow.SetShaftResolution(20)
        # Create the mapper for the spins
        arrowMapper = vtk.vtkGlyph3DMapper()
        arrowMapper.SetSourceConnection(arrow.GetOutputPort())
        arrowMapper.SetInputData(self.src)
        arrowMapper.SetScaleFactor(0.50)
        arrowMapper.SetScalarVisibility(False)
        arrowMapper.SetScaleModeToNoDataScaling()
        arrowMapper.Update()
        # Define the vector actor for the spins
        self.vector = vtk.vtkLODActor()
        self.vector.SetMapper(arrowMapper)
        self.vector.GetProperty().SetSpecular(0.3)
        self.vector.GetProperty().SetSpecularPower(60)
        self.vector.GetProperty().SetAmbient(0.2)
        self.vector.GetProperty().SetDiffuse(0.8)
        self.vector.GetProperty().SetColor(0, 0, 0)
        self.vector.VisibilityOff()
        # Setting information of the spins
        # Create vectors
        self.spinarrow = vtk.vtkArrowSource()
        self.spinarrow.SetTipRadius(0.20)
        self.spinarrow.SetShaftRadius(0.10)
        self.spinarrow.SetTipResolution(20)
        self.spinarrow.SetShaftResolution(20)
        # Create the mapper for the spins
        self.SpinMapper = vtk.vtkGlyph3DMapper()
        self.SpinMapper.SetSourceConnection(self.spinarrow.GetOutputPort())
        self.SpinMapper.SetInputData(self.src_spins)
        self.SpinMapper.SetScalarRange(scalar_range_spins)
        self.SpinMapper.SetScaleFactor(0.50)
        self.SpinMapper.SetScaleModeToNoDataScaling()
        self.SpinMapper.SetLookupTable(self.lut)
        self.SpinMapper.SetColorModeToMapScalars()
        self.SpinMapper.Update()
        # Define the vector actor for the spins
        self.Spins = vtk.vtkLODActor()
        self.Spins.SetMapper(self.SpinMapper)
        self.Spins.GetProperty().SetSpecular(0.3)
        self.Spins.GetProperty().SetSpecularPower(60)
        self.Spins.GetProperty().SetAmbient(0.2)
        self.Spins.GetProperty().SetDiffuse(0.8)
        if window.SpinsBox.isChecked():
            self.Spins.VisibilityOn()
        else:
            self.Spins.VisibilityOff()
        if ASDdata.kmc_flag:
            # Setting data structures for the KMC particle visualization
            self.kmc_src = vtk.vtkPolyData()
            self.kmc_src.SetPoints(ASDdata.coord_KMC)
            # Atom sphere
            kmc_part = vtk.vtkSphereSource()
            kmc_part.SetRadius(1.75)
            kmc_part.SetThetaResolution(40)
            kmc_part.SetPhiResolution(40)
            # Atom glyph
            kmc_part_mapper = vtk.vtkGlyph3DMapper()
            kmc_part_mapper.SetInputData(self.kmc_src)
            kmc_part_mapper.SetSourceConnection(kmc_part.GetOutputPort())
            kmc_part_mapper.SetScaleFactor(0.5)
            kmc_part_mapper.ClampingOn()
            kmc_part_mapper.SetScaleModeToNoDataScaling()
            kmc_part_mapper.SetColorModeToMapScalars()
            kmc_part_mapper.Update()
            # Atoms actors
            self.kmc_part_actor = vtk.vtkLODActor()
            self.kmc_part_actor.SetMapper(kmc_part_mapper)
            self.kmc_part_actor.GetProperty().SetOpacity(0.9)
            self.kmc_part_actor.GetProperty().SetColor(0.0, 0.0, 1.0)
            self.kmc_part_actor.GetProperty().EdgeVisibilityOn()
            self.kmc_part_actor.GetProperty().SetEdgeColor(0, 0, 0)
        # Setting information of the renderer
        # Define the renderer
        # Add the actors to the scene
        if ASDdata.flag2D:
            ren.AddActor(self.MagDensActor)
        else:
            ren.AddViewProp(self.MagDensActor)
        ren.AddActor(self.Spins)
        ren.AddActor(self.vector)
        ren.AddActor(self.contActor)
        # If the KMC particles are present add them to the renderer
        if ASDdata.kmc_flag:
            ren.AddActor(self.kmc_part_actor)
        # Defining the camera directions
        ren.GetActiveCamera().Azimuth(self.camera_azimuth)
        ren.GetActiveCamera().Elevation(self.camera_elevation)
        ren.GetActiveCamera().Yaw(self.camera_yaw)
        ren.GetActiveCamera().Roll(self.camera_roll)
        ren.GetActiveCamera().Pitch(self.camera_pitch)
        ren.GetActiveCamera().SetFocalPoint(self.camera_focal)
        ren.GetActiveCamera().SetPosition(self.camera_pos)
        ren.GetActiveCamera().SetViewUp(0, 1, 0)
        # Start the renderer
        iren.Start()
        renWin.Render()
        return

    def UpdateMoments(self, window, ASDdata, ASDGenActors, renWin):
        """Function to update the visualization of the moments as one advances
        in time or in ensembles.
        Arguments:
            window {QMainWindow object} -- QMainWindow object where the
            visualizations are contained.
            ASData {class} -- class containing the data read from the UppASD
            simulations.
            ASDGenActors {class} -- class of general actors for the VTK
            visualization.
            renWin {vtkWidget render window} -- current rendering window.

        Author:
            Jonathan Chico
        """
        # Read the actual data of the magnetic moments
        (ASDdata.moments, ASDdata.colors, ASDdata.number_time_steps,
         ASDdata.time_sep) =\
            ASDdata.readVectorsData(ASDdata.MagFile, window.current_time,
                                    ASDdata.num_atoms,
                                    ASDdata.number_time_steps)
        # Update the colors
        if window.DensX.isChecked():
            self.src.GetPointData().SetScalars(ASDdata.colors[0])
        if window.SpinX.isChecked():
            self.src_spins.GetPointData().SetScalars(ASDdata.colors[0])
        if window.DensY.isChecked():
            self.src.GetPointData().SetScalars(ASDdata.colors[1])
        if window.SpinY.isChecked():
            self.src_spins.GetPointData().SetScalars(ASDdata.colors[1])
        if window.DensZ.isChecked():
            self.src.GetPointData().SetScalars(ASDdata.colors[2])
        if window.SpinZ.isChecked():
            self.src_spins.GetPointData().SetScalars(ASDdata.colors[2])
        # Update the vectors
        self.src.GetPointData().SetVectors(ASDdata.moments)
        self.src_spins.GetPointData().SetVectors(ASDdata.moments)
        # Update the general actors
        window.ProgressBar.setValue((window.current_time-1)*100 /
                                    (ASDdata.number_time_steps-1))
        window.ProgressLabel.\
            setText('   {:}%'.format(int(window.ProgressBar.value())))
        time_label =\
            str('{: 4.2f}'.format(float(window.TimeStepLineEdit.text()) *
                                  ASDdata.time_sep
                                  [window.current_time - 1]*1e9)) + ' ns'
        ASDGenActors.time_label.SetInput(time_label)
        # Render the window
        renWin.Render()
        return
