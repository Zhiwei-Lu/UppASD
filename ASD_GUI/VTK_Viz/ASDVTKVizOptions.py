"""@package ASDVTKVizOptions
Contains a class with a set of functions dealing with the visualization options
in the VTK visualization mode. Dealing with visibility of objects, size of
glyphs, types of glyphs, colormap used etc.

Author
----------
Jonathan Chico
"""


class ASDVizOptions():

    def __init__(self, GenActors, EneActors, MomActors, NeighActors):

        self.GenActors = GenActors
        self.EneActors = EneActors
        self.MomActors = MomActors
        self.NeighActors = NeighActors

        return

    def Screenshot(self, renWin, number_of_screenshots=0, png_mode=True,
                   pov_mode=False):
        """Function to take the rendering window and save it to file, either in
        .png or in .pov format.

        Arguments:
            renWin {vtkWidget render window} -- current rendering window.
            number_of_screenshots {int} -- current number of the screenshot
            that is being saved. (default: {0})
            png_mode {bool} -- variable indicated if the scene should be
            stored as a png (default: {True})
            pov_mode {bool} -- variable indicated if the scene should be
            stored as a pov (default: {False})

        Author:
            Anders Bergman
        """

        from vtk import vtkWindowToImageFilter, vtkPOVExporter, vtkPNGWriter

        win2im = vtkWindowToImageFilter()
        win2im.SetInput(renWin)
        win2im.Update()
        win2im.SetInputBufferTypeToRGBA()
        win2im.ReadFrontBufferOff()
        # Save snapshot as a '.pov'
        if pov_mode:
            povexp = vtkPOVExporter()
            povexp.SetInput(renWin)
            renWin.Render()
            povexp.SetFileName('snap%.5d.pov' % number_of_screenshots)
            povexp.Write()
        # Save snapshot as a '.png'
        if png_mode:
            toPNG = vtkPNGWriter()
            toPNG.SetFileName('snap%.5d.png' % number_of_screenshots)
            toPNG.SetInputConnection(win2im.GetOutputPort())
            toPNG.Write()
        return

    def toggle_projections(self, renWin, window, ren, checked=False):
        if checked:
            ren.GetActiveCamera().ParallelProjectionOn()
            window.ParallelScaleLineEdit.\
                setText(str(window.ParallelScaleSlider.value()))
            ren.GetActiveCamera().\
                SetParallelScale(float(window.ParallelScaleLineEdit.text()))
            renWin.Render()
        else:
            ren.GetActiveCamera().ParallelProjectionOff()
            renWin.Render()
        return

    def ChangeParallelProj(self, ren, renWin, line, slider, MainWindow):
        if line:
            MainWindow.ParallelScaleSlider.\
                setValue(float(MainWindow.ParallelScaleLineEdit.text()))
            ren.GetActiveCamera().\
                SetParallelScale(float(MainWindow.
                                       ParallelScaleLineEdit.text()))
            renWin.Render()
        if slider:
            ren.GetActiveCamera().\
                SetParallelScale(MainWindow.ParallelScaleSlider.value())
            MainWindow.ParallelScaleLineEdit.\
                setText(str(MainWindow.ParallelScaleSlider.value()))
            renWin.Render()
        return

    def reset_camera(self, ren, renWin, current_Actors):
        """Function to reset the camera to the initial position.

        Arguments:
            ren-- current VTK renderer.
            renWin {vtkWidget render window} -- current rendering window.
            current_Actors -- current actors which are being visualized.

        Author:
            Jonathan Chico
        """
        # Defining the camera directions
        ren.GetActiveCamera().SetFocalPoint(current_Actors.xmid,
                                            current_Actors.ymid,
                                            current_Actors.zmid)
        ren.GetActiveCamera().SetPosition(current_Actors.xmid,
                                          current_Actors.ymid,
                                          current_Actors.height)
        ren.GetActiveCamera().Azimuth(0)
        ren.GetActiveCamera().Elevation(0)
        ren.GetActiveCamera().Yaw(0)
        ren.GetActiveCamera().Roll(0)
        ren.GetActiveCamera().Pitch(0)
        ren.GetActiveCamera().SetViewUp(0, 1, 0)
        renWin.Render()
        return

    def Update_Camera(self, Window, ren, renWin):
        """Function to update the value of the camera for the current
        visualization from the values entered by the user in the GUI.

        Arguments:
            Window: QMainWindow where the visualizations are being carried out.
            ren: current VTK renderer.
            renWin {vtkWidget render window} -- current rendering window.

        Author:
            Jonathan Chico
        """
        camera_focal = [0]*3
        camera_pos = [0]*3
        camera_pos[0] = float(Window.CamPosX.text())
        camera_pos[1] = float(Window.CamPosY.text())
        camera_pos[2] = float(Window.CamPosZ.text())
        camera_focal[0] = float(Window.FocalPosX.text())
        camera_focal[1] = float(Window.FocalPosY.text())
        camera_focal[2] = float(Window.FocalPosZ.text())
        ren.GetActiveCamera().SetFocalPoint(camera_focal)
        ren.GetActiveCamera().SetPosition(camera_pos)
        ren.GetActiveCamera().\
            Elevation(float(Window.CamElevationLineEdit.text()))
        ren.GetActiveCamera().Azimuth(float(Window.CamAzimuthLineEdit.text()))
        ren.GetActiveCamera().Pitch(float(Window.CamPitchLineEdit.text()))
        ren.GetActiveCamera().Roll(float(Window.CamRollLineEdit.text()))
        ren.GetActiveCamera().Yaw(float(Window.CamYawLineEdit.text()))
        renWin.Render()
        return

    def update_dock_info(self, current_Actors, Window):
        """Update the dock window information when the camera is setup

        Arguments:
            current_Actors {[type]} -- [description]
            Window {[type]} -- [description]
        """
        Window.FocalPosX.setText(str(current_Actors.camera_focal[0]))
        Window.FocalPosY.setText(str(current_Actors.camera_focal[1]))
        Window.FocalPosZ.setText(str(current_Actors.camera_focal[2]))
        Window.CamPosX.setText(str(current_Actors.camera_pos[0]))
        Window.CamPosY.setText(str(current_Actors.camera_pos[1]))
        Window.CamPosZ.setText(str(current_Actors.camera_pos[2]))
        Window.CamYawLineEdit.setText(str(current_Actors.camera_yaw))
        Window.CamRollLineEdit.setText(str(current_Actors.camera_roll))
        Window.CamPitchLineEdit.setText(str(current_Actors.camera_pitch))
        Window.CamAzimuthLineEdit.setText(str(current_Actors.camera_azimuth))
        Window.CamElevationLineEdit.\
            setText(str(current_Actors.camera_elevation))
        return

    def set_Camera_viewUp(self, ren, renWin, cam_dir=(1, 0, 0)):
        """Set the camera view up to be defined to the (1, 0, 0)

        Arguments:
            ren {[type]} -- [description]
            renWin {vtkWidget render window} -- current rendering window.

        Keyword Arguments:
            cam_dir {tuple} -- Direction indicating the "up" direction of the
            camera (default: {(1, 0, 0)})
        """
        ren.GetActiveCamera().SetViewUp(cam_dir)
        renWin.Render()
        return

    def toggle_Axes(self, check=False):
        """Toggle option for the axes

        Keyword Arguments:
            check {bool} -- Logical indicating if the axes should be
            toggled/detoggled (default: {False})
        """
        if check:
            self.GenActors.OrientMarker.SetEnabled(1)
        else:
            self.GenActors.OrientMarker.SetEnabled(0)
        return

    def toggle_ScalarBar(self, check=False):
        """Toggle option for the scalar bar

        Keyword Arguments:
            check {bool} -- Logical indicating if the scalar bar should be
            toggled/detoggled (default: {False})
        """
        if check:
            self.GenActors.scalar_bar_widget.SetEnabled(1)
        else:
            self.GenActors.scalar_bar_widget.SetEnabled(0)
        return

    def toggle_contours(self, check=False):
        """Toggle options for the contours

        Keyword Arguments:
            check {bool} -- Logical indicating if the contours should be
            toggled/detoggled (default: {False})
        """
        if check:
            self.MomActors.contActor.VisibilityOn()
        else:
            self.MomActors.contActor.VisibilityOff()
        return

    def toggle_directions(self, check=False):
        """Toggle the directions arrows

        Keyword Arguments:
            check {bool} -- Logical indicating if the spin directions should be
            toggled/detoggled (default: {False})
        """
        if check:
            self.MomActors.vector.VisibilityOn()
        else:
            self.MomActors.vector.VisibilityOff()
        return

    def toggle_spins(self, check=False):
        """Toggle the directions arrows

        Keyword Arguments:
            check {bool} -- Logical indicating if the spins should be
            toggled/detoggled (default: {False})
        """
        if check:
            self.MomActors.Spins.VisibilityOn()
        else:
            self.MomActors.Spins.VisibilityOff()
        return

    def toggle_density(self, check=False):
        """Toggle the magnetization density

        Keyword Arguments:
            check {bool} -- Logical indicating if the interpolated
            magnetization should be toggled/detoggled (default: {False})
        """
        if check:
            self.MomActors.MagDensActor.VisibilityOn()
        else:
            self.MomActors.MagDensActor.VisibilityOff()
        return

    def toggle_cluster(self, check=False):
        """Toggle the visualization of the embedded cluster

        Keyword Arguments:
            check {bool} -- Logical indicating if the cluster should be
            toggled/detoggled (default: {False})
        """
        if check:
            self.GenActors.atom.VisibilityOn()
            self.GenActors.atom_imp.VisibilityOn()
        else:
            self.GenActors.atom.VisibilityOff()
            self.GenActors.atom_imp.VisibilityOff()
        return

    def toggle_KMC(self, check=False):
        """Toggle the KMC particle visualization

        Keyword Arguments:
            check {bool} -- Logical indicating if the KMC particles should be
            toggled/detoggled (default: {False})
        """
        if check:
            self.MomActors.kmc_part_actor.VisibilityOn()
        else:
            self.MomActors.kmc_part_actor.VisibilityOff()
        return

    def toggle_clipper(self, check=False, current_Actors=None,
                       clip_dir=(0, 0, 1), window=None, origin=(0, 0, 0),
                       clip_min=0, clip_max=1, renWin=None):
        """Toggle the plane clipper

        Keyword Arguments:
            check {bool} -- Logical indicating if the clipping plane should be
            toggled/detoggled (default: {False})
            current_Actors {[type]} -- [description] (default: {None})
            clip_dir {tuple} -- Plane normal (default: {(0, 0, 1)})
            window {[type]} -- [description] (default: {None})
            origin {tuple} -- Origin of the plane (default: {(0, 0, 0)})
            clip_min {int} -- [description] (default: {0})
            clip_max {int} -- [description] (default: {1})
            renWin {vtkWidget render window} -- current rendering window.
            (default: {None})
        """
        if check:
            self.GenActors.clipperActor.VisibilityOn()
            current_Actors.VisibilityOff()
            self.set_clipp_plane(clip_dir, window, origin, clip_min, clip_max,
                                 renWin)
        else:
            self.GenActors.clipperActor.VisibilityOff()
            current_Actors.VisibilityOn()
            renWin.Render()
        return

    def toggle_time_label(self, check=False):
        """Toggle the time label

        Keyword Arguments:
            check {bool} -- Logical indicating if the time stamp should be
            toggled/detoggled (default: {False})
        """
        if check:
            self.GenActors.time_label_widget.On()
        else:
            self.GenActors.time_label_widget.Off()
        return

    def set_clipp_plane(self, clip_dir=(0, 0, 1), window=None,
                        origin=(0, 0, 0), clip_min=0, clip_max=1, renWin=None):
        """Set the clipping plane such that the normal plane is 'origin'

        Keyword Arguments:
            clip_dir {tuple} -- [description] (default: {(0, 0, 1)})
            window {[type]} -- [description] (default: {None})
            origin {tuple} -- [description] (default: {(0, 0, 0)})
            clip_min {int} -- [description] (default: {0})
            clip_max {int} -- [description] (default: {1})
            renWin {vtkWidget render window} -- current rendering window.
            (default: {None})
        """
        self.GenActors.plane.SetOrigin(origin)
        self.GenActors.plane.SetNormal(clip_dir)
        window.ClippingPlaneSlider.setMinimum(clip_min)
        window.ClippingPlaneSlider.setMaximum(clip_max)
        window.ClippingPlaneSlider.setValue(clip_min)
        renWin.Render()
        return

    def ClippingUpdate(self, origin=[0, 0, 0], window=None, renWin=None):
        """Set the position of the clipping plane via the slider

        Keyword Arguments:
            origin {list} -- Position of the origin of the clipping plane
            (default: {[0, 0, 0]})
            window {[type]} -- [description] (default: {None})
            renWin {vtkWidget render window} -- current rendering window.
            (default: {None})
        """
        self.GenActors.plane.SetOrigin(origin)
        window.ClipPlaneLabel.\
            setText('Clip. Plane Pos. ={:.1f},{:.1f},{:.1f}'.
                    format(float(origin[0]), float(origin[1]),
                           float(origin[2])))
        renWin.Render()
        return

    def set_projection(self, mom_type='spins', axis=2):
        """Set the color of the magnetization density along an axis

        Keyword Arguments:
            mom_type {str} -- Indicator of which kind of actors are being
            visualized (default: {'spins'})
            axis {int} -- Qunatization axis (x, y, z) (default: {2})
        """

        if mom_type == 'density':
            self.MomActors.src.GetPointData().\
                SetScalars(self.MomActors.glob_color[axis])
        elif mom_type == 'spins':
            self.MomActors.src_spins.GetPointData().\
                SetScalars(self.MomActors.glob_color[axis])
        return

    def ChangeSpinsSize(self, value=0):
        """Set the size of the spins via the slider

        Keyword Arguments:
            value {int} -- Size of the spins (default: {0})
        """
        self.MomActors.SpinMapper.SetScaleFactor(0.50*value/10)
        return

    def toggle_NAtoms(self, check=False):
        """Toggle the atoms for the neighbour map

        Keyword Arguments:
            check {bool} -- Logical for the neighbour atoms (default: {False})
        """
        if check:
            self.NeighActors.AtomsActor.VisibilityOn()
        else:
            self.NeighActors.AtomsActor.VisibilityOff()
        return

    def toggle_Neigh(self, check=False):
        """Toggle the neighbour cloud for the neighbour map

        Keyword Arguments:
            check {bool} -- Logical for the toggling of the neighbour map
            (default: {False})
        """
        if check:
            self.NeighActors.NeighActor.VisibilityOn()
        else:
            self.NeighActors.NeighActor.VisibilityOff()
        return

    def NeighOpacityUpdate(self, value=0):
        """Set the opacity of the neighbour spheres

        Keyword Arguments:
            value {int} -- Set the opacity of the neighbour atoms
            (default: {0})
        """
        self.NeighActors.NeighActor.GetProperty().SetOpacity(value*0.1)
        return

    def AtomOpacityUpdate(self, value=0):
        """Set the opacity of the atom spheres

        Keyword Arguments:
            value {int} -- Opacity of the atomic spheres (default: {0})
        """
        self.NeighActors.AtomsActor.GetProperty().SetOpacity(value*0.1)
        return

    def GlyphQualityUpdate(self, value=20, viz_type=None, mode=1, renWin=None):
        """Function for setting up the quality of the glyphs rendered in VTK.

        Keyword Arguments:
            value {int} -- Quality factor of the glyphs (default: {20})
            viz_type {str} -- Type of visualization, magnetization, energy or
            neighbours (default: {None})
            mode {int} -- Type of rendering, interpolated or discrete
            (default: {1})
            renWin {vtkWidget render window} -- current rendering window.
            (default: {None})
        """
        if viz_type == 'M':
            try:
                self.MomActors.spinarrow.SetTipResolution(value)
                self.MomActors.spinarrow.SetShaftResolution(value)
            except (NameError, AttributeError):
                pass
            try:
                self.MomActors.spinsphere.SetThetaResolution(value)
                self.MomActors.spinsphere.SetPhiResolution(value)
            except (NameError, AttributeError):
                pass
            try:
                self.MomActors.spincones.SetResolution(value)
            except (NameError, AttributeError):
                pass

        if viz_type == 'N':
            if mode == 1:
                self.NeighActors.NeighGlyphs.SetThetaResolution(value)
                self.NeighActors.NeighGlyphs.SetPhiResolution(value)
            if mode == 2:
                self.NeighActors.NeighGlyphs.SetTipResolution(value)
                self.NeighActors.NeighGlyphs.SetShaftResolution(value)
        if viz_type == 'E':
            self.EneActors.EneAtom.SetThetaResolution(value)
            self.EneActors.EneAtom.SetPhiResolution(value)

        renWin.Render()
        return

    def ChangeSpinGlyph(self, renWin, keyword='Arrows'):
        """"Change the type of glyphs used to visualize the atomistic spins.
        It allows the user to change the glyphs for the atomistic spins on the
        fly the user can choose between the following actors:
            * Cone
            * Arrows
            * Spheres
            * Cubes

        Arguments:
            renWin {vtkWidget render window} -- current rendering window.

        Keyword Arguments:
            keyword {str} -- identifier keyword to choose between the different
            types of glyphs. (default: {'Arrows'})

        Author:
            Jonathan Chico
        """

        import vtk

        if keyword == 'Cubes':
            try:
                del self.MomActors.spinarrow
            except (NameError, AttributeError):
                pass
            try:
                del self.MomActors.spinsphere
            except (NameError, AttributeError):
                pass
            try:
                del self.MomActors.spincones
            except (NameError, AttributeError):
                pass
            self.MomActors.spincube = vtk.vtkCubeSource()
            self.MomActors.spincube.SetXLength(1.0)
            self.MomActors.spincube.SetYLength(1.0)
            self.MomActors.spincube.SetZLength(1.0)
            self.MomActors.SpinMapper.\
                SetSourceConnection(self.MomActors.spincube.GetOutputPort())
            self.MomActors.SpinMapper.ClampingOn()
            self.MomActors.SpinMapper.OrientOff()
            renWin.Render()
        if keyword == 'Spheres':
            try:
                del self.MomActors.spinarrow
            except (NameError, AttributeError):
                pass
            try:
                del self.MomActors.spincube
            except (NameError, AttributeError):
                pass
            try:
                del self.MomActors.spincones
            except (NameError, AttributeError):
                pass
            self.MomActors.spinsphere = vtk.vtkSphereSource()
            self.MomActors.spinsphere.SetRadius(1.00)
            self.MomActors.spinsphere.SetThetaResolution(20)
            self.MomActors.spinsphere.SetPhiResolution(20)
            self.MomActors.SpinMapper.\
                SetSourceConnection(self.MomActors.spinsphere.GetOutputPort())
            self.MomActors.SpinMapper.ClampingOn()
            self.MomActors.SpinMapper.OrientOff()
            renWin.Render()
        if keyword == 'Arrows':
            try:
                del self.MomActors.spinsphere
            except (NameError, AttributeError):
                pass
            try:
                del self.MomActors.spincube
            except (NameError, AttributeError):
                pass
            try:
                del self.MomActors.spincones
            except (NameError, AttributeError):
                pass
            self.MomActors.spinarrow = vtk.vtkArrowSource()
            self.MomActors.spinarrow.SetTipRadius(0.20)
            self.MomActors.spinarrow.SetShaftRadius(0.10)
            self.MomActors.spinarrow.SetTipResolution(10)
            self.MomActors.spinarrow.SetShaftResolution(10)
            self.MomActors.SpinMapper.\
                SetSourceConnection(self.MomActors.spinarrow.GetOutputPort())
            self.MomActors.SpinMapper.OrientOn()
            renWin.Render()
        if keyword == 'Cones':
            try:
                del self.MomActors.spinsphere
            except (NameError, AttributeError):
                pass
            try:
                del self.MomActors.spincube
            except (NameError, AttributeError):
                pass
            try:
                del self.MomActors.spinarrow
            except (NameError, AttributeError):
                pass
            self.MomActors.spincones = vtk.vtkConeSource()
            self.MomActors.spincones.SetRadius(0.50)
            self.MomActors.spincones.SetHeight(1.00)
            self.MomActors.spincones.SetResolution(10)
            self.MomActors.SpinMapper.\
                SetSourceConnection(self.MomActors.spincones.GetOutputPort())
            self.MomActors.SpinMapper.OrientOn()
            renWin.Render()
        return

    def set_colormap(self, window=None, flag2D=True, viz_type='M',
                     renWin=None):
        """Select the type of colormap that will be used for the different
        actors
        It allows the user to choose between the following color schemes:
            * Coolwarm
            * RdGy
            * Spectral
            * BlackBody

        Keyword Arguments:
            window {QMainWindow object} -- QMainWindow where the visualizations
            are being carried out. (default: {None})
            flag2D {bool} -- identifier indicating whether the system is in
            2D or 3D. (default: {True})
            viz_type {str} -- identifier for the different types of
            visualization possible in the VTK API. (default: {'M'})
            renWin {vtkWidget render window} -- current rendering window.
            (default: {None})
        """

        import vtk

        self.lut = vtk.vtkLookupTable()
        num_colors = 256
        self.lut.SetNumberOfTableValues(num_colors)
        self.transfer_func = vtk.vtkColorTransferFunction()
        # Set the color map to be given by the diverging Coolwarm scheme by
        # Kenneth Moreland
        if window.sender() == window.ColorMapCM and\
           window.ColorMapCM.isChecked():
            self.transfer_func.SetColorSpaceToDiverging()
            if (flag2D and viz_type == 'M') or (flag2D and viz_type == 'E') or\
               viz_type == 'N':
                self.transfer_func.AddRGBPoint(0, 0.230, 0.299, 0.754)
                self.transfer_func.AddRGBPoint(1, 0.706, 0.016, 0.150)
            else:
                self.transfer_func.AddRGBPoint(-1, 0.230, 0.299, 0.754)
                self.transfer_func.AddRGBPoint(1, 0.706, 0.016, 0.150)
        # Set the color to be given by the black body function
        if window.sender() == window.ColorMapBB and\
           window.ColorMapBB.isChecked():
            self.transfer_func.SetColorSpaceToRGB()
            if (flag2D and viz_type == 'M') or (flag2D and viz_type == 'E') or\
               viz_type == 'N':
                self.transfer_func.AddRGBPoint(0.0, 0.0, 0.0, 0.0)
                self.transfer_func.AddRGBPoint(0.4, 0.9, 0.0, 0.0)
                self.transfer_func.AddRGBPoint(0.8, 0.9, 0.9, 0.0)
                self.transfer_func.AddRGBPoint(1.0, 1.0, 1.0, 1.0)
            else:
                self.transfer_func.AddRGBPoint(-1.0, 0.0, 0.0, 0.0)
                self.transfer_func.AddRGBPoint(-0.5, 0.9, 0.0, 0.0)
                self.transfer_func.AddRGBPoint(0.5, 0.9, 0.9, 0.0)
                self.transfer_func.AddRGBPoint(1.0, 1.0, 1.0, 1.0)
                #self.transfer_func.AddRGBPoint(0.00, 0.9333,0.8235,0.0784);
                #self.transfer_func.AddRGBPoint(0.58, 0.8902,0.4118,0.0196);
                #self.transfer_func.AddRGBPoint(1.00, 0.6980,0.1333,0.1333);
        #-----------------------------------------------------------------------
        # Set the color map to be given by the diverging RdGy
        if window.sender() == window.ColorMapRdGy and\
           window.ColorMapRdGy.isChecked():
            self.transfer_func.SetColorSpaceToDiverging()
            if (flag2D and viz_type == 'M') or (flag2D and viz_type == 'E') or\
               viz_type == 'N':
                self.transfer_func.AddRGBPoint(0.0, 0.79216, 0.00000, 0.12549)
                self.transfer_func.AddRGBPoint(0.5, 1.00000, 1.00000, 1.00000)
                self.transfer_func.AddRGBPoint(1.0, 0.25098, 0.25098, 0.25098)
            else:
                self.transfer_func.AddRGBPoint(-1.0, 0.79216, 0.00000, 0.12549)
                self.transfer_func.AddRGBPoint(0.0, 1.00000, 1.00000, 1.00000)
                self.transfer_func.AddRGBPoint(1.0, 0.25098, 0.25098, 0.25098)
        # Set the color map to be given by the diverging spectral clor map
        if window.sender() == window.ColorMapSpectral and\
           window.ColorMapSpectral.isChecked():
            self.transfer_func.SetColorSpaceToRGB()
            if (flag2D and viz_type == 'M') or (flag2D and viz_type == 'E') or\
               viz_type == 'N':
                self.transfer_func.AddRGBPoint(0.00, 0.61961, 0.00392, 0.25882)
                self.transfer_func.AddRGBPoint(0.25, 0.95686, 0.42745, 0.26275)
                self.transfer_func.AddRGBPoint(0.50, 1.00000, 1.00000, 0.74902)
                self.transfer_func.AddRGBPoint(0.75, 0.40000, 0.76078, 0.64706)
                self.transfer_func.AddRGBPoint(1.00, 0.36863, 0.30980, 0.63529)
            else:
                self.transfer_func.\
                    AddRGBPoint(-1.00, 0.61961, 0.00392, 0.25882)
                self.transfer_func.\
                    AddRGBPoint(-0.50, 0.95686, 0.42745, 0.26275)
                self.transfer_func.\
                    AddRGBPoint(0.00, 1.00000, 1.00000, 0.74902)
                self.transfer_func.\
                    AddRGBPoint(0.50, 0.40000, 0.76078, 0.64706)
                self.transfer_func.\
                    AddRGBPoint(1.00, 0.36863, 0.30980, 0.63529)
        # Construct the lut with the selected colomap
        for ii, ss in enumerate([float(xx)/float(num_colors) for xx in
                                 range(num_colors)]):
            cc = self.transfer_func.GetColor(ss)
            self.lut.SetTableValue(ii, cc[0], cc[1], cc[2], 1.0)
        self.lut.Build()
        # Color the actors depending of the type of visualization
        if viz_type == 'M':
            if (flag2D and viz_type == 'M') or (flag2D and viz_type == 'E') or\
               viz_type == 'N':
                self.MomActors.MagDensMap.SetLookupTable(self.lut)
                self.MomActors.SpinMapper.SetLookupTable(self.lut)
                self.GenActors.scalar_bar.SetLookupTable(self.lut)
                self.GenActors.clipperMapper.SetLookupTable(self.lut)
            else:
                self.MomActors.volumeProperty.SetColor(self.transfer_func)
                self.MomActors.SpinMapper.SetLookupTable(self.transfer_func)
                self.GenActors.scalar_bar.SetLookupTable(self.transfer_func)
                self.GenActors.clipperMapper.SetLookupTable(self.transfer_func)
        elif viz_type == 'N':
            self.NeighActors.NeighMapper.SetLookupTable(self.lut)
            self.GenActors.scalar_bar.SetLookupTable(self.lut)
            self.GenActors.clipperMapper.SetLookupTable(self.lut)
        elif viz_type == 'E':
            if flag2D:
                self.EneActors.EneDensMap.SetLookupTable(self.lut)
                self.GenActors.scalar_bar.SetLookupTable(self.lut)
                self.GenActors.clipperMapper.SetLookupTable(self.lut)
            else:
                self.EneActors.volumeProperty.SetColor(self.transfer_func)
                self.GenActors.scalar_bar.SetLookupTable(self.transfer_func)
                self.GenActors.clipperMapper.SetLookupTable(self.transfer_func)
        # Render the scene
        renWin.Render()
        return
