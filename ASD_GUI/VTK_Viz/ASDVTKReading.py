""" @package ASDVTKReading
The ASDVTKReading clas encompasses all the reading routines necessary for the
several types of visualizations available in the ASD_visualizer.
These include:
    - Reading restart files
    - Reading moment files
    - Reading struct files
    - Reading coord files
The ASDVTKReading tries to make use of the linecache reader capabilities, which
should allow one to deal with large data sets in a much more efficient capacity
than the numpy and string options. However, they are still not at efficient as
the panda read_csv routines, which are not included to avoid compatibility
issues.
This routine also handles whether the structure is 2D or 3D, bear in mind,
that 2D and 3D in this context are not about a true 2D or 3D structure, if not
what kind of density rendering can be done, as for very thin samples, but with
thickness different around 0, a 2D density rendering approach is more
appropiate than a full 3D render approach
When including any further visualization capabilities, requiring different
files it is recommended to include the reading routines inside this class.

Author
----------
Jonathan Chico
"""


class ASDReading():
    """Class for reading the data from ASD files, it makes use of the
    pandas framework to read the data needed for the visualization of magnetic
    moments, neighbours, etc.
    That is any data needed for the VTK visualization.
    """

    def __init__(self):
        """Constructor for the ASDReading class. It contains a set of
        definitions mainly dealing with the names of the data files.
        """
        self.restart = []
        self.posfiles = []
        self.kmcfiles = []
        self.enefiles = []
        self.full_mom = []
        self.full_ene = []
        self.full_KMC = []
        self.structfiles = []
        self.dmdatafiles = []
        self.magnetization = []
        self.not_read_pos = True
        self.not_read_mom = True
        self.not_read_ene = True
        self.not_read_neigh = True
        self.not_read_dmneigh = True
        return

    def getFileName(self, window):
        """Function that gets the file names needed for the different types
        of visualization in the VTK mode, namely:
            - coord.*.out
            - restart.*.out
            - moment.*.out
            - restart.*.out
            - struct.*.out
            - dmstruct.*.out
            - localenergy.*.out
            - kmc_info.*.out
            - clus_info.*.out

        Arguments:
            window {[type]} -- [description]
        """
        from PyQt5 import QtWidgets

        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.AnyFile)
        dlg.setDirectory('.')
        if dlg.exec_():
            if window.sender() == window.actionCoordinate_File:
                self.posfiles = dlg.selectedFiles()[0]
                self.not_read_pos = True
            if window.sender() == window.actionMagnetization_File:
                self.magnetization = dlg.selectedFiles()[0]
                self.not_read_mom = True
            if window.sender() == window.actionStruct_File:
                self.structfiles = dlg.selectedFiles()[0]
                self.not_read_neigh = True
            if window.sender() == window.actionDM_File:
                self.dmdatafiles = dlg.selectedFiles()[0]
                self.not_read_dmneigh = True
            if window.sender() == window.actionKMC_File:
                self.kmcfiles = dlg.selectedFiles()[0]
            if window.sender() == window.actionEnergy_File:
                self.enefiles = dlg.selectedFiles()[0]
                self.not_read_ene = True
        return

    def ReadingWrapper(self, mode, viz_type, file_names, window):
        """Wrapper handling the reading of all the files needed for the
        visualization of the data in the VTK mode.

        Arguments:
            mode {[type]} -- [description]
            viz_type {[type]} -- [description]
            file_names {[type]} -- [description]
            window {[type]} -- [description]
        """
        import glob
        import UI.ASDInputWindows

        self.posfiles = file_names[0]
        self.magnetization = file_names[1]
        self.kmcfiles = file_names[2]
        self.structfiles = file_names[3]
        self.enefiles = file_names[4]
        self.dmdatafiles = file_names[5]
        self.error_trap = False
        # This selects which type of visualization one has moments, neigbours
        # or energy
        # Magnetization (restart, moments) type of visualization
        if viz_type == 'M':
            # Find the restartfile
            if len(self.magnetization) > 0:
                self.MagFile = open(self.magnetization)
            else:
                window.Res_Error_Window = UI.ASDInputWindows.Error_Window()
                window.Res_Error_Window.FunMsg.\
                    setText("I'm sorry, Dave. I'm afraid I can't do that.")
                window.Res_Error_Window.ErrorMsg.\
                    setText("Error: No magnetic configuration file found!")
                window.Res_Error_Window.show()
                self.error_trap = True
                print("Error: No magnetic configuration file selected!")
                print("I'm sorry, Dave. I'm afraid I can't do that.")
        # Neighbour type of visualization
        elif viz_type == 'N':
            if mode == 1:
                # Find the structfile
                if len(self.structfiles) > 0:
                    self.structFile = self.structfiles
                else:
                    print("No file name selected from menu."
                          "Trying to find a 'struct.*.out' file")
                    self.structfiles = glob.glob("struct.*.out")
                    if len(self.structfiles) > 0:
                        self.structfiles = self.structfiles[0]
                        self.structFile = self.structfiles
                    else:
                        window.Neigh_Error_Window =\
                            UI.ASDInputWindows.Error_Window()
                        window.Neigh_Error_Window.FunMsg.\
                            setText("I'm sorry, Dave."
                                    "I'm afraid I can't do that.")
                        window.Neigh_Error_Window.ErrorMsg.\
                            setText("Error: No 'struct.*.out' file found!")
                        window.Neigh_Error_Window.show()
                        self.error_trap = True
                        print("No 'struct.*.out' file found!")
                        print("I'm sorry, Dave. I'm afraid I can't do that.")
            elif mode == 2:
                # Find the dmdatafile
                if len(self.dmdatafiles) > 0:
                    self.DMFile = self.dmdatafiles
                else:
                    print("No file name selected from menu."
                          "Trying to find a 'dmdata.*.out' file")
                    self.dmdatafiles = glob.glob("dmdata.*.out")
                    if len(self.dmdatafiles) > 0:
                        self.dmdatafiles = self.dmdatafiles[0]
                        self.DMFile = self.dmdatafiles
                    else:
                        window.DMNeigh_Error_Window =\
                            UI.ASDInputWindows.Error_Window()
                        window.DMNeigh_Error_Window.FunMsg.\
                            setText("I'm sorry, Dave."
                                    "I'm afraid I can't do that.")
                        window.DMNeigh_Error_Window.ErrorMsg.\
                            setText("Error: No 'dmdata.*.out' file found!")
                        window.DMNeigh_Error_Window.show()
                        self.error_trap = True
                        print("No 'dmdata.*.out' file found!")
                        print("I'm sorry, Dave. I'm afraid I can't do that.")
        # Energy type of visualization
        elif viz_type == 'E':
            # Find the restartfile
            if len(self.enefiles) > 0:
                self.eneFile = self.enefiles
            else:
                print("No file name selected from menu."
                      "Trying to find a 'localenergy.*.out' file")
                self.enefiles = glob.glob("localenergy.*.out")
                if len(self.enefiles) > 0:
                    self.enefiles = self.enefiles[0]
                    self.eneFile = self.enefiles
                else:
                    window.Ene_Error_Window = UI.ASDInputWindows.Error_Window()
                    window.Ene_Error_Window.FunMsg.\
                        setText("I'm sorry, Dave. I'm afraid I can't do that.")
                    window.Ene_Error_Window.ErrorMsg.\
                        setText("Error: No 'localenergy.*.out' file found!")
                    window.Ene_Error_Window.show()
                    self.error_trap = True
                    print("No 'localenergy.*.out' file found!")
                    print("I'm sorry, Dave. I'm afraid I can't do that.")
        # Find the coordinate file
        if len(self.posfiles) > 0:
            atomsFile = open(self.posfiles)
        else:
            print("No file name selected from menu."
                  "Trying to find a 'coord.*.out' file")
            self.posfiles = glob.glob("coord.*.out")
            if len(self.posfiles) > 0:
                self.posfiles = self.posfiles[0]
                atomsFile = self.posfiles
            else:
                window.Coord_Error_Window = UI.ASDInputWindows.Error_Window()
                window.Coord_Error_Window.FunMsg.\
                    setText("Sorry But Our Princess is in Another Castle.")
                window.Coord_Error_Window.ErrorMsg.\
                    setText("Error: No 'coord.*.out' file found!")
                window.Coord_Error_Window.show()
                self.error_trap = True
                print("No 'coord.*.out' file found!")
                print("Sorry But Our Princess is in Another Castle.")
        # Cluster coordinates file
        posfiles_c = glob.glob("clus_info.*.out")
        if len(posfiles_c) > 0:
            self.cluster_flag = True
            # Open the file if it exists
            atomsFile_c = open(posfiles_c[0])
        else:
            self.cluster_flag = False
        # Check if the KMC file must be read
        if len(self.kmcfiles) > 0:
            self.kmc_flag = True
            self.KMCFile = self.kmcfiles
        else:
            self.kmcfiles = glob.glob("kmc_info.*.out")
            if len(self.kmcfiles) > 0:
                self.kmc_flag = True
                # Open the kmc file if it exists
                self.KMCFile = self.kmcfiles[0]
            else:
                self.kmc_flag = False
        # If not read already read the coordinates file
        if self.not_read_pos and not self.error_trap:
            # Actually reading the files
            (self.coord, self.num_atoms, self.flag2D, self.min_val) =\
                self.readAtoms(atomsFile)
            self.not_read_pos = False
            # Checking if the clusters are present
            if self.cluster_flag:
                # Setting the coordinates of the impurity cluster
                (self.coord_c, self.num_atoms_c, self.colors_clus,
                 self.points_clus_imp, self.colors_imp, self.imp_nr) =\
                     self.readAtoms_clus(atomsFile_c)
        # If the type of visualization is on the moments mode read the data
        # about it
        if viz_type == 'M' and self.not_read_mom and\
                not self.error_trap:
            # Read the data for the vectors
            (self.moments, self.colors, self.number_time_steps,
             self.time_sep) =\
                self.readVectorsData(self.MagFile, 0, self.num_atoms, 1)
            self.not_read_mom = False
            # Check if there are KMC files present
            if self.kmc_flag:
                (self.coord_KMC, self.nrKMCpar) =\
                    self.readKMCData(self.KMCFile, 0, 1)
        # If the type of visualization is about the neigbours read that data
        # instead
        elif viz_type == 'N' and self.not_read_neigh and\
                not self.error_trap and mode == 1:
            (self.neighbours, self.Neigh_strength, self.curr_atom,
             self.neigh_types, self.num_types_total) =\
                 self.readNeighbours(self.structFile)
            # Calculate neighbours to iAtom
            (self.neighs, self.atomCenter, self.nTypes, self.neigh_colors,
             self.nNeighs, self.num_types, self.types_counters, self.types) =\
                self.setNeighbours(self.neighbours, 0, self.coord,
                                   self.Neigh_strength, self.curr_atom,
                                   self.neigh_types)
            self.not_read_neigh = False
        # Read the DM neighbour data
        elif viz_type == 'N' and self.not_read_dmneigh and\
                not self.error_trap and mode == 2:
            (self.neighbours, self.dm_vec, self.dm_strength, self.curr_atom,
             self.neigh_types, self.num_types_total) =\
                 self.readDMNeighbours(self.DMFile)
            # Calculate the neighbours to iAtom for the DM interactions
            (self.neighs, self.atomCenter, self.nTypes, self.neigh_colors,
             self.dm_vectors, self.nNeighs, self.num_types,
             self.types_counters, self.types) =\
                self.setDMNeighbours(self.neighbours, 0, self.coord,
                                     self.dm_vec, self.dm_strength,
                                     self.curr_atom, self.neigh_types)
            self.not_read_dmneigh = False
        # If it is an energy type of simulation and the file has not been read,
        # read it
        elif viz_type == 'E' and self.not_read_ene and\
                not self.error_trap:
            # Read the data for the energy
            (self.energies, self.number_time_steps, self.time_sep) =\
                self.readEnergyData(self.eneFile, 0, self.num_atoms, 1)
            self.not_read_ene = False
        # Store some global parameters
        self.mode = mode
        self.viz_type = viz_type
        return

    def readAtoms(self, file_coord="coord._UppASD_.out"):
        """Function to read the position of the atoms in the system, as
        well as calculating if the system should be treated like a 2D or 3D
        system.

        Keyword Arguments:
            file_coord {str} -- Name of the coordinates file
            (default: {"coord._UppASD_.out"})

        Returns:
            [type] -- [description]
        """

        from vtk import vtkPoints
        import numpy as np
        import pandas as pd
        # Define the parameters used to check if the system is 2D
        tol = 2.00
        self.flag2D = False
        # Define vtkPoints as this type of arrays will be used to define the
        # grid
        points = vtkPoints()
        # Read the data with pandas
        coord = pd.read_csv(file_coord, header=None, delim_whitespace=True,
                            usecols=[1, 2, 3]).values
        # Define the number of atoms in the system
        self.num_atoms = len(coord)
        # Pass the numpy type arrays to vtk objects
        for ii in range(0, self.num_atoms):
            points.InsertPoint(ii, coord[ii, 0], coord[ii, 1], coord[ii, 2])
        # Data to check if one should consider the data to be rendered in 2D
        # or 3D
        max_x = np.amax(coord[:, 0])
        min_x = np.amin(coord[:, 0])
        max_y = np.amax(coord[:, 1])
        min_y = np.amin(coord[:, 1])
        max_z = np.amax(coord[:, 2])
        min_z = np.amin(coord[:, 2])
        dist_x = np.sqrt((max_x - min_x)**2)
        dist_y = np.sqrt((max_y - min_y)**2)
        dist_z = np.sqrt((max_z - min_z)**2)
        min_dist = np.amin([dist_x, dist_y, dist_z])
        # If the minimum distace is small set the flag to be 2D, this does not
        # mean the system is trully 2D, if not that the distance is small
        # enough, such that the 2D delaunay algorithm is used
        if min_dist < tol:
            self.flag2D = True
        self.min_val = min_z
        # Cleanup temporary arrays
        del coord
        return points, self.num_atoms, self.flag2D, self.min_val

    def readAtoms_clus(self, file_clus="clus_info._UppASD_.out"):
        """Function to read the positions of the atoms in the cluster,
        this also contains information about whether one should plot the
        impurity center atom.

        Keyword Arguments:
            file_clus {str} -- Name of the cluster infomation file
            (default: {"clus_info._UppASD_.out"})

        Returns:
            [type] -- [description]
        """
        from vtk import vtkPoints, vtkUnsignedCharArray
        import numpy as np
        import pandas as pd
        tol = 1e-5
        # Define vtkPoints arrays for the creation of the grid
        points_clus = vtkPoints()
        points_clus_imp = vtkPoints()
        # Define UnsignedCharArrays for the definitions of the colors to be
        # used
        colors_clus = vtkUnsignedCharArray()
        colors_imp = vtkUnsignedCharArray()
        # Set the size of the arrays
        colors_clus.SetNumberOfComponents(3)
        colors_imp.SetNumberOfComponents(3)
        # Read the data with pandas
        _data = pd.read_csv(file_clus, skiprows=1, header=None,
                            delim_whitespace=True, usecols=[2, 3, 4, 5]).values
        coord = _data[:, 0:3]
        itype = _data[:, 3]
        del _data
        # Define the number of atoms in the cluster
        self.num_atoms_clus = len(coord)
        # This will ensure that one can keep track of how many impurities one
        # actually has in the selected area
        imp_nr = 0
        # Find the indices the correspond to the impurity atoms
        ind_type = np.where(itype == 1)[0]
        # Pass the numpy type data to vtk data
        for ii in range(0, self.num_atoms_clus):
            points_clus.InsertPoint(ii, coord[ii, 0], coord[ii, 1], 
                                    coord[ii, 2])
            colors_clus.InsertTuple3(ii, 0, 0, 0)
            for jj in range(0, len(ind_type)):
                # Data to display the center of the impurity cluster
                dist =\
                    np.sqrt(np.sum((coord[ii, :] - coord[ind_type[jj], :])**2))
                # This is to ensure that the impurity cluster and the embedded
                # cluster are different structures that can be used differently
                if dist < tol:
                    colors_imp.InsertTuple3(imp_nr, 51, 160, 44)
                    points_clus_imp.InsertPoint(imp_nr, coord[ii, 0],
                                                coord[ii, 1], coord[ii, 2])
                    imp_nr = imp_nr + 1
                elif dist < 1:
                    colors_imp.InsertTuple3(imp_nr, 106, 61, 154)
                    points_clus_imp.InsertPoint(imp_nr, coord[ii, 0],
                                                coord[ii, 1], coord[ii, 2])
                    imp_nr = imp_nr + 1
        # Cleanup the leftover data
        del coord
        return points_clus, self.num_atoms_clus, colors_clus, points_clus_imp,\
            colors_imp, imp_nr

    def readVectorsData(self, file_mom='moment._UppASD_.out', time=0,
                        num_atoms=1, temp_count=0):
        """Read the magnetic moments vectors, for both restart and moment
        visualization.
        The number of total lines will be found and used to determine the
        number of frames, these must be then passed back to the function

        Keyword Arguments:
            file_mom {str} -- Name of the magnetic configuration file that will
            be rendered (default: {'moment._UppASD_.out'})
            time {int} -- [description] (default: {0})
            num_atoms {int} -- Number of atoms in the sample (default: {1})
            temp_count {int} -- [description] (default: {0})

        Returns:
            [type] -- [description]
        """

        from vtk import vtkFloatArray
        import numpy as np
        import pandas as pd
        # Create a Double array which represents the vectors
        vectors = vtkFloatArray()
        colors_x = vtkFloatArray()
        colors_y = vtkFloatArray()
        colors_z = vtkFloatArray()
        # Define number of elements
        vectors.SetNumberOfComponents(3)
        colors_x.SetNumberOfComponents(1)
        colors_y.SetNumberOfComponents(1)
        colors_z.SetNumberOfComponents(1)
        # Check which kind of visualization is being done restartfile or
        # momentfile
        if time == 0:
            # Find the format and type of file
            (type_fmt, file_type) = self.check_format(file_mom)
            # Check if the file is in the new format
            if type_fmt == 'new':
                self.full_mom =\
                    pd.read_csv(file_mom, header=None, delim_whitespace=True,
                                skiprows=7, usecols=[4, 5, 6]).values
                file_mom.seek(0)
                # Find how many different "times" there are
                self.number_time_steps = len(self.full_mom)/num_atoms
                # If there are more than one time step
                if self.number_time_steps > 1:
                    # Read the times
                    self.time_sep =\
                        pd.read_csv(file_mom, header=None, skiprows=7,
                                    delim_whitespace=True, usecols=[0]).values
                    # Find the separations between different times
                    self.time_sep = np.unique(self.time_sep)
                    # If there is only one time check if there are several
                    # ensembles
                    if len(self.time_sep) == 1:
                        # Read the ensembles
                        file_mom.seek(0)
                        self.time_sep =\
                            pd.read_csv(file_mom, header=None, skiprows=7,
                                        delim_whitespace=True,
                                        usecols=[1]).values
                        # Find how many different ensembles there are
                        self.time_sep = np.unique(self.time_sep)
                elif self.number_time_steps == 1:
                    self.time_sep = 0
            # Read the file in the old format
            elif type_fmt == 'old':
                # Read the restartfile
                if file_type == 'restart':
                    # Read the restartfile
                    self.full_mom =\
                        pd.read_csv(file_mom, header=None, skiprows=1,
                                    delim_whitespace=True,
                                    usecols=[3, 4, 5]).values
                    file_mom.seek(0)
                    # Find how many different "times" there are
                    self.number_time_steps =\
                        len(self.full_mom)/num_atoms
                    if self.number_time_steps > 1:
                        # Read the ensembles
                        self.time_sep =\
                            pd.read_csv(file_mom, header=None,
                                        delim_whitespace=True,
                                        usecols=[0]).values
                        # Find how many different ensembles there are
                        self.time_sep = np.unique(self.time_sep)
                    elif self.number_time_steps == 1:
                        self.time_sep = 0
                # Read the momentfile
                if file_type == 'moment':
                    # Read the momentfile
                    self.full_mom =\
                        pd.read_csv(file_mom, header=None,
                                    delim_whitespace=True,
                                    usecols=[2, 3, 4]).values
                    file_mom.seek(0)
                    # Find how many different "times" there are
                    self.number_time_steps =\
                        len(self.full_mom)/num_atoms
                    if self.number_time_steps > 1:
                        # Read the times
                        self.time_sep =\
                            pd.read_csv(file_mom, header=None,
                                        delim_whitespace=True,
                                        usecols=[0]).values
                        # Find the separations between different times
                        self.time_sep = np.unique(self.time_sep)
                    elif self.number_time_steps == 1:
                        self.time_sep = 0
        else:
            self.number_time_steps = temp_count
        # Find the boundaries
        _offset = time*(num_atoms)
        min_x = np.amin(self.full_mom[_offset:_offset + num_atoms, 0])
        min_y = np.amin(self.full_mom[_offset:_offset + num_atoms, 1])
        min_z = np.amin(self.full_mom[_offset:_offset + num_atoms, 2])
        max_x = np.amax(self.full_mom[_offset:_offset + num_atoms, 0])
        max_y = np.amax(self.full_mom[_offset:_offset + num_atoms, 1])
        max_z = np.amax(self.full_mom[_offset:_offset + num_atoms, 2])
        # Loop over all the atoms
        for ii in range(0, num_atoms):
            # Pass the data from the numpy arrays to vtk data structures
            vectors.InsertTuple3(ii,
                                 self.full_mom[_offset + ii, 0],
                                 self.full_mom[_offset + ii, 1],
                                 self.full_mom[_offset + ii, 2])
            if self.flag2D:
                colors_x.InsertValue(ii, self.full_mom[_offset + ii, 0])
                colors_y.InsertValue(ii, self.full_mom[_offset + ii, 1])
                colors_z.InsertValue(ii, self.full_mom[_offset + ii, 2])
            else:
                colors_x.InsertValue(ii,
                                     (self.full_mom[_offset + ii, 0] - min_x) /
                                     (max_x - min_x))
                colors_y.InsertValue(ii,
                                     (self.full_mom[_offset + ii, 1] - min_y) /
                                     (max_y-min_y))
                colors_z.InsertValue(ii,
                                     (self.full_mom[_offset + ii, 2] - min_z) /
                                     (max_z-min_z))
        # Pass the colors to an array
        colors = []
        colors.append(colors_x)
        colors.append(colors_y)
        colors.append(colors_z)
        return vectors, colors, self.number_time_steps, self.time_sep

    def check_format(self, filename="restart._UppASD_.out"):
        """Function to determine the type of file and the format of the file

        Keyword Arguments:
            filename {str} -- File name of the studied magnetic configuration
            (default: {"restart._UppASD_.out"})

        Returns:
            [type] -- [description]
        """

        if filename.closed:
            try:
                file_data = open(filename, 'r+')
            except IOError:
                file_data = filename
        else:
            file_data = filename

        line = file_data.readline()
        data = str.split(line)
        if data[0][0] == '#':
            type_fmt = 'new'
            file_type = ''
        else:
            type_fmt = 'old'
            if len(data) == 1:
                file_type = 'restart'
            else:
                file_type = 'moment'
        file_data.seek(0)
        return type_fmt, file_type

    def readEnergyData(self, file_ene="localenergy._UppASD_.out", time=0,
                       num_atoms=1, temp_count=0):
        """Read the site dependent and time dependent energy. It works in
        the same way that the visualization of the magnetic moments.
        in the same way that for the moments one must calculate and then pass
        back the number of time steps in the system.

        Keyword Arguments:
            file_ene {str} -- File name for the site dependent energy
            (default: {"localenergy._UppASD_.out"})
            time {int} -- [description] (default: {0})
            num_atoms {int} -- Number of atoms in the sample (default: {1})
            temp_count {int} -- [description] (default: {0})

        Returns:
            [type] -- [description]
        """

        from vtk import vtkFloatArray
        import numpy as np
        import pandas as pd
        # Create a Double array which represents the vectors
        ene_xc = vtkFloatArray()
        ene_dm = vtkFloatArray()
        ene_bq = vtkFloatArray()
        ene_pd = vtkFloatArray()
        ene_tot = vtkFloatArray()
        ene_dip = vtkFloatArray()
        ene_ani = vtkFloatArray()
        ene_bqdm = vtkFloatArray()
        ene_bext = vtkFloatArray()
        ene_chir = vtkFloatArray()
        # Define number of elements
        ene_xc.SetNumberOfComponents(1)
        ene_dm.SetNumberOfComponents(1)
        ene_bq.SetNumberOfComponents(1)
        ene_pd.SetNumberOfComponents(1)
        ene_tot.SetNumberOfComponents(1)
        ene_dip.SetNumberOfComponents(1)
        ene_ani.SetNumberOfComponents(1)
        ene_bqdm.SetNumberOfComponents(1)
        ene_bext.SetNumberOfComponents(1)
        ene_chir.SetNumberOfComponents(1)
        # Find the number of time steps to perform the simulation
        if time == 0:
            # Read the data in the moment file to find the number of time steps
            self.full_ene =\
                pd.read_csv(file_ene, header=None, skiprows=1,
                            delim_whitespace=True,
                            usecols=[0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13]).\
                values
            # Defining the number of time steps
            self.number_time_steps = len(self.full_ene)/num_atoms
            temp_count = self.number_time_steps
            self.time_sep = np.unique(self.full_ene[:, 0])
        else:
            self.number_time_steps = temp_count
        # Loop over all the atoms
        _offset = time*(num_atoms)
        for ii in range(0, num_atoms):
            # Pass the data from the numpy arrays to vtk data structures
            ene_tot.InsertValue(ii, self.full_ene[_offset + ii, 1])
            ene_xc.InsertValue(ii, self.full_ene[_offset + ii, 2])
            ene_ani.InsertValue(ii, self.full_ene[_offset + ii, 3])
            ene_dm.InsertValue(ii, self.full_ene[_offset + ii, 4])
            ene_pd.InsertValue(ii, self.full_ene[_offset + ii, 5])
            ene_bqdm.InsertValue(ii, self.full_ene[_offset + ii, 6])
            ene_bq.InsertValue(ii, self.full_ene[_offset + ii, 7])
            ene_dip.InsertValue(ii, self.full_ene[_offset + ii, 8])
            ene_bext.InsertValue(ii, self.full_ene[_offset + ii, 9])
            ene_chir.InsertValue(ii, self.full_ene[_offset + ii, 10])
        # Pass the energies to an array
        energies = []
        energies.append(ene_tot)
        energies.append(ene_xc)
        energies.append(ene_dm)
        energies.append(ene_ani)
        energies.append(ene_bq)
        energies.append(ene_bqdm)
        energies.append(ene_pd)
        energies.append(ene_bext)
        energies.append(ene_dip)
        energies.append(ene_chir)
        return energies, self.number_time_steps, self.time_sep

    def readKMCData(self, file_KMC="kmc_info.*.out", time=0, temp_nrKMCpar=0):
        """Read the data needed to visualize the time evolution of the KMC
        particles, it works in the same way that the visualization of the
        magnetic moments.
        In the same way that for the moments one must calculate and then pass
        back the number of time steps in the system.

        Keyword Arguments:
            file_KMC {str} -- name of the KMC file which contains the positions
            of the KMC particles (default: {"kmc_info.*.out"})
            time {int} -- [description] (default: {0})
            temp_nrKMCpar {int} -- [description] (default: {0})

        Returns:
            [type] -- [description]
        """
        from vtk import vtkPoints
        import numpy as np
        import pandas as pd

        coord_KMC = vtkPoints()
        # Read the file first to see how many KMC particles there are
        if time == 0:
            # Read the data in the KMC file to find the number of KMC particles
            self.full_KMC =\
                pd.read_csv(file_KMC, header=None, delim_whitespace=True,
                            usecols=[0, 3, 4, 5]).values
            self.nrKMCpar = len(np.unique(self.full_KMC[:, 0]))
            temp_nrKMCpar = self.nrKMCpar
        else:
            self.nrKMCpar = temp_nrKMCpar
        # Passing the helper arrays to vtk data structures
        for ii in range(0, self.nrKMCpar):
            coord_KMC.\
                InsertPoint(ii,
                            self.full_KMC[time*self.nrKMCpar + ii, 1],
                            self.full_KMC[time*self.nrKMCpar + ii, 2],
                            self.full_KMC[time*self.nrKMCpar + ii, 3])
        return coord_KMC, self.nrKMCpar

    def readNeighbours(self, file_nm):
        """Read and arrange the neighbours from the struct file
        If one reads this file correctly one can generate a visual
        representation of the neighbouring atoms as defined in the neighbour
        map

        Arguments:
            file_nm {str} -- file containing the exchange interactions
            neighbour map

        Returns:
            [type] -- [description]
        """
        import numpy as np
        import pandas as pd
        # Read the data using pandas
        neigh_data =\
            pd.read_csv(file_nm, skiprows=5, header=None,
                        delim_whitespace=True, usecols=[0, 1, 3, 7]).values
        # Store the data in convenient arrays
        curr_atom = neigh_data[:, 0]
        neighbours = neigh_data[:, 1]
        neigh_types = neigh_data[:, 2]
        Neigh_strength = neigh_data[:, 3]
        num_types_total = len(np.unique(neigh_types))
        del neigh_data
        return neighbours, Neigh_strength, curr_atom, neigh_types,\
            num_types_total

    def readDMNeighbours(self, file_nm):
        """Read and arrange the neighbours from the dmdata file
        If one reads this file correctly one can generate a visual
        representation of the neighbouring atoms as defined in the dm
        neighbour map

        Arguments:
            file_nm {[type]} -- file containing the DM vectors

        Returns:
            [type] -- [description]
        """
        import numpy as np
        import pandas as pd

        tol = 1e-6

        # Read the data using pandas
        neigh_data = pd.read_csv(file_nm, skiprows=5, header=None,
                                 delim_whitespace=True,
                                 usecols=[0, 1, 3, 7, 8, 9]).values
        # Store the data in convenient arrays
        curr_atom = neigh_data[:, 0]
        neighbours = neigh_data[:, 1]
        neigh_types = neigh_data[:, 2]
        dm_vec = neigh_data[:, 3:6]
        dm_strength = np.zeros(len(neigh_data), dtype=np.float64)
        for ii in range(0, len(neigh_data)):
            dm_strength[ii] = np.sqrt(dm_vec[ii].dot(dm_vec[ii]))
            if dm_strength > tol:
                dm_vec[ii, :] = dm_vec[ii, :]/dm_strength[ii]
            else:
                dm_vec[ii, :] = 0.0
        num_types_total = len(np.unique(neigh_types))
        del neigh_data
        return neighbours, dm_vec, dm_strength, curr_atom, neigh_types,\
            num_types_total

    def setNeighbours(self, neighbours, iatom, coords, Neigh_strength,
                      curr_atom, neigh_types):
        """The previously defined set of data from the neighbours can then be
        stored in vtk friendly arrays
        Notice the variable iAtom, which defines which atom is being currently
        visualized, that is the variable to be set by the slider

        Arguments:
            neighbours {[type]} -- [description]
            iatom {int} -- [description]
            coords {np.array [3, num_atoms] np.float64} -- Coordinates for all
            the atoms in the system
            Neigh_strength {[type]} -- [description]
            curr_atom {int} -- [description]
            neigh_types {[type]} -- [description]

        Returns:
            [type] -- [description]
        """
        from vtk import vtkPoints, vtkFloatArray
        import numpy as np
        # Set the arrays for the neighbours and the center atom
        neighpoints = vtkPoints()
        atompoint = vtkPoints()
        # Find the indices which correspond to the current atom
        ind = np.where(curr_atom == (iatom + 1))[0]
        # Find the number of neighbours
        num_neighs = len(ind)
        # Find the number of types
        (types, types_counters) =\
            np.unique(neigh_types[ind], return_counts=True)
        num_types = len(types)
        # Find the positions of the current atom
        (x, y, z) = coords.GetPoint(iatom)
        atompoint.InsertPoint(0, x, y, z)
        ntypes = vtkFloatArray()
        ntypes.SetNumberOfComponents(1)
        ntypes.InsertValue(0, 1.25)
        colors = vtkFloatArray()
        colors.SetNumberOfComponents(1)
        colors.InsertValue(0, 0)
        # Pass the data to vtk arrays
        for ineigh in range(0, num_neighs):
            curr_neigh = int(neighbours[ind[ineigh]] - 1)
            (xx, yy, zz) = coords.GetPoint(curr_neigh)
            neighpoints.InsertPoint(ineigh, xx, yy, zz)
            ntypes.InsertValue(ineigh, neigh_types[ind[ineigh]])
            colors.InsertValue(ineigh, Neigh_strength[ind[ineigh]])
        return neighpoints, atompoint, ntypes, colors, num_neighs, num_types,\
            types_counters, types

    def setDMNeighbours(self, neighbours, iatom, coords, dm_vec, dm_strength,
                        curr_atom, neigh_types):
        """The previously defined set of data from the DM vectors
        neighbours can then be stored in vtk friendly arrays.
        Notice the variable iAtom, which defines which atom is being currently
        visualized, that is the variable to be set by the slider

        Arguments:
            neighbours {[type]} -- [description]
            iatom {int} -- [description]
            coords {np.array [3, num_atoms] np.float64} -- Coordinates for all
            the atoms in the system
            dm_vec {[type]} -- [description]
            dm_strength {[type]} -- [description]
            curr_atom {int} -- [description]
            neigh_types {[type]} -- [description]

        Returns:
            [type] -- [description]
        """
        from vtk import vtkPoints, vtkFloatArray
        import numpy as np
        # Set the arrays for the neighbours and the center atom
        neighpoints = vtkPoints()
        atompoint = vtkPoints()
        dm_vectors = vtkFloatArray()
        # Define number of elements
        dm_vectors.SetNumberOfComponents(3)
        # Find the indices which correspond to the current atom
        ind = np.where(curr_atom == (iatom + 1))[0]
        # Find the number of neighbours
        tol = 1e-10
        ind_non_zero = np.where(dm_strength[ind] > tol)[0]
        num_neighs = len(ind[ind_non_zero])
        # Find the number of types
        (types, types_counters) =\
            np.unique(neigh_types[ind], return_counts=True)
        num_types = len(types)
        # Find the positions of the current atom
        (x, y, z) = coords.GetPoint(iatom)
        atompoint.InsertPoint(0, x, y, z)
        ntypes = vtkFloatArray()
        ntypes.SetNumberOfComponents(1)
        ntypes.InsertValue(0, 1.25)
        colors = vtkFloatArray()
        colors.SetNumberOfComponents(1)
        colors.InsertValue(0, 0)
        # Pass the data to vtk arrays
        for ineigh in range(0, num_neighs):
            curr_neigh = int(neighbours[ind[ind_non_zero][ineigh]] - 1)
            (xx, yy, zz) = coords.GetPoint(curr_neigh)
            neighpoints.InsertPoint(ineigh, xx, yy, zz)
            ntypes.InsertValue(ineigh, neigh_types[ind[ind_non_zero][ineigh]])
            colors.InsertValue(ineigh, dm_strength[ind[ind_non_zero][ineigh]])
            dm_vectors.\
                InsertTuple3(ineigh,
                             dm_vec[ind[ind_non_zero][ineigh], 0],
                             dm_vec[ind[ind_non_zero][ineigh], 1],
                             dm_vec[ind[ind_non_zero][ineigh], 2])
        return neighpoints, atompoint, ntypes, colors, dm_vectors, nNeighs,\
            num_types, types_counters, types
