!!!program main
!!!   implicit none
!!!
!!!   call threetemp
!!!
!!!end program main
module temperature_3tm

   use Parameters
   use Profiling
   !integer, parameter :: dblprec = selected_real_kind(15, 307)  !< define precision for double reals

   real(dblprec) :: ps
   real(dblprec) :: C_elec
   real(dblprec) :: C_latt
   real(dblprec) :: C_spin
   real(dblprec) :: Gel
   real(dblprec) :: Ges
   real(dblprec) :: Gsl
   real(dblprec) :: P_pulse
   real(dblprec) :: t0_pulse
   real(dblprec) :: sigma_pulse
   real(dblprec) :: G_cool
   !
   real(dblprec) :: Temp_final
   real(dblprec) :: Temp_latt_init  !< Initial lattice temperature for 3TM
   real(dblprec) :: Temp_spin_init  !< Initial spin temperature for 3TM
   real(dblprec) :: Temp_elec_init  !< Initial electron temperature for 3TM
   !
   character :: do_cs_temp          !< Use variable Cv for spins (Read from file)
   character :: do_cl_temp          !< Use variable Cv for ions  (Read from file)

   integer   :: cs_ntemp            !< Number of read temperatures for Cs(t)
   integer   :: cl_ntemp            !< Number of read temperatures for Cl(t)

   character(len=35) :: csfile  !< Specific heat for spins
   character(len=35) :: clfile  !< Specific heat for spins

   real(dblprec), dimension(:,:), allocatable   :: cs_array      !< spin temperature as an array
   real(dblprec), dimension(:,:), allocatable   :: cl_array      !< lattice temperature as an array

   private

   public :: set_temperature_3tm_defaults, read_parameters_3tm, threetemp_single
   public :: threetemp_print, set_initial_temp_3tm, init_3tm_cv


contains 

   !-----------------------------------------------------------------------------
   ! SUBROUTINE: set_temperature_3tm_defaults
   !> @brief Initialize default values for 3TM
   !
   !> @author Anastasiia Pervishko, Anders Bergman
   !-----------------------------------------------------------------------------
   subroutine set_temperature_3tm_defaults()

      implicit none

      ! Original values:
      !   ps=1.0E-12, C_elec=6.0E+3, C_latt=2.2E+6, C= 0.7E+6, Gel=8.0E+17, Ges=6.0E+17, Gsl=0.3E+17, pi=3.14
      !ps=1.0e-12_dblprec
      ps=1.0_dblprec
      C_elec=6.0e3_dblprec
      C_latt=2.2e6_dblprec
      C_spin= 0.7e6_dblprec
      Gel=8.0e17_dblprec
      Ges=6.0e17_dblprec
      Gsl=0.3e17_dblprec
      P_pulse=1700.0_dblprec
      t0_pulse=1.0e-12_dblprec
      !t0_pulse=2.0e-12_dblprec
      !sigma_pulse=0.01_dblprec
      sigma_pulse=0.02e-12_dblprec
      G_cool=5.0e10_dblprec
      !
      Temp_final=300.0_dblprec
      Temp_spin_init=-1.0_dblprec
      Temp_latt_init=-1.0_dblprec
      Temp_elec_init=-1.0_dblprec
      !
      do_cs_temp = 'N'
      do_cl_temp = 'N'

   end subroutine set_temperature_3tm_defaults

   !-----------------------------------------------------------------------------
   ! SUBROUTINE: threetemp_demo
   !> @brief Routine for timestep evolution of temperatures according to 3TM
   !
   !> @author Anastasiia Pervishko
   !-----------------------------------------------------------------------------
   subroutine threetemp_demo()

      implicit none
      integer, parameter :: n=3
      real(dblprec) :: ti, tf, dt, tmax
      real(dblprec), dimension(3) :: xi, xf
      integer :: i

      open (unit=16, file='temperature.dat')


      ps=1.0e-12
      ti =    0.0               ! initial time (ps)
      xi(1) = 300.0             ! initial Te
      xi(2) = 300.0             ! initial TL
      xi(3) = 300.0             ! initial Ts

      dt   = 0.001               ! timestep (ps)
      tmax = 10.0                ! final time (ps)

      write (16,102) ti, xi(1), xi(2), xi(3)

      do while (ti <= tmax)
         tf = ti + dt

         call rk4n(f_3tm,ti, tf, xi, xf, n)

         write(16,102) tf, xf(1), xf(2), xf(3)

         ti = tf
         do i = 1,n
            xi(i) = xf(i)
         end do
      end do
      ps=1.0

      102 format(4(1pe12.3))
   end subroutine threetemp_demo

   !-----------------------------------------------------------------------------
   ! SUBROUTINE: threetemp_demo
   !> @brief Routine for timestep evolution of temperatures according to 3TM
   !
   !> @author Anders Bergman, Anastasiia Pervishko
   !-----------------------------------------------------------------------------
   subroutine threetemp_print(rstep,mstep,delta_t,simid)

      implicit none

      integer, intent(in) :: rstep !< Starting simulation step
      integer, intent(in) :: mstep !< Number of simulation steps
      real(dblprec), intent(in) :: delta_t !< Timestep
      character(len=8), intent(in) :: simid !< simulation name

      integer, parameter :: n=3
      real(dblprec) :: ti, tf
      real(dblprec), dimension(3) :: xi, xf
      integer :: i,istep
      character(len=30) :: filn

      write (filn,'(''temperature_3tm.'',a8,''.out'')') simid
      open(ofileno,file=filn, position='append')


      ti =    rstep*delta_t    ! initial time (ps)
      xi(1) = Temp_elec_init      ! initial Te
      xi(2) = Temp_latt_init      ! initial TL
      xi(3) = Temp_spin_init      ! initial Ts


      write (ofileno,102) ti, xi(1), xi(2), xi(3)

      do istep=rstep,rstep+mstep
         tf = ti + delta_t

         call rk4n(f_3tm_var,ti, tf, xi, xf, n)

         write(ofileno,102) tf, xf(1), xf(2), xf(3)

         ti = tf
         do i = 1,n
            xi(i) = xf(i)
         end do
      end do

      close(ofileno)

      102 format(4(1pe12.3))
   end subroutine threetemp_print

   !-----------------------------------------------------------------------------
   ! SUBROUTINE: threetemp_single
   !> @brief Routine for single timestep evolution of temperatures according to 3TM
   !
   !> @author Anastasiia Pervishko, Anders Bergman
   !-----------------------------------------------------------------------------
   subroutine threetemp_single(t_in,dt,Temp_s,Temp_l,Temp_e)

      implicit none

      real(dblprec), intent(in) :: t_in        !< Current time
      real(dblprec), intent(in) :: dt          !< Time step
      real(dblprec), intent(inout) :: Temp_s   !< Spin temperature
      real(dblprec), intent(inout) :: Temp_l   !< Lattice temperature
      real(dblprec), intent(inout) :: Temp_e   !< Electronic temperature

      integer, parameter :: n=3
      real(dblprec) ::  t_fin
      real(dblprec), dimension(3) :: xi, xf

      xi(1) = Temp_e            ! initial Te
      xi(2) = Temp_l            ! initial TL
      xi(3) = Temp_s            ! initial Ts


      t_fin = t_in + dt

      call rk4n(f_3tm_var,t_in, t_fin, xi, xf, n)

      Temp_e =  xf(1)           ! initial Te
      Temp_l =  xf(2)           ! initial TL
      Temp_s =  xf(3)           ! initial Ts

   end subroutine threetemp_single

   !-----------------------------------------------------------------------------
   ! SUBROUTINE: f_3tm
   !> @brief Temperature functions for the three-temperature model with constant
   !         heat capacities
   !> @author Anastasiia Pervishko
   !-----------------------------------------------------------------------------
   subroutine f_3tm(t, x, dx,n)
      implicit none
      real(dblprec) ::  t
      integer :: n
      real(dblprec), dimension(n) :: x
      real(dblprec), dimension(n) :: dx
      real(dblprec) :: A, B, D, F, G, H

 !   real, parameter:: A=ps*Gel/C_elec, B=ps*Ges/C_elec, D=ps*Gel/C_latt,F=ps*Gsl/C_latt, G=ps*Ges/C_spin, H=ps*Gsl/C_spin
      A=ps*Gel/C_elec
      B=ps*Ges/C_elec
      D=ps*Gel/C_latt
      F=ps*Gsl/C_latt
      G=ps*Ges/C_spin
      H=ps*Gsl/C_spin

      dx(1) = -A*(x(1)-x(2))/x(1) - B*(x(1)-x(3))/x(1) + &
         P_pulse/(1.0e-12/ps)*exp(-(t - t0_pulse/ps)**(2)/(2.0_dblprec*(sigma_pulse/ps)**2)) &
      - ps*G_cool*(x(1)-Temp_final)
      dx(2) = D*(x(1)-x(2)) - F*(x(2)-x(3))
      dx(3) = G*(x(1)-x(3)) + H*(x(2)-x(3))

   end subroutine f_3tm

   !-----------------------------------------------------------------------------
   ! SUBROUTINE: f_3tm_var
   !> @brief Temperature functions for the three-temperature model for variable
   !         heat capacities
   !> @author Anastasiia Pervishko, Anders Bergman
   !-----------------------------------------------------------------------------
   subroutine f_3tm_var(t, x, dx,n)
      implicit none
      real(dblprec) ::  t
      integer :: n
      real(dblprec), dimension(n) :: x
      real(dblprec), dimension(n) :: dx
      real(dblprec) :: A, B, D, F, G, H

 !   real, parameter:: A=ps*Gel/C_elec, B=ps*Ges/C_elec, D=ps*Gel/C_latt,F=ps*Gsl/C_latt, G=ps*Ges/C_spin, H=ps*Gsl/C_spin
      A=ps*Gel/C_elec
      B=ps*Ges/C_elec
      D=ps*Gel/f_C_latt(x(2))
      F=ps*Gsl/f_C_latt(x(2))
      G=ps*Ges/f_C_spin(x(3))
      H=ps*Gsl/f_C_spin(x(3))

      dx(1) = -A*(x(1)-x(2))/x(1) - B*(x(1)-x(3))/x(1) + &
         P_pulse/(1.0e-12/ps)*exp(-(t - t0_pulse/ps)**(2)/(2.0_dblprec*(sigma_pulse/ps)**2)) &
      - ps*G_cool*(x(1)-Temp_final)
      dx(2) = D*(x(1)-x(2)) - F*(x(2)-x(3))
      dx(3) = G*(x(1)-x(3)) + H*(x(2)-x(3))

   end subroutine f_3tm_var

   function f_C_latt(temp) result(C_l)
      !
      use Math_Functions, only : f_interp_1d
      !
      implicit none
      ! 
      real(dblprec), intent(in) :: temp
      real(dblprec) :: C_l
      !
      if (do_cl_temp/='Y') then
         C_l=C_latt
      else
         C_l=f_interp_1d(temp,cl_array(:,1),cl_array(:,2),cl_ntemp)
      end if

      return
      !
   end function f_C_latt

   function f_C_spin(temp) result(C_s)
      !
      use Math_Functions, only : f_interp_1d
      !
      implicit none
      ! 
      real(dblprec), intent(in) :: temp
      real(dblprec) :: C_s
      !
      if (do_cs_temp/='Y') then
         C_s=C_spin
      else
         C_s=f_interp_1d(temp,cs_array(:,1),cs_array(:,2),cs_ntemp)
      end if

      return
      !
   end function f_C_spin

   !-----------------------------------------------------------------------------
   ! SUBROUTINE: rk4n
   !> @brief Runge-Kutta solver for function fcn
   !
   !> @author Anastasiia Pervishko
   !-----------------------------------------------------------------------------
   subroutine rk4n(fcn,ti, tf, xi, xf, n)
      implicit none
      real(dblprec) :: ti, tf
      integer :: n 
      real(dblprec), dimension(n) :: xi
      real(dblprec), dimension(n) :: xf
      external :: fcn

      integer  :: j
      real(dblprec) h, t
      real(dblprec), dimension(n) ::  x
      real(dblprec), dimension(n) ::  dx
      real(dblprec), dimension(n) ::  k1,k2,k3,k4

      h = tf-ti
      t = ti

      call fcn(t, xi, dx, n)
      do j=1,n
         k1(j) = h*dx(j)
         x(j)  = xi(j) + k1(j)/2.0_dblprec
      end do

      call fcn(t+h/2.0_dblprec, x, dx, n)
      do j=1,n
         k2(j) = h*dx(j)
         x(j)  = xi(j) + k2(j)/2.0_dblprec
      end do

      call fcn(t+h/2.0_dblprec, x, dx, n)
      do j=1,n
         k3(j) = h*dx(j)
         x(j)  = xi(j) + k3(j)
      end do

      call fcn(t+h, x, dx, n)
      do j=1,n
         k4(j) = h*dx(j)
         xf(j) = xi(j) + k1(j)/6.0_dblprec+k2(j)/3.0_dblprec+k3(j)/3.0_dblprec+k4(j)/6.0_dblprec
      end do

      !print '(a,3f12.6,2g20.4)', 'xi: :',xi,t,h
      !print '(a,3g14.4)', '  k1:',k1
      !print '(a,3g14.4)', '  k2:',k2
      !print '(a,3g14.4)', '  k3:',k3
      !print '(a,3g14.4)', '  k4:',k4
      !print '(a,3f12.6)', 'xf: :',xf

   end subroutine rk4n

   !!!    !-----------------------------------------------------------------------------
   !!!    ! SUBROUTINE: allocate_temp
   !!!    !> @brief Subroutine to allocate the arrays that contain the temperature for
   !!!    !> the three-temperature model
   !!!    !> @author Anders Bergman
   !!!    !-----------------------------------------------------------------------------
   !!!    subroutine allocate_3tm(natom, ip_nphase,flag)
   !!! 
   !!!       use Profiling 
   !!! 
   !!!       implicit none
   !!! 
   !!!       integer, intent(in) :: natom
   !!!       integer, intent(in) :: ip_nphase
   !!!       integer, intent(in) :: flag
   !!! 
   !!!       integer :: i_stat,i_all
   !!! 
   !!!       if(flag>=0) then
   !!!          allocate(temp_s_array(natom),stat=i_stat)
   !!!          call memocc(i_stat,product(shape(temp_s_array))*kind(temp_s_array),'temp_s_array','allocate_3tm')
   !!!          allocate(temp_l_array(natom),stat=i_stat)
   !!!          call memocc(i_stat,product(shape(temp_l_array))*kind(temp_l_array),'temp_l_array','allocate_3tm')
   !!!          allocate(temp_e_array(natom),stat=i_stat)
   !!!          call memocc(i_stat,product(shape(temp_e_array))*kind(temp_e_array),'temp_e_array','allocate_3tm')
   !!!          if(ip_nphase>0) then
   !!!             allocate(iptemp_s_array(natom,ip_nphase),stat=i_stat)
   !!!             call memocc(i_stat,product(shape(iptemp_s_array))*kind(iptemp_s_array),'iptemp_s_array','allocate_3tm')
   !!!             allocate(iptemp_l_array(natom,ip_nphase),stat=i_stat)
   !!!             call memocc(i_stat,product(shape(iptemp_l_array))*kind(iptemp_l_array),'iptemp_l_array','allocate_3tm')
   !!!             allocate(iptemp_e_array(natom,ip_nphase),stat=i_stat)
   !!!             call memocc(i_stat,product(shape(iptemp_e_array))*kind(iptemp_e_array),'iptemp_e_array','allocate_3tm')
   !!!          end if
   !!!       else
   !!!          i_all=-product(shape(temp_s_array))*kind(temp_s_array)
   !!!          deallocate(temp_s_array,stat=i_stat)
   !!!          call memocc(i_stat,i_all,'temp_s_array','allocate_3tm')
   !!!          i_all=-product(shape(temp_l_array))*kind(temp_l_array)
   !!!          deallocate(temp_l_array,stat=i_stat)
   !!!          call memocc(i_stat,i_all,'temp_l_array','allocate_3tm')
   !!!          i_all=-product(shape(temp_e_array))*kind(temp_e_array)
   !!!          deallocate(temp_e_array,stat=i_stat)
   !!!          call memocc(i_stat,i_all,'temp_e_array','allocate_3tm')
   !!!          if(ip_nphase>0) then
   !!!             i_all=-product(shape(iptemp_s_array))*kind(iptemp_s_array)
   !!!             deallocate(iptemp_s_array,stat=i_stat)
   !!!             call memocc(i_stat,i_all,'iptemp_s_array','allocate_3tm')
   !!!             i_all=-product(shape(iptemp_l_array))*kind(iptemp_l_array)
   !!!             deallocate(iptemp_l_array,stat=i_stat)
   !!!             call memocc(i_stat,i_all,'iptemp_l_array','allocate_3tm')
   !!!             i_all=-product(shape(iptemp_e_array))*kind(iptemp_e_array)
   !!!             deallocate(iptemp_e_array,stat=i_stat)
   !!!             call memocc(i_stat,i_all,'iptemp_e_array','allocate_3tm')
   !!!          end if
   !!!       end if
   !!! 
   !!!    end subroutine allocate_3tm

   !-----------------------------------------------------------------------------
   ! SUBROUTINE: set_initial_temp_3tm
   !> @brief Set initial temperatures for 3TM 
   !
   !> @author Anders Bergman
   !-----------------------------------------------------------------------------
   subroutine set_initial_temp_3tm(Temp,Temp_s,Temp_l,Temp_e)

      implicit none

      real(dblprec), intent(in) :: Temp   !< Global temperature for non-3TM simulations
      real(dblprec), intent(out) :: Temp_s !< Global temperature for non-3TM simulations
      real(dblprec), intent(out) :: Temp_l !< Global temperature for non-3TM simulations
      real(dblprec), intent(out) :: Temp_e !< Global temperature for non-3TM simulations

      ! If initial temperatures are not specified then 
      ! set all initial temperaturees to global Temp
      if (Temp_latt_init<0.0_dblprec) Temp_latt_init=Temp
      if (Temp_spin_init<0.0_dblprec) Temp_spin_init=Temp
      if (Temp_elec_init<0.0_dblprec) Temp_elec_init=Temp

      Temp_l=Temp_latt_init
      Temp_s=Temp_spin_init
      Temp_e=Temp_elec_init

   end subroutine set_initial_temp_3tm

   !---------------------------------------------------------------------------
   ! SUBROUTINE: read_parameters_3tm
   !> @brief
   !> Read input parameters for the three-temperature model
   !
   !> @author
   !> Anders Bergman
   !---------------------------------------------------------------------------
   subroutine read_parameters_3tm(ifile)

      use FileParser
      use ErrorHandling

      implicit none

      ! ... Formal Arguments ...
      integer, intent(in) :: ifile   !< File to read from
      !
      ! ... Local Variables ...
      character(len=50) :: keyword, cache
      integer :: rd_len, i_err, i_errb
      logical :: comment

      do
         10      continue
         ! Read file character for character until first whitespace
         keyword=""
         call bytereader(keyword,rd_len,ifile,i_errb)

         ! converting Capital letters
         call caps2small(keyword)

         ! check for comment markers (currently % and #)
         comment=(scan(trim(keyword),'%')==1).or.(scan(trim(keyword),'#')==1).or.&
            (scan(trim(keyword),'*')==1).or.(scan(trim(keyword),'=')==1.or.&
            (scan(trim(keyword),'!')==1))

         if (comment) then
            read(ifile,*)
         else
            ! Parse keyword
            keyword=trim(keyword)
            select case(keyword)
            ! This is the flags for the ElkGeometry module

         case('ce')
            read(ifile,*,iostat=i_err) C_elec
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('cl')
            read(ifile,*,iostat=i_err) C_latt
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('cs')
            read(ifile,*,iostat=i_err) C_spin
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('gel')
            read(ifile,*,iostat=i_err) Gel
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('ges')
            read(ifile,*,iostat=i_err) Ges
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('p_pulse')
            read(ifile,*,iostat=i_err) P_pulse
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('t0_pulse')
            read(ifile,*,iostat=i_err) t0_pulse
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('sigma_pulse')
            read(ifile,*,iostat=i_err) sigma_pulse
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('temp_spin_init')
            read(ifile,*,iostat=i_err) Temp_spin_init
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('temp_latt_init')
            read(ifile,*,iostat=i_err) Temp_latt_init
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('temp_elec_init')
            read(ifile,*,iostat=i_err) Temp_elec_init
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('temp_final')
            read(ifile,*,iostat=i_err) Temp_final
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('g_cool')
            read(ifile,*,iostat=i_err) G_cool
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('do_cs_temp')
            read(ifile,*,iostat=i_err) do_cs_temp
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('do_cl_temp')
            read(ifile,*,iostat=i_err) do_cl_temp
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err

         case('cv_spinfile')
            read(ifile,'(a)',iostat=i_err) cache
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err
            csfile=trim(adjustl(cache))
            call ErrorHandling_check_file_exists(csfile, &
               'Please specify csfile <file> where <file> is a valid heat capacity file')

         case('cv_lattfile')
            read(ifile,'(a)',iostat=i_err) cache
            if(i_err/=0) write(*,*) 'ERROR: Reading ',trim(keyword),' data',i_err
            clfile=trim(adjustl(cache))
            call ErrorHandling_check_file_exists(clfile, &
               'Please specify clfile <file> where <file> is a valid heat capacity file')

         end select
      end if

      ! End of file
      if (i_errb==20) goto 20
      ! End of row
      if (i_errb==10) goto 10
   end do

   20   continue

   rewind(ifile)
   return
end subroutine read_parameters_3tm

subroutine init_3tm_cv(flag)
   !
   implicit none
   !
   integer, intent(in) :: flag
   !
   integer :: itemp, i_stat, i_all
   real(dblprec) :: tdum,cdum
   !

   if (flag>0) then
      if (do_cs_temp=='Y') then
         open(ifileno,file=csfile)

         ! Find the number of spin temperatures 
         cs_ntemp=0
         do 
            read(ifileno,*,end=30) tdum, cdum
            cs_ntemp=cs_ntemp+1
         end do
         30         continue
         rewind(ifileno)

         ! Allocate array for spin Cv
         allocate(cs_array(cs_ntemp,2),stat=i_stat)
         call memocc(i_stat,product(shape(cs_array))*kind(cs_array),'cs_array','init_3tm_cv')

         ! Read all input spin temperatures and heat capacities
         do itemp=1,cs_ntemp
            read(ifileno,*) cs_array(itemp,1), cs_array(itemp,2)
         end do
         close(ifileno)
         print *,' Spin CV file read'
      end if

      if (do_cl_temp=='Y') then
         open(ifileno,file=clfile)
         ! Find the number of lattice temperatures 
         cl_ntemp=0
         do 
            read(ifileno,*,end=40) tdum, cdum
            cl_ntemp=cl_ntemp+1
         end do
         40         continue

         ! Allocate array for lattice Cv
         allocate(cl_array(cl_ntemp,2),stat=i_stat)
         call memocc(i_stat,product(shape(cl_array))*kind(cl_array),'cl_array','init_3tm_cv')

         ! Read all input spin temperatures and heat capacities
         do itemp=1,cs_ntemp
            read(ifileno,*) cl_array(itemp,1), cl_array(itemp,2)
         end do
         print *,' Lattice CV file read'
      end if

   else

      if (do_cs_temp=='Y') then
         ! Deallocate arrays
         i_all=-product(shape(cs_array))*kind(cs_array)
         deallocate(cs_array,stat=i_stat)
         call memocc(i_stat,i_all,'cs_array','init_3tm_cv')
      end if

      if (do_cl_temp=='Y') then
         i_all=-product(shape(cl_array))*kind(cl_array)
         deallocate(cl_array,stat=i_stat)
         call memocc(i_stat,i_all,'cl_array','init_3tm_cv')
      end if
   end if

end subroutine init_3tm_cv

end module temperature_3tm
