!> Module for findng unique sets of atoms ("metatypes") from coordination numbers
module MetaTypes
   use Parameters
   use Profiling
   !
   implicit none
   !
   integer :: NT_meta
   integer, dimension(:), allocatable :: atype_meta


contains

   subroutine allocate_metatype(Natom,flag)

      implicit none

      integer, intent(in), optional :: Natom !< Number of atoms in system
      integer, intent(in) :: flag  !< Allocate or deallocate (1/-1)

      integer :: i_all, i_stat

      ! If flag > 0 allocate arrays
      if(flag>0) then
         allocate(atype_meta(Natom),stat=i_stat)
         call memocc(i_stat,product(shape(atype_meta))*kind(atype_meta),'atype_meta','allocate_metatype')
      else
         ! Otherwise deallocate arrays
         i_all=-product(shape(atype_meta))*kind(atype_meta)
         deallocate(atype_meta,stat=i_stat)
         call memocc(i_stat,i_all,'atype_meta','allocate_metatype')
      end if

   end subroutine allocate_metatype

   subroutine find_metatype_coordination(Natom,atype,ham,metatype)
      use HamiltonianDataType

      integer, intent(in) :: Natom    !< Number of atoms
      integer, dimension(Natom) , intent(in) :: atype !< Array of proper atom types
      type(ham_t), intent(in) :: ham  !< Hamiltonian
      integer, intent(in) :: metatype  !< Allocate or deallocate (1/-1)

      integer :: iatom, nuniq, itype, NT
      integer, dimension(:), allocatable :: nn_max_type
      integer :: i_all, i_stat


      call allocate_metatype(Natom,1)

      NT=maxval(atype)
      !allocate(nn_max_type(nuniq),stat=i_stat)
      allocate(nn_max_type(2*NT),stat=i_stat)
      call memocc(i_stat,product(shape(nn_max_type))*kind(nn_max_type),'nn_max_type','find_metatype_coordination')
      nn_max_type=0

      ! Identify atoms with less then maximum coordination
      if (metatype==1) then
         do itype=1,NT
            do iatom=1,Natom
               if (atype(iatom)==itype) then
                  nn_max_type(itype)=max(nn_max_type(itype),ham%nlistsize(iatom))
               end if
            end do
            do iatom=1,Natom
               if (atype(iatom)==itype) then
                  if (ham%nlistsize(iatom)==nn_max_type(itype)) then
                     atype_meta(iatom)=itype
                  else
                     atype_meta(iatom)=itype+NT
                  end if
               end if
            end do
         end do
         NT_meta=maxval(atype_meta)
      else if (metatype==2) then
         do itype=1,NT
            do iatom=1,Natom
               if (atype(iatom)==itype) then
                  nn_max_type(itype)=max(nn_max_type(itype),ham%nlistsize(iatom))
               end if
            end do
            do iatom=1,Natom
               if (atype(iatom)==itype) then
                  if (ham%nlistsize(iatom)==nn_max_type(itype)) then
                     atype_meta(iatom)=1
                  else
                     atype_meta(iatom)=2
                  end if
               end if
            end do
         end do
         NT_meta=2
      else if (metatype==0) then
         NT_meta=NT
         atype_meta=atype
      end if

      i_all=-product(shape(nn_max_type))*kind(nn_max_type)
      deallocate(nn_max_type,stat=i_stat)
      call memocc(i_stat,i_all,'nn_max_type','find_metatype_coordination')

   end subroutine find_metatype_coordination


end module MetaTypes
