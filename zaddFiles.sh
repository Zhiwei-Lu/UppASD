#!/bin/bash

# Adds relevant missing files to the git repository
cat filesToAdd.dat | while read filename

do

    git add $filename
    
done
